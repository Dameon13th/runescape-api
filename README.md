# Introduction #

The Runescape.API exposes C# classes that abstract calling into the Runescape public REST APIs.

### Archaeology ###

This entire project started with my own frustration of figuring out the materials required to repair my artifacts. Tired of using Excel spreadsheets I looked for something better.
This library contains static classes that contain all the Artifacts and Materials related to the Archaeology skill.


There is also Artifact and Material storage classes that allow you to add artifacts and it well calculate the required materials for you. Using the IGrandExchange API it can even calculate the cost for buying the materials to repair the artifacts.

### Documentation ###

The [Sandcastle](https://en.wikipedia.org/wiki/Sandcastle_(software)) generated web pages are hosted on a public AWS S3 bucket.
[Documentation](http://runescape-api.s3-website.us-east-2.amazonaws.com)

### Reaching Out ###
If you are any questions or suggestions reach out to Thanatoes13th@live.com