using System;
using System.Net;
using System.Net.Http;

namespace Runescape.Api.Tests
{
    internal abstract class UrlCustomHandler
    {
        public HttpStatusCode StatusCode { get; set; }

        public Action<HttpRequestMessage> RequestValidater { get; set; }

        public abstract HttpContent GetHttpContent();
    }
}