using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace Runescape.Api.Tests
{
    internal class JsonSerializedHandler : UrlCustomHandler
    {
        public object Object { get; set; }

        #region Overrides of UrlCustomHandler

        public override HttpContent GetHttpContent()
        {
            string json = JsonConvert.SerializeObject(Object);
            byte[] buffer = Encoding.UTF8.GetBytes(json);

            MemoryStream stream = new MemoryStream();
            stream.Write(buffer, 0, buffer.Length);

            if (stream.CanSeek)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }

            StreamContent content = new StreamContent(stream);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return content;
        }

        #endregion
    }
}