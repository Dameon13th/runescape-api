using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Runescape.Api.Tests
{
    internal class StreamContentHandler : UrlCustomHandler
    {
        public string ContentType { get; set; }

        public Stream Stream { get; set; }

        #region Overrides of UrlCustomHandler

        public override HttpContent GetHttpContent()
        {
            StreamContent content = new StreamContent(Stream);
            content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);
            content.Headers.ContentLength = Stream.Length;

            return content;
        }

        #endregion
    }
}