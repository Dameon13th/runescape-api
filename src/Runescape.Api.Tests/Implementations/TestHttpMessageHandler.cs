using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Runescape.Api.Tests
{
    internal class TestHttpMessageHandler : HttpMessageHandler
    {
        #region "Private Members"

        private readonly Dictionary<string, UrlCustomHandler> _handlers;

        #endregion

        #region "Constructor"

        public TestHttpMessageHandler()
        {
            _handlers = new Dictionary<string, UrlCustomHandler>();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Adds a handler for the given <paramref name="url"/>.
        /// </summary>
        /// <param name="url">The URL of the <see cref="HttpRequestMessage"/> to return the <paramref name="returnData"/> for.</param>
        /// <param name="statusCode">
        /// The status code of the <see cref="HttpResponseMessage"/> to be returned for the requested <paramref name="url"/>
        /// </param>
        /// <param name="returnData">
        /// The object to serialized into JSON and used as the body of the <see cref="HttpResponseMessage"/> to be returned
        /// for the requested <paramref name="url"/>.
        /// </param>
        public void AddCustomHandler(string url, HttpStatusCode statusCode, object returnData)
        {
            JsonSerializedHandler handler = new JsonSerializedHandler
            {
                StatusCode = statusCode,
                Object = returnData
            };
            _handlers[url] = handler;
        }

        public void AddCustomHandler(string url, HttpStatusCode statusCode, string contentType, Stream stream)
        {
            StreamContentHandler handler = new StreamContentHandler
            {
                StatusCode = statusCode,
                ContentType = contentType,
                Stream = stream
            };
            _handlers[url] = handler;
        }

        #endregion

        #region Overrides of HttpMessageHandler

        /// <summary>Send an HTTP request as an asynchronous operation.</summary>
        /// <param name="request">The HTTP request message to send.</param>
        /// <param name="cancellationToken">The cancellation token to cancel operation.</param>
        /// <returns>The task object representing the asynchronous operation.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string url = request.RequestUri.OriginalString;

            if (!_handlers.ContainsKey(url))
            {
                throw new NotImplementedException($"No handler added for the following URL.\n{request.RequestUri.AbsolutePath}");
            }

            UrlCustomHandler handler = _handlers[url];
            if (handler.RequestValidater != null)
            {
                handler.RequestValidater(request);
            }

            HttpResponseMessage response = new HttpResponseMessage(handler.StatusCode)
            {
                Content = handler.GetHttpContent()
            };
            return Task.FromResult(response);
        }

        #endregion
    }

    internal class ExceptionHttpMessageHandler : TestHttpMessageHandler
    {
        #region Overrides of HttpMessageHandler

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException("This is a simulated Net exception.  For testing fail cases.");
        }

        #endregion
    }
}