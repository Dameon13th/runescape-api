using System;
using System.Text;

namespace Runescape.Api.Tests
{
    internal static class RandomString
    {
        #region "Private Members"

        private static readonly Random _random;

        #endregion

        #region "Constructor"

        static RandomString()
        {
            _random = new Random();
        }

        #endregion

        #region "Public Methods"

        public static string Next(int size)
        {
            StringBuilder builder = new StringBuilder(size);

            for (int i = 0; i < size; i++)
            {
                bool isUpper = _random.Next() % 2 == 0;
                builder.Append(Next(isUpper));
            }

            return builder.ToString();
        }

        public static string Next(int size, bool isUpper)
        {
            StringBuilder builder = new StringBuilder(size);

            for (int i = 0; i < size; i++)
            {
                builder.Append(Next(isUpper));
            }

            return builder.ToString();
        }

        public static string Url()
        {
            return $"https://{Next(10)}.com";
        }

        public static char Next(bool isUpper)
        {
            char offset = isUpper ? 'A' : 'a';
            return (char)_random.Next(offset, offset + 26);
        }

        #endregion
    }
}