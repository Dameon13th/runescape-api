﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    [TestFixture]
    public class SolomonStoreTests
    {
        #region "Tests"
        [Test]
        public async Task Test_Correct_Response_Returned()
        {
            // Setup
            SolomonResponseRaw expected = TestData.BuildSolomonResponse();

            ISolomonStoreApi solomonStoreApi = new SolomonBuilder()
                .WithResponse(expected)
                .Finish();

            // Act
            ISolomonResponse actual = await solomonStoreApi.GetAsync();

            // Assert
            AssertAreEqual(expected, actual);
        }

        #endregion

        #region "Private Methods"

        private static void AssertAreEqual(SolomonResponseRaw expected, ISolomonResponse actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.LastUpdated, actual.LastUpdated);
            AssertAreEqual(expected.Urls, actual.Urls);
            AssertAreEqual(expected.PhraseBook, actual.PhraseBook);
            AssertAreEqual(expected.Media, actual.Media);
        }

        private static void AssertAreEqual(IUrls expected, IUrls actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Benefits, actual.Benefits);
            Assert.AreEqual(expected.Faq, actual.Faq);
            Assert.AreEqual(expected.Guide, actual.Guide);
            Assert.AreEqual(expected.Help, actual.Help);
            Assert.AreEqual(expected.HomePage, actual.HomePage);
            Assert.AreEqual(expected.Loyalty, actual.Loyalty);
            Assert.AreEqual(expected.OfferWall, actual.OfferWall);
            Assert.AreEqual(expected.Privacy, actual.Privacy);
            Assert.AreEqual(expected.Terms, actual.Terms);
        }

        private static void AssertAreEqual(Dictionary<string, string> expected, IReadOnlyDictionary<string, string> actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            foreach (KeyValuePair<string, string> keyValuePair in expected)
            {
                Assert.AreEqual(keyValuePair.Value, actual[keyValuePair.Key]);
            }
        }

        private static void AssertAreEqual(Dictionary<string, MediaWrapper> expected, IReadOnlyDictionary<long, IMedia> actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            foreach (KeyValuePair<string, MediaWrapper> valuePair in expected)
            {
                long mediaId;
                if (!long.TryParse(valuePair.Key, out mediaId))
                {
                    Assert.Fail("A media object was created without an integer for the key, Check the TestData.BuildSolomonResponse method.");
                }

                IMedia media = actual[mediaId];

                Assert.AreEqual($"{valuePair.Value.Media.Type}", media.Type);

                if (media.Type == "::mtxn_rs_shop#Json_ImageMedia")
                {
                    AssertAreEqual(valuePair.Value.Media, media as IMediaImage);
                }
                else
                {
                    AssertAreEqual(valuePair.Value.Media, media as IMediaVideo);
                }
            }
        }

        private static void AssertAreEqual(Media expected, IMediaImage actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual($"{ApiUrls.BaseUrl}{expected.ImageUrl}", actual.ImageUrl);
        }

        private static void AssertAreEqual(Media expected, IMediaVideo actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            // In an effort to save from having multiple properties that are simply image URL I decided
            // to move the image url property to the base class and simply copy over the poser url from the
            // raw response object.
            Assert.AreEqual($"{ApiUrls.BaseUrl}{expected.PosterUrl}", actual.ImageUrl);
            Assert.AreEqual($"{ApiUrls.BaseUrl}{expected.VideoUrl}", actual.VideoUrl);
        }

        #endregion
    }
}
