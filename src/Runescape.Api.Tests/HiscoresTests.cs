﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    [TestFixture]
    public class HiscoresTests
    {
        #region "Tests"

        [Test]
        [TestCase(Skill.Archaeology, 20)]
        [TestCase(Skill.Agility, 10)]
        public async Task Test_Correct_Skill_Rankings_Returned(Skill skill, int count)
        {
            // Setup
            Ranking[] expected = TestData.BuildRankings(count);

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithRankings(skill, count, expected)
                    .Finish();

            // Act
            IReadOnlyList<IRanking> actual = await hiscores.GetRankingAsync(skill, count);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(Skill.Archaeology, 20)]
        [TestCase(Skill.Agility, 10)]
        public async Task Test_Web_Exception_Returns_Null_Skill_Ranking(Skill skill, int count)
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IReadOnlyList<IRanking> actual = await hiscores.GetRankingAsync(skill, count);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(Activity.AprilFoolsCowTipping, 20)]
        [TestCase(Activity.AprilFoolsRatsKilledAfterMiniquest, 10)]
        public async Task Test_Correct_Activity_Rankings_Returned(Activity activity, int count)
        {
            // Setup
            Ranking[] expected = TestData.BuildRankings(count);

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithRankings(activity, count, expected)
                    .Finish();

            // Act
            IReadOnlyList<IRanking> actual = await hiscores.GetRankingAsync(activity, count);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(Activity.AprilFoolsCowTipping, 20)]
        [TestCase(Activity.AprilFoolsRatsKilledAfterMiniquest, 10)]
        public async Task Test_Web_Exception_Returns_Null_Activity_Ranking(Activity skill, int count)
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IReadOnlyList<IRanking> actual = await hiscores.GetRankingAsync(skill, count);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(GameMode.Normal, "Player1")]
        [TestCase(GameMode.Ironman, "Player2")]
        [TestCase(GameMode.Hardcore, "Player3")]
        public async Task Test_Correct_Player_Hiscore_Returned(GameMode gameMode, string playerName)
        {
            // Setup
            IPlayerHiscores expected = TestData.BuildHiscores(playerName);

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithPlayerHiscores(gameMode, playerName, expected)
                    .Finish();

            // Act
            IPlayerHiscores actual = await hiscores.GetHiscoresAsync(gameMode, playerName);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(GameMode.Normal, "Player1")]
        [TestCase(GameMode.Ironman, "Player2")]
        [TestCase(GameMode.Hardcore, "Player3")]
        public async Task Test_Web_Exception_Returns_Empty_Hiscores(GameMode gameMode, string playerName)
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IPlayerHiscores expected = await hiscores.GetHiscoresAsync(gameMode, playerName);

            // Assert
            Assert.NotNull(expected);
            Assert.AreEqual(expected.PlayerName, playerName);
            Assert.AreEqual(expected.Mode, gameMode);
        }

        [Test]
        [TestCase("Player1", SeasonType.Current)]
        [TestCase("Player2", SeasonType.Archived)]
        public async Task Test_Correct_Seasonal_Ranking_Returned(string playerName, SeasonType seasonType)
        {
            // Setup
            SeasonalRanking[] expected = TestData.BuildSeasonalRankings();

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithSeasonalRankings(playerName, seasonType, expected)
                    .Finish();

            // Act
            IReadOnlyList<ISeasonalRanking> actual = await hiscores.GetSeasonalRankingsAsync(playerName, seasonType);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase("Player1", SeasonType.Current)]
        [TestCase("Player2", SeasonType.Archived)]
        public async Task Test_Web_Exception_Returns_Null_Seasonal_Rankings(string playerName, SeasonType seasonType)
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IReadOnlyList<ISeasonalRanking> actual = await hiscores.GetSeasonalRankingsAsync(playerName, seasonType);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(SeasonType.Current)]
        [TestCase(SeasonType.Archived)]
        public async Task Test_Correct_Seasons_Returned(SeasonType seasonType)
        {
            // Setup
            Season[] expected = TestData.BuildSeasons();

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithSeasons(seasonType, expected)
                    .Finish();

            // Act
            IReadOnlyList<ISeason> actual = await hiscores.GetSeasonsAsync(seasonType);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(SeasonType.Current)]
        [TestCase(SeasonType.Archived)]
        public async Task Test_Web_Exception_Returns_Null_Seasons(SeasonType seasonType)
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IReadOnlyList<ISeason> actual = await hiscores.GetSeasonsAsync(seasonType);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        public async Task Test_Correct_Clan_Rankings_Returned()
        {
            // Setup
            ClanRanking[] expected = TestData.BuildClanRankings();

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithClanRankings(expected)
                    .Finish();

            // Act
            IReadOnlyList<IClanRanking> actual = await hiscores.GetClanRankingsAsync();

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        public async Task Test_Web_Exception_Returns_Null_Clan_Rankings()
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IReadOnlyList<IClanRanking> actual = await hiscores.GetClanRankingsAsync();

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase("Clan 1")]
        [TestCase("Clan 2")]
        public async Task Test_Correct_Clan_Members_Returned(string clanName)
        {
            // Setup
            ClanMember[] expected = TestData.BuildClanMembers();

            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithClanMembers(clanName, expected)
                    .Finish();

            // Act
            IReadOnlyList<IClanMember> actual = await hiscores.GetClanMembersAsync(clanName);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase("Clan 1")]
        [TestCase("Clan 2")]
        public async Task Test_Web_Exception_Returns_Empty_Clan_Members(string clanName)
        {
            // Setup
            IHiscores hiscores =
                new HiscoresBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IReadOnlyList<IClanMember> actual = await hiscores.GetClanMembersAsync(clanName);

            // Assert
            Assert.NotNull(actual);
            Assert.Zero(actual.Count);
        }

        #endregion

        #region "Private Methods"

        private static void AssertAreEqual(IReadOnlyList<IRanking> expected, IReadOnlyList<IRanking> actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                AssertAreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(IRanking expected, IRanking actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Rank, actual.Rank);
            Assert.AreEqual(expected.Score, actual.Score);
        }

        private static void AssertAreEqual(IPlayerHiscores expected, IPlayerHiscores actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(expected.Skills);
            Assert.NotNull(expected.Activities);
            Assert.NotNull(actual);
            Assert.NotNull(actual.Skills);
            Assert.NotNull(actual.Activities);
            Assert.AreEqual(actual.Skills.Count, expected.Skills.Count);
            Assert.AreEqual(actual.Activities.Count, expected.Activities.Count);
            Assert.AreEqual(expected.PlayerName, actual.PlayerName);

            foreach (Skill skill in TestData.EnumerableEnum<Skill>())
            {
                AssertAreEqual(expected.Skills[skill], actual.Skills[skill]);
            }

            foreach (Activity activity in TestData.EnumerableEnum<Activity>())
            {
                AssertAreEqual(expected.Activities[activity], actual.Activities[activity]);
            }
        }

        private static void AssertAreEqual(IHiscore expected, IHiscore actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Experience, actual.Experience);
            Assert.AreEqual(expected.Level, actual.Level);
            Assert.AreEqual(expected.Rank, actual.Rank);
        }

        private static void AssertAreEqual(IReadOnlyList<ISeasonalRanking> expected, IReadOnlyList<ISeasonalRanking> actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                AssertAreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(ISeasonalRanking expected, ISeasonalRanking actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.StartDate, actual.StartDate);
            Assert.AreEqual(expected.EndDate, actual.EndDate);
            Assert.AreEqual(expected.Rank, actual.Rank);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.FormattedScore, actual.FormattedScore);
            Assert.AreEqual(expected.Rank, actual.Rank);
            Assert.AreEqual(expected.HiscoreId, actual.HiscoreId);
        }

        private static void AssertAreEqual(IReadOnlyList<ISeason> expected, IReadOnlyList<ISeason> actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                AssertAreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(ISeason expected, ISeason actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.StartDate, actual.StartDate);
            Assert.AreEqual(expected.EndDate, actual.EndDate);
            Assert.AreEqual(expected.DaysRunning, actual.DaysRunning);
            Assert.AreEqual(expected.MonthsRunning, actual.MonthsRunning);
            Assert.AreEqual(expected.Recurrence, actual.Recurrence);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Description, actual.Description);
            Assert.AreEqual(expected.Status, actual.Status);
            Assert.AreEqual(expected.Type, actual.Type);
            Assert.AreEqual(expected.Id, actual.Id);
        }

        private static void AssertAreEqual(IReadOnlyList<IClanRanking> expected, IReadOnlyList<IClanRanking> actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                AssertAreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(IClanRanking expected, IClanRanking actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.ClanMates, actual.ClanMates);
            Assert.AreEqual(expected.TotalExperience, actual.TotalExperience);
        }

        private static void AssertAreEqual(IReadOnlyList<IClanMember> expected, IReadOnlyList<IClanMember> actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                AssertAreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(IClanMember expected, IClanMember actual)
        {
            Assert.NotNull(actual);
            Assert.NotNull(expected);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Rank, actual.Rank);
            Assert.AreEqual(expected.TotalExperience, actual.TotalExperience);
            Assert.AreEqual(expected.Kills, actual.Kills);
        }

        #endregion
    }
}
