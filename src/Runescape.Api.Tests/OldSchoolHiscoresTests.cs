﻿using System.Threading.Tasks;
using NUnit.Framework;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    [TestFixture]
    public class OldSchoolHiscoresTests
    {
        #region "Tests"

        [Test]
        [TestCase(OldSchoolGameMode.Normal, "Player1")]
        [TestCase(OldSchoolGameMode.IronMan, "Player2")]
        [TestCase(OldSchoolGameMode.HardCore, "Player3")]
        [TestCase(OldSchoolGameMode.Deadman, "Player4")]
        [TestCase(OldSchoolGameMode.Ultimate, "Player5")]
        public async Task Test_Correct_Player_Hiscore_Returned(OldSchoolGameMode gameMode, string playerName)
        {
            // Setup
            IOldSchoolPlayerHiscores expected = TestData.BuildOsHiscores(playerName);

            IOldSchoolHiscores hiscores =
                new OldSchoolHiscoresBuilder()
                    .WithHiscores(gameMode, playerName, expected)
                    .Finish();

            // Act
            IOldSchoolPlayerHiscores actual = await hiscores.GetHiscoresAsync(gameMode, playerName);

            // Assert
            AssertAreEqual(expected, actual);
        }

        #endregion

        #region "Private Methods"

        private static void AssertAreEqual(IOldSchoolPlayerHiscores expected, IOldSchoolPlayerHiscores actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(expected.Skills);
            Assert.NotNull(expected.Activities);
            Assert.NotNull(actual);
            Assert.NotNull(actual.Skills);
            Assert.NotNull(actual.Activities);
            Assert.AreEqual(actual.Skills.Count, expected.Skills.Count);
            Assert.AreEqual(actual.Activities.Count, expected.Activities.Count);
            Assert.AreEqual(expected.PlayerName, actual.PlayerName);

            foreach (Skill skill in TestData.EnumerableEnum<Skill>())
            {
                if (((int)skill) > 23)
                {
                    continue;
                }
                AssertAreEqual(expected.Skills[skill], actual.Skills[skill]);
            }

            foreach (OldSchoolActivity activity in TestData.EnumerableEnum<OldSchoolActivity>())
            {
                AssertAreEqual(expected.Activities[activity], actual.Activities[activity]);
            }
        }

        private static void AssertAreEqual(IHiscore expected, IHiscore actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Experience, actual.Experience);
            Assert.AreEqual(expected.Level, actual.Level);
            Assert.AreEqual(expected.Rank, actual.Rank);
        }

        #endregion
    }
}