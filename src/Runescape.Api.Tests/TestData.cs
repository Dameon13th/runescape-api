using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    internal static class TestData
    {
        #region "Private Members"

        private static readonly string _alpabet = "#abcdefghijklmnopqrstuvwxyz";
        private static readonly Random _random;

        #endregion

        #region "Constructors"

        static TestData()
        {
            _random = new Random();
        }

        #endregion

        #region "Public Methods"

        public static LastUpdatedResponse BuildLastUpdatedResponse(int daysSinceUpdate)
        {
            return new LastUpdatedResponse
            {
                DaysSinceUpdate = daysSinceUpdate
            };
        }

        public static GetCategoriesResponse BuildGetCategoriesResponse(GrandExchangeCategory category)
        {
            GetCategoriesResponse response = new GetCategoriesResponse
            {
                CountByLetter = new List<CountByLetter>()
            };
            foreach (char c in _alpabet)
            {
                response.CountByLetter.Add(new CountByLetter
                {
                    Letter = new string(c, 1),
                    ItemCount = _random.Next(0, 20)
                });
            }

            return response;
        }

        public static ItemsResponse BuildItemsResponse(GrandExchangeCategory category, char firstLetter, int page)
        {
            ItemsResponse response = new ItemsResponse
            {
                Items = new List<Item>(),
                Count = _random.Next(1, 100),
                Category = category,
                FirstLetter = firstLetter,
                PageNumber = page
            };

            for (int i = 0; i < _random.Next(1, 15); i++)
            {
                Item item = new Item
                {
                    Id = i,
                    IconUrl = RandomString.Url(),
                    LargeIconUrl = RandomString.Url(),
                    Type = RandomString.Url(),
                    TypeIconUrl = RandomString.Url(),
                    Name = RandomString.Url(),
                    Current = new Price
                    {
                        Trend = RandomString.Next(10),
                        Value = 100
                    },
                    Today = new Price
                    {
                        Trend = RandomString.Next(10),
                        Value = 200
                    },
                    RawIsMembers = "true"
                };
                response.Items.Add(item);
            }

            return response;
        }

        public static ItemDetailResponse BuildItemDetailResponse(int id)
        {
            return new ItemDetailResponse
            {
                Item = new ItemDetail
                {
                    Id = id,
                    IconUrl = "http://Icon Url",
                    LargeIconUrl = "http://Large Icon Url",
                    Type = "Type",
                    TypeIconUrl = "http://Type Icon Url",
                    Name = "Name",
                    Current = new Price
                    {
                        Trend = "trend",
                        Value = 100
                    },
                    Today = new Price
                    {
                        Trend = "Trend",
                        Value = 200
                    },
                    RawIsMembers = "true",
                    Day30 = new PriceChange
                    {
                        Trend = "Trend",
                        Change = "Change"
                    },
                    Day90 = new PriceChange
                    {
                        Trend = "Trend",
                        Change = "Change"
                    },
                    Day180 = new PriceChange
                    {
                        Trend = "Trend",
                        Change = "Change"
                    },
                }
            };
        }

        public static Image BuildImage(long id, ImageSize size)
        {
            // Use the id to get a starting point for stream length
            long length = id;
            if (size == ImageSize.Large) // If large multiply id by 5 because why not?
            {
                length *= 5;
            }

            // Create a buffer and fill it with random bytes
            byte[] buffer = new byte[length];
            _random.NextBytes(buffer);

            return new Image
            {
                Extension = "gif",
                Length = length,
                Stream = new MemoryStream(buffer)
            };
        }

        public static Ranking[] BuildRankings(int count)
        {
            Ranking[] rankings = new Ranking[count];

            for (int i = 0; i < count; i++)
            {
                Ranking ranking = new Ranking
                {
                    Name = "Name " + i,
                    Rank = i,
                    Score = _random.Next(200000000)
                };
                rankings[i] = ranking;
            }

            return rankings;
        }

        public static PlayerHiscores BuildHiscores(string playerName)
        {
            PlayerHiscores hiscores = new PlayerHiscores(playerName);

            foreach (Skill skill in EnumerableEnum<Skill>())
            {
                Hiscore skillHiscore = new Hiscore("")
                {
                    Rank = _random.Next(50),
                    Experience = _random.Next(5000),
                    Level = _random.Next(120)
                };
                hiscores.Skills.Add(skill, skillHiscore);
            }

            foreach (Activity activity in EnumerableEnum<Activity>())
            {
                Hiscore activityHiscore = new Hiscore("")
                {
                    Rank = _random.Next(50),
                    Experience = _random.Next(5000)
                };
                hiscores.Activities.Add(activity, activityHiscore);
            }


            return hiscores;
        }

        public static OldSchoolPLayerHiscores BuildOsHiscores(string playername)
        {
            OldSchoolPLayerHiscores hiscores = new OldSchoolPLayerHiscores(playername);

            foreach (Skill skill in EnumerableEnum<Skill>())
            {
                if (((int) skill) > 23)
                {
                    continue;
                }

                Hiscore skillHiscore = new Hiscore("")
                {
                    Rank = _random.Next(50),
                    Experience = _random.Next(5000),
                    Level = _random.Next(120)
                };
                hiscores.Skills.Add(skill, skillHiscore);
            }

            foreach (OldSchoolActivity activity in EnumerableEnum<OldSchoolActivity>())
            {
                Hiscore activityHiscore = new Hiscore("")
                {
                    Rank = _random.Next(50),
                    Experience = _random.Next(5000)
                };
                hiscores.Activities.Add(activity, activityHiscore);
            }
            return hiscores;
        }

        public static SeasonalRanking[] BuildSeasonalRankings()
        {
            SeasonalRanking[] rankings = new SeasonalRanking[_random.Next(20)];

            for (int i = 0; i < rankings.Length; i++)
            {
                SeasonalRanking ranking = new SeasonalRanking
                {
                    StartDate = $"Start Date {i}",
                    EndDate = $"End Date {i}",
                    Rank = _random.Next(50),
                    Title = $"Title {i}",
                    FormattedScore = $"Formatted Score {i}",
                    RawScore = _random.Next(100),
                    HiscoreId = _random.Next(1000)
                };
                rankings[i] = ranking;
            }
            return rankings;
        }

        public static ClanMember[] BuildClanMembers()
        {
            ClanMember[] members = new ClanMember[_random.Next(100)];

            for (int i = 0; i < members.Length; i++)
            {
                ClanMember member = new ClanMember("")
                {
                    Name = $"Name {i}",
                    Rank = $"Rank {i}",
                    TotalExperience = _random.Next(1000),
                    Kills = _random.Next(255)
                };
                members[i] = member;
            }
            return members;
        }

        public static Season[] BuildSeasons()
        {
            Season[] seasons = new Season[_random.Next(20)];

            for (int i = 0; i < seasons.Length; i++)
            {
                Season season = new Season
                {
                    StartDate = $"Start Date {i}",
                    EndDate = $"End Date {i}",
                    DaysRunning = _random.Next(10),
                    MonthsRunning = _random.Next(10),
                    Recurrence = _random.Next(10),
                    Title = $"Title {i}",
                    Name = $"Name {i}",
                    Description = $"Description {i}",
                    Status = $"Status {i}",
                    Type = $"Type {i}",
                    Id = _random.Next(1000)
                };
                seasons[i] = season;
            }

            return seasons;
        }

        public static ClanRanking[] BuildClanRankings()
        {
            ClanRanking[] rankings = new ClanRanking[_random.Next(20)];

            for (int i = 0; i < rankings.Length; i++)
            {
                ClanRanking ranking = new ClanRanking
                {
                    Name = $"Name {i}",
                    ClanMates = _random.Next(500),
                    TotalExperience = _random.Next(10000)
                };
                rankings[i] = ranking;
            }

            return rankings;
        }

        public static BeastData BuildBeastData(long beastId)
        {
            return new BeastData
            {
                Name = $"Beast {beastId}",
                Id = beastId,
                IsMembers = true,
                Weakness = Weakness.Air.ToString(),
                Level = _random.Next(150),
                LifePoints = _random.Next(9999, 10000),
                Defense = _random.Next(99),
                Attack = _random.Next(99),
                Magic = _random.Next(99),
                Ranged = _random.Next(99),
                ExperiencePoints = _random.Next(1000),
                SlayerLevel = _random.Next(120),
                Size = _random.Next(3),
                IsAttackable = true,
                IsAggressive = true,
                Description = $"Description of beast {beastId}",
                Area = new List<string>
                {
                    "Area 1",
                    "Area 2"
                },
                Animations = new Dictionary<string, long>
                {
                    {"Animation 1", _random.Next(1000)},
                    {"Animation 2", _random.Next(1000)}
                }
            };
        }

        public static BeastSearchResult[] BuildBeastSearchResults()
        {
            BeastSearchResult[] searchResults = new BeastSearchResult[_random.Next(10)];

            for (int i = 0; i < searchResults.Length; i++)
            {
                BeastSearchResult searchResult = new BeastSearchResult
                {
                    Id = i,
                    Name = $"Search Result {i}"
                };
                searchResults[i] = searchResult;
            }

            return searchResults;
        }

        public static string[] BuildAreaNames()
        {
            string[] areas = new string[_random.Next(100)];
            for (int i = 0; i < areas.Length; i++)
            {
                areas[i] = $"Area {i}";
            }
            return areas;
        }

        public static Dictionary<string, int> BuildSlayerCategories()
        {
            Dictionary<string, int> categories = new Dictionary<string, int>();

            for (int i = 0; i < _random.Next(20); i++)
            {
                categories[$"Category {i}"] = i;
            }

            return categories;
        }

        public static Dictionary<string, int> BuildWeaknesses()
        {
            Dictionary<string, int> categories = new Dictionary<string, int>();

            for (int i = 0; i < _random.Next(20); i++)
            {
                categories[$"Weakness {i}"] = i;
            }

            return categories;
        }

        public static SolomonResponseRaw BuildSolomonResponse()
        {
            SolomonResponseRaw raw = new SolomonResponseRaw
            {
                Type = RandomString.Next(10),
                Version = _random.Next(100000, 1000000),
                Urls = new Urls
                {
                    Benefits = RandomString.Url(),
                    Faq = RandomString.Url(),
                    Guide = RandomString.Url(),
                    Help = RandomString.Url(),
                    HomePage = RandomString.Url(),
                    Loyalty = RandomString.Url(),
                    OfferWall = RandomString.Url(),
                    Privacy = RandomString.Url(),
                    Terms = RandomString.Url(),
                },
                PhraseBook = new Dictionary<string, string>(),
                Media = new Dictionary<string, MediaWrapper>()
            };

            for (int i = 0; i < _random.Next(100); i++)
            {
                raw.PhraseBook[RandomString.Next(7)] = RandomString.Next(15);

                MediaWrapper wrapper = new MediaWrapper
                {
                    Media = new Media
                    {
                        ImageUrl = RandomString.Url(),
                        PosterUrl = RandomString.Url(),
                        VideoUrl = RandomString.Url()
                    }
                };

                wrapper.Media.Type = i % 2 == 0 ?
                    "::mtxn_rs_shop#Json_ImageMedia" : 
                    "::mtxn_rs_shop#Json_VideoMedia";
                raw.Media[i.ToString()] = wrapper;
            }

            return raw;
        }

        #endregion

        public static IEnumerable<T> EnumerableEnum<T>()
        {
            return Enum.GetValues(typeof(T)).OfType<T>();
        }
    }
}