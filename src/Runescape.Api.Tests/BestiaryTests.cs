﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    [TestFixture]
    public class BestiaryTests
    {
        #region "Tests"

        [Test]
        [TestCase(101)]
        [TestCase(102)]
        public async Task Test_Correct_BeastData_Returned(long beastId)
        {
            // Set up
            BeastData expected = TestData.BuildBeastData(beastId);

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithBeastData(beastId, expected)
                    .Finish();

            // Act
            IBeastData actual = await bestiary.GetBeastDataAsync(beastId);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(int.MaxValue)]
        public async Task Test_Beast_Not_Found_Returns_Null(int badBeastId)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithBadBeastId(badBeastId)
                    .Finish();

            // Act
            IBeastData actual = await bestiary.GetBeastDataAsync(badBeastId);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(101)]
        public async Task Test_Web_Exception_Returns_Null_Beast(int beastId)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IBeastData actual = await bestiary.GetBeastDataAsync(beastId);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase("Cow")]
        [TestCase("Cow", "Imp")]
        public async Task Test_Correct_Search_Results_Returned(params string[] terms)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithSearchTerms(expected, terms)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.SearchBeastsAsync(terms);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase("Cow", "Imp")]
        public async Task Test_Web_Exception_Returns_Null_Search(params string[] terms)
        {
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.SearchBeastsAsync(terms);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase('a')]
        [TestCase('A')]
        [TestCase('b')]
        [TestCase('B')]
        public async Task Test_Correct_Bestiray_Names_Returned(char firstLetter)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithBestiaryNames(firstLetter, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.SearchBeastsAsync(firstLetter);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase('a')]
        public async Task Test_Web_Exception_Returns_Null_Beast_Name(char firstLetter)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.SearchBeastsAsync(firstLetter);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        public async Task Test_Correct_Area_Names_Returned()
        {
            // Setup
            string[] expected = TestData.BuildAreaNames();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithAreaNames(expected)
                    .Finish();

            // Act
            IReadOnlyList<string> actual = await bestiary.GetAreaNamesAsync();

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        public async Task Test_Web_Exception_Returns_Null_Areas()
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<string> actual = await bestiary.GetAreaNamesAsync();

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase("Area 1")]
        [TestCase("Area 2")]
        public async Task Test_Correct_Area_Beast_Returned(string areaName)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithAreaBeast(areaName, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByAreaAsync(areaName);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase("Area 1")]
        public async Task Test_Web_Exception_Returns_Null_Area_Beasts(string areaName)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByAreaAsync(areaName);

            // Assert
            Assert.IsNull(actual);
        }


        [Test]
        public async Task Test_Correct_Slayer_Categories_Returned()
        {
            // Set up
            Dictionary<string, int> expected = TestData.BuildSlayerCategories();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithSlayerCategories(expected)
                    .Finish();

            // Act
            IReadOnlyList<ISlayerCategory> actual = await bestiary.GetSlayerCategoriesAsync();

            // Assert
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            foreach (ISlayerCategory slayerCategory in actual)
            {
                Assert.AreEqual(expected[slayerCategory.Name], slayerCategory.Id);
            }
        }

        [Test]
        public async Task Test_Web_Exception_Returns_Null_Slayer_Categories()
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<ISlayerCategory> actual = await bestiary.GetSlayerCategoriesAsync();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.Count, 0);
        }

        [Test]
        [TestCase(101)]
        [TestCase(102)]
        public async Task Test_Correct_Slayer_Beast_Returned_By_Int(int slayerCategoryId)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithSlayerBeast(slayerCategoryId, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsBySlayerCategory(slayerCategoryId);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(SlayerCategory.AberrantSpectres)]
        [TestCase(SlayerCategory.AbyssalDemons)]
        public async Task Test_Correct_Slayer_Beast_Returned_By_Enum(SlayerCategory slayerCategory)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithSlayerBeast((int)slayerCategory, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsBySlayerCategory(slayerCategory);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(101)]
        [TestCase(102)]
        public async Task Test_Web_Exception_Returns_Null_Slayer_Beasts_By_Int(int slayerCategoryId)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsBySlayerCategory(slayerCategoryId);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(SlayerCategory.AberrantSpectres)]
        [TestCase(SlayerCategory.AbyssalDemons)]
        public async Task Test_Web_Exception_Returns_Null_Slayer_Beasts_By_Enum(SlayerCategory slayerCategory)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsBySlayerCategory(slayerCategory);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        public async Task Test_Correct_Weaknesses_Returned()
        {
            // Set up
            Dictionary<string, int> expected = TestData.BuildWeaknesses();

            IBestiary bestiary = new BestiaryBuilder()
                .WithWeaknesses(expected)
                .Finish();

            // Act
            IReadOnlyList<IWeakness> actual = await bestiary.GetWeaknessesAsync();

            // Assert
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            foreach (IWeakness weakness in actual)
            {
                Assert.AreEqual(expected[weakness.Name], weakness.Id);
            }
        }

        [Test]
        public async Task Test_Web_Exception_Returns_Null_Weaknesses()
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IWeakness> actual = await bestiary.GetWeaknessesAsync();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count);
        }

        [Test]
        [TestCase(101)]
        [TestCase(102)]
        public async Task Test_Correct_Weakness_Beast_Returned_By_Int(int weaknessId)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWeaknessBeasts(weaknessId, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByWeaknessAsync(weaknessId);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(Weakness.Air)]
        [TestCase(Weakness.Arrow)]
        public async Task Test_Correct_Weakness_Beast_Returned_By_Enum(Weakness weakness)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWeaknessBeasts((int)weakness, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByWeaknessAsync(weakness);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(101)]
        [TestCase(102)]
        public async Task Test_Web_Exception_Returns_Null_Weakness_Beasts_By_Int(int weaknessId)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByWeaknessAsync(weaknessId);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(Weakness.Air)]
        [TestCase(Weakness.Arrow)]
        public async Task Test_Web_Exception_Returns_Null_Weakness_Beasts_By_Enum(Weakness weakness)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByWeaknessAsync(weakness);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(1, 10)]
        [TestCase(11, 20)]
        public async Task Test_Correct_Level_Group_Returned(int low, int high)
        {
            // Set up
            BeastSearchResult[] expected = TestData.BuildBeastSearchResults();

            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithLevelGroup(low, high, expected)
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByLevelGroupAsync(low, high);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(1, 10)]
        [TestCase(11, 20)]
        public async Task Test_Web_Exception_Returns_Null_Level_Group(int low, int high)
        {
            // Set up
            IBestiary bestiary =
                new BestiaryBuilder()
                    .WithWebException()
                    .Finish();

            // Act
            IReadOnlyList<IBeastSearchResult> actual = await bestiary.GetBeastsByLevelGroupAsync(low, high);

            // Assert
            Assert.IsNull(actual);
        }

        #endregion

        #region "Private Methods"

        private static void AssertAreEqual(IBeastData expected, IBeastData actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.IsMembers, actual.IsMembers);
            Assert.AreEqual(expected.Weakness, actual.Weakness);
            Assert.AreEqual(expected.Level, actual.Level);
            Assert.AreEqual(expected.LifePoints, actual.LifePoints);
            Assert.AreEqual(expected.Defense, actual.Defense);
            Assert.AreEqual(expected.Attack, actual.Attack);
            Assert.AreEqual(expected.Magic, actual.Magic);
            Assert.AreEqual(expected.Ranged, actual.Ranged);
            Assert.AreEqual(expected.ExperiencePoints, actual.ExperiencePoints);
            Assert.AreEqual(expected.SlayerLevel, actual.SlayerLevel);
            Assert.AreEqual(expected.SlayerCategory, actual.SlayerCategory);
            Assert.AreEqual(expected.Size, actual.Size);
            Assert.AreEqual(expected.IsAttackable, actual.IsAttackable);
            Assert.AreEqual(expected.IsAggressive, actual.IsAggressive);
            Assert.AreEqual(expected.IsPosonous, actual.IsPosonous);
            Assert.AreEqual(expected.Description, actual.Description);
            AssertAreEqual(expected.Area, actual.Area);
            AssertAreEqual(expected.Animations, actual.Animations);
        }

        private static void AssertAreEqual(IReadOnlyList<string> expected, IReadOnlyList<string> actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(IReadOnlyList<IBeastSearchResult> expected, IReadOnlyList<IBeastSearchResult> actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                AssertAreEqual(expected[i], actual[i]);
            }
        }

        private static void AssertAreEqual(IBeastSearchResult expected, IBeastSearchResult actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
        }

        private static void AssertAreEqual(IReadOnlyDictionary<string, long> expected, IReadOnlyDictionary<string, long> actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            foreach (string key in expected.Keys)
            {
                Assert.AreEqual(expected[key], actual[key]);
            }
        }

        #endregion
    }
}
