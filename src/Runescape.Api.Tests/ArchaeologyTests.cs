﻿using NUnit.Framework;
using Runescape.Api.Archaeology;

namespace Runescape.Api.Tests
{
    [TestFixture]
    [Description("A collection of test that will validate the Atifacts.Required materials.")]
    public class ArchaeologyTests
    {
        [Test]
        public void Armadylean_I_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.IkovianGerege);
            artifacts.Add(Artifacts.ToyGlider);
            artifacts.Add(Artifacts.ToyWarGolem);
            artifacts.Add(Artifacts.AvianSongEggPlayer);
            artifacts.Add(Artifacts.KeshikDrum);
            artifacts.Add(Artifacts.MorinKhuur);
            artifacts.Add(Artifacts.AviansieDreamcoat);
            artifacts.Add(Artifacts.CeremonialPlume);
            artifacts.Add(Artifacts.PeacockingParasol);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 16);
            requiredMaterials.Add(Materials.Diamond, 1);
            requiredMaterials.Add(Materials.Goldrune, 34);
            requiredMaterials.Add(Materials.StormguardSteel, 72);
            requiredMaterials.Add(Materials.ArmadyleanYellow, 148);
            requiredMaterials.Add(Materials.ThirdAgeIron, 72);
            requiredMaterials.Add(Materials.Clockwork, 1);
            requiredMaterials.Add(Materials.PhoenixFeather, 1);
            requiredMaterials.Add(Materials.AnimalFurs, 38);
            requiredMaterials.Add(Materials.SamiteSilk, 60);
            requiredMaterials.Add(Materials.WhiteOak, 132);
            requiredMaterials.Add(Materials.WingsofWar, 46);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("54366.9", expectedExp.ToString());
        }

        [Test]
        public void Armadylean_II_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.HawkeyeLensMultiVisionScope);
            artifacts.Add(Artifacts.Talon3RazorWing);
            artifacts.Add(Artifacts.PrototypeGravimeter);
            artifacts.Add(Artifacts.SongbirdRecorder);
            artifacts.Add(Artifacts.DayguardShield);
            artifacts.Add(Artifacts.StormguardGerege);
            artifacts.Add(Artifacts.GolemHeart);
            artifacts.Add(Artifacts.GolemInstruction);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 20);
            requiredMaterials.Add(Materials.Diamond, 1);
            requiredMaterials.Add(Materials.Goldrune, 20);
            requiredMaterials.Add(Materials.StormguardSteel, 156);
            requiredMaterials.Add(Materials.Quintessence, 104);
            requiredMaterials.Add(Materials.BlackMushroomInk, 1);
            requiredMaterials.Add(Materials.Vellum, 44);
            requiredMaterials.Add(Materials.Soapstone, 16);
            requiredMaterials.Add(Materials.Orthenglass, 86);
            requiredMaterials.Add(Materials.Rope, 1);
            requiredMaterials.Add(Materials.WhiteOak, 20);
            requiredMaterials.Add(Materials.ThirdAgeIron, 26);
            requiredMaterials.Add(Materials.AetheriumAlloy, 74);
            requiredMaterials.Add(Materials.WingsofWar, 90);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("102111.2", expectedExp.ToString());
        }

        [Test]
        public void Armadylean_III_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.BlackfireLance);
            artifacts.Add(Artifacts.NightguardShield);
            artifacts.Add(Artifacts.FlatCap);
            artifacts.Add(Artifacts.NightOwnFlightGoggles);
            artifacts.Add(Artifacts.PrototypeGodBow);
            artifacts.Add(Artifacts.PrototypeGodStaff);
            artifacts.Add(Artifacts.PrototypeGodSword);
            artifacts.Add(Artifacts.ChuluuStone);
            artifacts.Add(Artifacts.QuintessenceCounter);
            artifacts.Add(Artifacts.SphericalAstrolabe);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 40);
            requiredMaterials.Add(Materials.Goldrune, 58);
            requiredMaterials.Add(Materials.StormguardSteel, 70);
            requiredMaterials.Add(Materials.AetheriumAlloy, 286);
            requiredMaterials.Add(Materials.Soapstone, 40);
            requiredMaterials.Add(Materials.Orthenglass, 78);
            requiredMaterials.Add(Materials.WhiteOak, 70);
            requiredMaterials.Add(Materials.ArmadyleanYellow, 144);
            requiredMaterials.Add(Materials.SamiteSilk, 54);
            requiredMaterials.Add(Materials.Quintessence, 198);
            requiredMaterials.Add(Materials.WingsofWar, 138);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("340000", expectedExp.ToString());
        }

        [Test]
        public void Bandosian_I_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.EkeleshuunBinderMask);
            artifacts.Add(Artifacts.NarogoshuunHobdaGobBall);
            artifacts.Add(Artifacts.RekeshuunWarTether);
            artifacts.Add(Artifacts.OgreKyzajAxe);
            artifacts.Add(Artifacts.OrkCleaverSword);
            artifacts.Add(Artifacts.ThorobshuunBattleStandard);
            artifacts.Add(Artifacts.YurkolgokhStinkGrenade);
            artifacts.Add(Artifacts.HighPriestCrozier);
            artifacts.Add(Artifacts.HighPriestMitre);
            artifacts.Add(Artifacts.HighPriestOrb);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 26);
            requiredMaterials.Add(Materials.WeaponPoison, 1);
            requiredMaterials.Add(Materials.Goldrune, 56);
            requiredMaterials.Add(Materials.FossilisedBone, 60);
            requiredMaterials.Add(Materials.MalachiteGreen, 114);
            requiredMaterials.Add(Materials.Vellum, 24);
            requiredMaterials.Add(Materials.SamiteSilk, 48);
            requiredMaterials.Add(Materials.YubiuskClay, 38);
            requiredMaterials.Add(Materials.VulcanisedRubber, 118);
            requiredMaterials.Add(Materials.WarforgedBronze, 84);
            requiredMaterials.Add(Materials.MarkOfTheKyzaj, 146);
            requiredMaterials.Add(Materials.WhiteOak, 16);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("80811.3", expectedExp.ToString());
        }

        [Test]
        public void Bandosian_II_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.BeastkeeperHelm);
            artifacts.Add(Artifacts.IdithuunHornRing);
            artifacts.Add(Artifacts.NosorogSculpture);
            artifacts.Add(Artifacts.OurgMegahitter);
            artifacts.Add(Artifacts.OurgTowerGoblinCowerShield);
            artifacts.Add(Artifacts.GaragorshuunAnchor);
            artifacts.Add(Artifacts.DorgeshuunSpear);
            artifacts.Add(Artifacts.ForgedInWarSculpture);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 42);
            requiredMaterials.Add(Materials.AnimalFurs, 20);
            requiredMaterials.Add(Materials.FossilisedBone, 24);
            requiredMaterials.Add(Materials.MalachiteGreen, 46);
            requiredMaterials.Add(Materials.Emerald, 1);
            requiredMaterials.Add(Materials.ThirdAgeIron, 56);
            requiredMaterials.Add(Materials.Orthenglass, 26);
            requiredMaterials.Add(Materials.YubiuskClay, 112);
            requiredMaterials.Add(Materials.VulcanisedRubber, 68);
            requiredMaterials.Add(Materials.WarforgedBronze, 178);
            requiredMaterials.Add(Materials.MarkOfTheKyzaj, 46);
            requiredMaterials.Add(Materials.WhiteOak, 82);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("124833.2", expectedExp.ToString());
        }

        [Test]
        public void Bandosian_III_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.HuzamogaarbChoasCrown);
            artifacts.Add(Artifacts.SaragorgakStarCrown);
            artifacts.Add(Artifacts.DrogokishuunHookSword);
            artifacts.Add(Artifacts.HobgoblinMansticker);
            artifacts.Add(Artifacts.KaliKraChieftainCrown);
            artifacts.Add(Artifacts.KaliKraMace);
            artifacts.Add(Artifacts.KaliKraWarHorn);
            artifacts.Add(Artifacts.DaBossManSculture);
            artifacts.Add(Artifacts.HorogotharCookingPot);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.VulcanisedRubber, 86);
            requiredMaterials.Add(Materials.AnimalFurs, 100);
            requiredMaterials.Add(Materials.FossilisedBone, 160);
            requiredMaterials.Add(Materials.MalachiteGreen, 118);
            requiredMaterials.Add(Materials.Soapstone, 84);
            requiredMaterials.Add(Materials.ThirdAgeIron, 112);
            requiredMaterials.Add(Materials.YubiuskClay, 176);
            requiredMaterials.Add(Materials.EyeofDagon, 20);
            requiredMaterials.Add(Materials.WarforgedBronze, 198);
            requiredMaterials.Add(Materials.StarofSaradomin, 20);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("313666.5", expectedExp.ToString());
        }

        [Test]
        public void Dragonkin_I_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.Pasaha);
            artifacts.Add(Artifacts.RitualBell);
            artifacts.Add(Artifacts.Kilaya);
            artifacts.Add(Artifacts.Vazara);
            artifacts.Add(Artifacts.DeathMask);
            artifacts.Add(Artifacts.DragonkinCalender);
            artifacts.Add(Artifacts.DragonkinStaff);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.Soapstone, 34);
            requiredMaterials.Add(Materials.Goldrune, 106);
            requiredMaterials.Add(Materials.CarbonBlack, 28);
            requiredMaterials.Add(Materials.Orgone, 146);
            requiredMaterials.Add(Materials.CompassRose, 168);
            requiredMaterials.Add(Materials.Felt, 40);
            requiredMaterials.Add(Materials.DragonMetal, 76);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("104277.8", expectedExp.ToString());
        }

        [Test]
        public void Dragonkin_II_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.DragonScalpel);
            artifacts.Add(Artifacts.ProtectiveGoggles);
            artifacts.Add(Artifacts.DragonBurner);
            artifacts.Add(Artifacts.OrthenglassFlask);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.Orgone, 42);
            requiredMaterials.Add(Materials.Orthenglass, 112);
            requiredMaterials.Add(Materials.Felt, 84);
            requiredMaterials.Add(Materials.DragonMetal, 138);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("81666.6", expectedExp.ToString());
        }

        [Test]
        public void Dragonkin_III_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.MeditationPipe);
            artifacts.Add(Artifacts.PersonalTotem);
            artifacts.Add(Artifacts.SingingBowl);
            artifacts.Add(Artifacts.LingamStone);
            artifacts.Add(Artifacts.MasterControl);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.Orgone, 242);
            requiredMaterials.Add(Materials.CompassRose, 102);
            requiredMaterials.Add(Materials.CarbonBlack, 88);
            requiredMaterials.Add(Materials.DragonMetal, 80);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("133666.7", expectedExp.ToString());
        }

        [Test]
        public void Dragonkin_IV_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.XoloHardHat);
            artifacts.Add(Artifacts.XoloPickaxe);
            artifacts.Add(Artifacts.XoloShield);
            artifacts.Add(Artifacts.XoloSpear);
            artifacts.Add(Artifacts.GoldDish);
            artifacts.Add(Artifacts.RakshaIdol);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.Orgone, 198);
            requiredMaterials.Add(Materials.Felt, 42);
            requiredMaterials.Add(Materials.Goldrune, 268);
            requiredMaterials.Add(Materials.DragonMetal, 288);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("253333.4", expectedExp.ToString());
        }

        [Test]
        public void Saradominist_I_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.FryingPan);
            artifacts.Add(Artifacts.HallowedLantern);
            artifacts.Add(Artifacts.CeremonialUnicornOrnament);
            artifacts.Add(Artifacts.CeremonialUnicornSaddle);
            artifacts.Add(Artifacts.EverlightHarp);
            artifacts.Add(Artifacts.EverlightTrumpet);
            artifacts.Add(Artifacts.EverlightViolin);
            artifacts.Add(Artifacts.FoldedArmFigurineFemale);
            artifacts.Add(Artifacts.FoldedArmFigurineMale);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 24);
            requiredMaterials.Add(Materials.Keramos, 50);
            requiredMaterials.Add(Materials.WhiteCandle, 1);
            requiredMaterials.Add(Materials.EverlightSilvthril, 58);
            requiredMaterials.Add(Materials.CobaltBlue, 42);
            requiredMaterials.Add(Materials.ThirdAgeIron, 40);
            requiredMaterials.Add(Materials.Goldrune, 72);
            requiredMaterials.Add(Materials.StarofSaradomin, 16);
            requiredMaterials.Add(Materials.SamiteSilk, 16);
            requiredMaterials.Add(Materials.WhiteMarble, 84);
            requiredMaterials.Add(Materials.WhiteOak, 42);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("14349.7", expectedExp.ToString());
        }

        [Test]
        public void Saradominist_II_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.DominionDiscus);
            artifacts.Add(Artifacts.DominionJavelin);
            artifacts.Add(Artifacts.DominionPelteShield);
            artifacts.Add(Artifacts.BronzeDominionMedal);
            artifacts.Add(Artifacts.SilverDominionMedal);
            artifacts.Add(Artifacts.DominionTorch);
            artifacts.Add(Artifacts.DecorativeVase);
            artifacts.Add(Artifacts.KantharosCup);
            artifacts.Add(Artifacts.PateraBowl);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.SilverBar, 1);
            requiredMaterials.Add(Materials.Keramos, 102);
            requiredMaterials.Add(Materials.Sapphire, 2);
            requiredMaterials.Add(Materials.EverlightSilvthril, 122);
            requiredMaterials.Add(Materials.WhiteMarble, 36);
            requiredMaterials.Add(Materials.CobaltBlue, 30);
            requiredMaterials.Add(Materials.ThirdAgeIron, 30);
            requiredMaterials.Add(Materials.Orthenglass, 48);
            requiredMaterials.Add(Materials.Goldrune, 42);
            requiredMaterials.Add(Materials.SamiteSilk, 28);
            requiredMaterials.Add(Materials.BronzeBar, 1);
            requiredMaterials.Add(Materials.StarofSaradomin, 132);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("36399.9", expectedExp.ToString());
        }

        [Test]
        public void Saradominist_III_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.DominarianDevice);
            artifacts.Add(Artifacts.FishingTrident);
            artifacts.Add(Artifacts.Amphora);
            artifacts.Add(Artifacts.RodOfAsclepius);
            artifacts.Add(Artifacts.KopisDagger);
            artifacts.Add(Artifacts.XiphosShortSword);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.WhiteMarble, 30);
            requiredMaterials.Add(Materials.LeatherScraps, 88);
            requiredMaterials.Add(Materials.Keramos, 68);
            requiredMaterials.Add(Materials.Goldrune, 48);
            requiredMaterials.Add(Materials.StarofSaradomin, 46);
            requiredMaterials.Add(Materials.Clockwork, 1);
            requiredMaterials.Add(Materials.ThirdAgeIron, 52);
            requiredMaterials.Add(Materials.EverlightSilvthril, 160);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("77778", expectedExp.ToString());
        }

        [Test]
        public void Saradominist_IV_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.ThePrideOfPadosanPainting);
            artifacts.Add(Artifacts.HallowedBeTheEverlightPainting);
            artifacts.Add(Artifacts.TheLordOfLightPainting);
            artifacts.Add(Artifacts.TheEnlightendSoulSCroll);
            artifacts.Add(Artifacts.TheEudoxianElementTablet);
            artifacts.Add(Artifacts.DoruSpear);
            artifacts.Add(Artifacts.KontosLane);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.CobaltBlue, 156);
            requiredMaterials.Add(Materials.Vellum, 108);
            requiredMaterials.Add(Materials.Goldrune, 50);
            requiredMaterials.Add(Materials.EverlightSilvthril, 140);
            requiredMaterials.Add(Materials.WhiteMarble, 60);
            requiredMaterials.Add(Materials.SamiteSilk, 110);
            requiredMaterials.Add(Materials.StarofSaradomin, 50);
            requiredMaterials.Add(Materials.WhiteOak, 110);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("216166.8", expectedExp.ToString());
        }

        [Test]
        public void Zamorakian_I_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.HookahPipe);
            artifacts.Add(Artifacts.OpulentWineGoblet);
            artifacts.Add(Artifacts.CrestOfDagon);
            artifacts.Add(Artifacts.DisorderPainting);
            artifacts.Add(Artifacts.ImpMask);
            artifacts.Add(Artifacts.LesserDemonMask);
            artifacts.Add(Artifacts.GreaterDemonMask);
            artifacts.Add(Artifacts.OrderOfDisRobes);
            artifacts.Add(Artifacts.RitualDagger);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 22);
            requiredMaterials.Add(Materials.Goldrune, 58);
            requiredMaterials.Add(Materials.Ruby, 1);
            requiredMaterials.Add(Materials.ThirdAgeIron, 30);
            requiredMaterials.Add(Materials.HellfireMetal, 24);
            requiredMaterials.Add(Materials.Demonhide, 36);
            requiredMaterials.Add(Materials.Vellum, 6);
            requiredMaterials.Add(Materials.Orthenglass, 26);
            requiredMaterials.Add(Materials.ChaoticBrimstone, 26);
            requiredMaterials.Add(Materials.EyeofDagon, 14);
            requiredMaterials.Add(Materials.SamiteSilk, 22);
            requiredMaterials.Add(Materials.CadmiumRed, 30);
            requiredMaterials.Add(Materials.WhiteOak, 6);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("6371.4", expectedExp.ToString());
        }

        [Test]
        public void Zamorakian_II_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.BrandingIron);
            artifacts.Add(Artifacts.Manacles);
            artifacts.Add(Artifacts.TheLakeOfFirePainting);
            artifacts.Add(Artifacts.LustMetalSculpture);
            artifacts.Add(Artifacts.ChaosStar);
            artifacts.Add(Artifacts.SpikedDogCollar);
            artifacts.Add(Artifacts.LarupiaTrophy);
            artifacts.Add(Artifacts.LionTrophy);
            artifacts.Add(Artifacts.SheWolfTrophy);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.ChaoticBrimstone, 88);
            requiredMaterials.Add(Materials.AnimalFurs, 84);
            requiredMaterials.Add(Materials.Goldrune, 24);
            requiredMaterials.Add(Materials.Orthenglass, 26);
            requiredMaterials.Add(Materials.HellfireMetal, 56);
            requiredMaterials.Add(Materials.LeatherScraps, 24);
            requiredMaterials.Add(Materials.Vellum, 10);
            requiredMaterials.Add(Materials.ThirdAgeIron, 68);
            requiredMaterials.Add(Materials.Ruby, 1);
            requiredMaterials.Add(Materials.EyeofDagon, 50);
            requiredMaterials.Add(Materials.SamiteSilk, 10);
            requiredMaterials.Add(Materials.CadmiumRed, 88);
            requiredMaterials.Add(Materials.WhiteOak, 36);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("40133.3", expectedExp.ToString());
        }

        [Test]
        public void Zamorakian_III_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.TormentMetalSculpture);
            artifacts.Add(Artifacts.PandemoniumTapestry);
            artifacts.Add(Artifacts.HellfireHaladie);
            artifacts.Add(Artifacts.HellfireKatar);
            artifacts.Add(Artifacts.HellfireZaghnal);
            artifacts.Add(Artifacts.PossessionMetalSculpture);
            artifacts.Add(Artifacts.Trishula);
            artifacts.Add(Artifacts.TsutsarothPiercing);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 60);
            requiredMaterials.Add(Materials.HellfireMetal, 262);
            requiredMaterials.Add(Materials.ChaoticBrimstone, 60);
            requiredMaterials.Add(Materials.Vellum, 12);
            requiredMaterials.Add(Materials.ThirdAgeIron, 110);
            requiredMaterials.Add(Materials.Orthenglass, 26);
            requiredMaterials.Add(Materials.EyeofDagon, 74);
            requiredMaterials.Add(Materials.SamiteSilk, 12);
            requiredMaterials.Add(Materials.CadmiumRed, 66);
            requiredMaterials.Add(Materials.WhiteOak, 38);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("141000", expectedExp.ToString());
        }

        [Test]
        public void Zamorakian_IV_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.ChaosElementalTrophy);
            artifacts.Add(Artifacts.ViriusTrophy);
            artifacts.Add(Artifacts.TsutsarothHelm);
            artifacts.Add(Artifacts.TsutsarothPauldron);
            artifacts.Add(Artifacts.TsutsarothUrumi);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.Demonhide, 44);
            requiredMaterials.Add(Materials.ThirdAgeIron, 40);
            requiredMaterials.Add(Materials.Orthenglass, 34);
            requiredMaterials.Add(Materials.Goldrune, 90);
            requiredMaterials.Add(Materials.EyeofDagon, 120);
            requiredMaterials.Add(Materials.ChaoticBrimstone, 52);
            requiredMaterials.Add(Materials.HellfireMetal, 170);
            requiredMaterials.Add(Materials.WhiteOak, 64);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("182000", expectedExp.ToString());
        }

        [Test]
        public void Zarosian_I_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.VenatorDagger);
            artifacts.Add(Artifacts.VenatorLightCrossBow);
            artifacts.Add(Artifacts.PrimisElementisStandard);
            artifacts.Add(Artifacts.LegionaryGladius);
            artifacts.Add(Artifacts.LegionarySquareShield);
            artifacts.Add(Artifacts.ZarosEffigy);
            artifacts.Add(Artifacts.ZarosianTrainingDummy);
            artifacts.Add(Artifacts.LegatusMaximusFigurine);
            artifacts.Add(Artifacts.SolemInUmbraPainting);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.ImperialSteel, 24);
            requiredMaterials.Add(Materials.TyrianPurple, 14);
            requiredMaterials.Add(Materials.ThirdAgeIron, 74);
            requiredMaterials.Add(Materials.ZarosianInsignia, 68);
            requiredMaterials.Add(Materials.Goldrune, 8);
            requiredMaterials.Add(Materials.SamiteSilk, 32);
            requiredMaterials.Add(Materials.AncientVis, 10);
            requiredMaterials.Add(Materials.WhiteOak, 34);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("4271.8", expectedExp.ToString());
        }

        [Test]
        public void Zarosian_II_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.AncientTimepiece);
            artifacts.Add(Artifacts.LegatusPendant);
            artifacts.Add(Artifacts.InciteFearSpellScroll);
            artifacts.Add(Artifacts.PontifexSignetRing);
            artifacts.Add(Artifacts.CeremonialMace);
            artifacts.Add(Artifacts.PontifexMaximusFigurine);
            artifacts.Add(Artifacts.ConsensusAdIdemPainting);
            artifacts.Add(Artifacts.PontifexCenser);
            artifacts.Add(Artifacts.PontifexCrozier);
            artifacts.Add(Artifacts.PontifexMitre);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.ImperialSteel, 56);
            requiredMaterials.Add(Materials.TyrianPurple, 50);
            requiredMaterials.Add(Materials.ThirdAgeIron, 72);
            requiredMaterials.Add(Materials.ZarosianInsignia, 64);
            requiredMaterials.Add(Materials.Goldrune, 168);
            requiredMaterials.Add(Materials.SamiteSilk, 42);
            requiredMaterials.Add(Materials.AncientVis, 126);
            requiredMaterials.Add(Materials.WhiteOak, 10);
            requiredMaterials.Add(Materials.BloodofOrcus, 18);
            requiredMaterials.Add(Materials.Dragonstone, 4);
            requiredMaterials.Add(Materials.Vellum, 20);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("46199.9", expectedExp.ToString());
        }

        [Test]
        public void Zarosian_III_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.ExsanguinateSpellScroll);
            artifacts.Add(Artifacts.NecromanticFocus);
            artifacts.Add(Artifacts.ZarosianEwer);
            artifacts.Add(Artifacts.ZarosianStein);
            artifacts.Add(Artifacts.SmokeCloudSpellScroll);
            artifacts.Add(Artifacts.VigoremVial);
            artifacts.Add(Artifacts.AncientMagicTablet);
            artifacts.Add(Artifacts.AnimateDeadSpellSCroll);
            artifacts.Add(Artifacts.PortablePhylactery);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.ImperialSteel, 158);
            requiredMaterials.Add(Materials.ThirdAgeIron, 68);
            requiredMaterials.Add(Materials.ZarosianInsignia, 60);
            requiredMaterials.Add(Materials.AncientVis, 172);
            requiredMaterials.Add(Materials.BloodofOrcus, 234);
            requiredMaterials.Add(Materials.Vellum, 120);
            requiredMaterials.Add(Materials.MoltenGlass);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("162000", expectedExp.ToString());
        }

        [Test]
        public void Zarosian_IV_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.PraetorianHood);
            artifacts.Add(Artifacts.PraetorianRobes);
            artifacts.Add(Artifacts.PraetorianStaff);
            artifacts.Add(Artifacts.AncientGlobe);
            artifacts.Add(Artifacts.BattlePlans);
            artifacts.Add(Artifacts.PrimaLegioPainting);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.Vellum, 40);
            requiredMaterials.Add(Materials.TyrianPurple, 188);
            requiredMaterials.Add(Materials.WhiteOak, 40);
            requiredMaterials.Add(Materials.ZarosianInsignia, 130);
            requiredMaterials.Add(Materials.ImperialSteel, 36);
            requiredMaterials.Add(Materials.SamiteSilk, 122);
            requiredMaterials.Add(Materials.AncientVis, 218);
            requiredMaterials.Add(Materials.DeathRune, 180);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("240000", expectedExp.ToString());
        }

        [Test]
        public void Zarosian_V_Required_Material_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.ApexCap);
            artifacts.Add(Artifacts.CurseTablet);
            artifacts.Add(Artifacts.FuneraryUrnOfShadow);
            artifacts.Add(Artifacts.InfulaRobes);
            artifacts.Add(Artifacts.FuneraryUrnOfSmoke);
            artifacts.Add(Artifacts.HandOfTheAncients);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 24);
            requiredMaterials.Add(Materials.TyrianPurple, 40);
            requiredMaterials.Add(Materials.Goldrune, 30);
            requiredMaterials.Add(Materials.BloodofOrcus, 24);
            requiredMaterials.Add(Materials.Soapstone, 74);
            requiredMaterials.Add(Materials.ZarosianInsignia, 12);
            requiredMaterials.Add(Materials.ImperialSteel, 16);
            requiredMaterials.Add(Materials.SamiteSilk, 54);
            requiredMaterials.Add(Materials.AncientVis, 74);
            requiredMaterials.Add(Materials.WhiteOak, 18);

            // Act
            MaterialStorage actualMaterials = artifacts - requiredMaterials;
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("15399.9", expectedExp.ToString());

        }

        [Test]
        public void Zarosian_VI_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.DecorativeAmphora);
            artifacts.Add(Artifacts.FuneraryUrnOfIce);
            artifacts.Add(Artifacts.LoarnabRod);
            artifacts.Add(Artifacts.InquisitorsCeremonialArmour);
            artifacts.Add(Artifacts.InquisitorsCeremonialMask);
            artifacts.Add(Artifacts.InquisitorsSeal);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 26);
            requiredMaterials.Add(Materials.TyrianPurple, 62);
            requiredMaterials.Add(Materials.Goldrune, 14);
            requiredMaterials.Add(Materials.BloodofOrcus, 30);
            requiredMaterials.Add(Materials.Soapstone, 56);
            requiredMaterials.Add(Materials.ZarosianInsignia, 20);
            requiredMaterials.Add(Materials.SamiteSilk, 52);
            requiredMaterials.Add(Materials.ImperialSteel, 18);
            requiredMaterials.Add(Materials.AncientVis, 66);
            requiredMaterials.Add(Materials.WhiteOak, 28);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("18900", expectedExp.ToString());
        }

        [Test]
        public void Zarosian_VII_Required_Materials_Test()
        {
            // Set up
            ArtifactStorage artifacts = new ArtifactStorage();
            artifacts.Add(Artifacts.GladiatorHelmet);
            artifacts.Add(Artifacts.GladiatorSword);
            artifacts.Add(Artifacts.FuneraryUrnOfBlood);
            artifacts.Add(Artifacts.FuneraryUrnOfMiasma);
            artifacts.Add(Artifacts.ModelChariot);
            artifacts.Add(Artifacts.TheSerpantsFallCarving);

            MaterialStorage requiredMaterials = new MaterialStorage();
            requiredMaterials.Add(Materials.LeatherScraps, 18);
            requiredMaterials.Add(Materials.TyrianPurple, 52);
            requiredMaterials.Add(Materials.Goldrune, 38);
            requiredMaterials.Add(Materials.BloodofOrcus, 48);
            requiredMaterials.Add(Materials.Soapstone, 60);
            requiredMaterials.Add(Materials.ZarosianInsignia, 30);
            requiredMaterials.Add(Materials.Vellum, 28);
            requiredMaterials.Add(Materials.ImperialSteel, 78);
            requiredMaterials.Add(Materials.AncientVis, 20);
            requiredMaterials.Add(Materials.WhiteOak, 12);

            // Act
            MaterialStorage actualMaterials = new MaterialStorage();
            double expectedExp = artifacts.CalculateExperience();

            // Assert
            Assert.IsEmpty(actualMaterials.TotalMaterials);
            Assert.AreEqual("23220", expectedExp.ToString());
        }
    }
}
