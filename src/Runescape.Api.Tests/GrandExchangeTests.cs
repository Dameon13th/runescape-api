using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    [TestFixture]
    [Description("A collection of test that mocks the HttpClinet to return expected responses without actually making an Http request to the Runescape API.")]
    public class GrandExchangeTests
    {
        #region "Private Members"

        private static readonly DateTime _runeDate = new DateTime(2002, 2, 27);

        #endregion

        #region "Tests"

        [Test]
        [TestCase(1)]
        [TestCase(365)]
        [Description("Test that ensures that the correct updated date is returned.")]
        public async Task Test_Correct_LastUpdated(int daySinceUpdate)
        {
            // Setup
            LastUpdatedResponse expected = TestData.BuildLastUpdatedResponse(daySinceUpdate);

            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithLastUpdated(expected)
                    .Finish();

            // Act
            DateTime actual = await grandExchange.GetLastUpdatedDateAsync();

            // Assert
            Assert.AreEqual(_runeDate.AddDays(daySinceUpdate), actual);
        }

        [Test]
        [Description("Test that ensures if there is an exception the Runedate (2/27/2002) is returned.")]
        public async Task Test_Web_Exception_Returns_Runedate()
        {
            // Setup
            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            DateTime actual = await grandExchange.GetLastUpdatedDateAsync();

            // Assert
            Assert.AreEqual(_runeDate, actual);
        }

        [Test]
        [TestCase(GrandExchangeCategory.Ammo)]
        [TestCase(GrandExchangeCategory.ArchaeologyMaterials)]
        [Description("Test that ensures that the category returned by the Runescape Rest API is what the GrandExchange returns.")]
        public async Task Test_Correct_Category_Returned(GrandExchangeCategory category)
        {
            // Setup
            GetCategoriesResponse expected = TestData.BuildGetCategoriesResponse(category);

            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithCategory(category, expected)
                    .Finish();

            // Act
            ICategoryResult actual = await grandExchange.GetCategoryAsync(category);

            //Assert
            Assert.NotNull(actual);
            Assert.AreEqual(category, actual.Category);

            foreach (KeyValuePair<string, int> valuePair in actual.CountByLetter)
            {
                string firstLetter = valuePair.Key;
                int itemCount = valuePair.Value;

                CountByLetter expectedCount = expected.CountByLetter.FirstOrDefault(cbl => cbl.Letter == firstLetter);

                Assert.NotNull(expectedCount);
                Assert.AreEqual(expectedCount.ItemCount, itemCount);
            }
        }

        [Test]
        [TestCase(GrandExchangeCategory.Ammo)]
        [TestCase(GrandExchangeCategory.ArchaeologyMaterials)]
        [Description("Test that ensures if there is an exception making the Http request a default empty ICategoryResult is returned.")]
        public async Task Test_Web_Exception_Returns_Empty_Category(GrandExchangeCategory category)
        {
            // Setup
            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            ICategoryResult actual = await grandExchange.GetCategoryAsync(category);

            // Assert
            Assert.NotNull(actual);
            Assert.AreEqual(category, actual.Category);
            Assert.AreEqual(0, actual.CountByLetter.Count);
        }

        [Test]
        [TestCase(GrandExchangeCategory.Ammo, 'a', 1)]
        [TestCase(GrandExchangeCategory.Bolts, 'b', 2)]
        [Description("Test that ensures that the IItemsResponse returned by the Runescae Rest API is what the Grandexchange returns")]
        public async Task Test_Correct_Items_Returned(GrandExchangeCategory category, char firstLetter, int page)
        {
            // Setup
            ItemsResponse expected = TestData.BuildItemsResponse(category, firstLetter, page);

            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithItems(category, firstLetter, page, expected)
                    .Finish();

            // Act
            IItemsResult actual = await grandExchange.GetItemsAsync(category, firstLetter, page);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(GrandExchangeCategory.Ammo, 'a', 1)]
        [Description("Test that ensures if there is an exception making the Http request an empty items response is returned.")]
        public async Task Test_Web_Exception_Returns_Empty(GrandExchangeCategory category, char firstLetter, int page)
        {
            // Setup
            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IItemsResult actual = await grandExchange.GetItemsAsync(category, firstLetter, page);

            // Assert
            Assert.NotNull(actual);
            AssertAreEqual(ItemsResponse.Empty, actual);
        }

        [Test]
        [TestCase(101)]
        [Description("Test that ensures that the Item Details returned by the Runescape Rest API is what the GrandExchange returns")]
        public async Task Test_Correct_Item_Details_Returned(int id)
        {
            ItemDetailResponse expected = TestData.BuildItemDetailResponse(id);

            // Setup
            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithItemDetails(id, expected)
                    .Finish();

            // Act
            IItemDetail actual = await grandExchange.GetItemDetailAsync(id);

            // Assert
            AssertAreEqual(expected.Item, actual);
        }

        [Test]
        [TestCase(101)]
        [Description("Test that ensures that the Item Details returned by the Runescape Rest API is what the GrandExchange returns")]
        public async Task Test_Web_Exception_Returns_Null(int id)
        {
            // Setup
            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IItemDetail actual = await grandExchange.GetItemDetailAsync(id);

            // Assert
            Assert.IsNull(actual);
        }

        [Test]
        [TestCase(101, ImageSize.Sprite)]
        [TestCase(101, ImageSize.Large)]
        public async Task TestGetImage(long id, ImageSize size)
        {
            // Setup
            Image expected = TestData.BuildImage(id, size);

            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithImage(id, size, expected)
                    .Finish();

            // Act
            IImage actual = await grandExchange.GetImageAsync(id, size);

            // Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        [TestCase(101, ImageSize.Sprite)]
        public async Task TestGetImage2(long id, ImageSize size)
        {
            // Setup
            IGrandExchange grandExchange =
                new GrandExchangeBuilder()
                    .WithNetException()
                    .Finish();

            // Act
            IImage actual = await grandExchange.GetImageAsync(id, size);

            // Assert
            Assert.IsNull(actual);
        }

        #endregion

        #region "Private Methods"

        private static void AssertAreEqual(IItemsResult expected, IItemsResult actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Category, actual.Category);
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.AreEqual(expected.FirstLetter, actual.FirstLetter);
            Assert.AreEqual(expected.PageNumber, actual.PageNumber);
            Assert.AreEqual(expected.Items.Count, actual.Items.Count);

            for (int i = 0; i < expected.Items.Count; i++)
            {
                IItem expectedItem = expected.Items[i];
                IItem actualItem = actual.Items[i];
                AssertAreEqual(expectedItem, actualItem);
            }
        }

        private static void AssertAreEqual(IItem expected, IItem actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.IconUrl, actual.IconUrl);
            Assert.AreEqual(expected.LargeIconUrl, actual.LargeIconUrl);
            Assert.AreEqual(expected.Type, actual.Type);
            Assert.AreEqual(expected.TypeIconUrl, actual.TypeIconUrl);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.IsMembers, actual.IsMembers);
            AssertAreEqual(expected.Current, actual.Current);
            AssertAreEqual(expected.Today, actual.Today);
        }

        private static void AssertAreEqual(IPrice expected, IPrice actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Value, actual.Value);
            Assert.AreEqual(expected.Trend, actual.Trend);
        }

        private static void AssertAreEqual(IPriceChange expected, IPriceChange actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Trend, actual.Trend);
            Assert.AreEqual(expected.Change, actual.Change);
        }

        private static void AssertAreEqual(IItemDetail expected, IItemDetail actual)
        {
            AssertAreEqual(expected, (IItem)actual);
            AssertAreEqual(expected.Day30, actual.Day30);
            AssertAreEqual(expected.Day90, actual.Day90);
            AssertAreEqual(expected.Day180, actual.Day180);
        }

        private static void AssertAreEqual(IImage expected, IImage actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Extension, actual.Extension);
            Assert.AreEqual(expected.Length, actual.Length);
        }

        #endregion
    }
}