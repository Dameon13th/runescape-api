using System.Net;
using System.Net.Http;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    internal class GrandExchangeBuilder : Builder<IGrandExchange>
    {
        #region "Public Methods"

        public GrandExchangeBuilder WithNetException()
        {
            MessageHandler = new ExceptionHttpMessageHandler();
            return this;
        }
        
        public GrandExchangeBuilder WithLastUpdated(LastUpdatedResponse response)
        {
            string apiUrl = ApiUrls.GrandExchange.LAST_UPDATE;
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, response);
            return this;
        }

        public GrandExchangeBuilder WithCategory(GrandExchangeCategory category, GetCategoriesResponse response)
        {
            string apiUrl = ApiUrls.GrandExchange.BuildCatergoryUrl(category);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, response);
            return this;
        }

        public GrandExchangeBuilder WithItems(GrandExchangeCategory category, char firstChar, int page, ItemsResponse response)
        {
            string apiUrl = ApiUrls.GrandExchange.BuildItemsUrl(category, firstChar, page);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, response);
            return this;
        }

        public GrandExchangeBuilder WithItemDetails(int id, ItemDetailResponse response)
        {
            string apiUrl = ApiUrls.GrandExchange.BuildItemDetailUrl(id);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, response);
            return this;
        }

        public GrandExchangeBuilder WithImage(long id, ImageSize size, Image image)
        {
            string apiUrl = ApiUrls.GrandExchange.BuildItemImageUrl(id, size);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, $"image/{image.Extension}", image.Stream);
            return this;
        }

        #endregion

        #region Overrides of Builder<IGrandExchange>

        public override IGrandExchange Finish()
        {
            return new GrandExchange(new Options(CacheOptions.None), HttpClientFactory);
        }

        #endregion
    }
}