﻿using System.IO;
using System.Net;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    internal class HiscoresBuilder : Builder<IHiscores>
    {
        #region "Public Methods"

        public HiscoresBuilder WithRankings(Skill skill, int count, Ranking[] rankings)
        {
            string apiUrl = ApiUrls.Hiscores.BuildRankingsUrl(skill, count);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, rankings);
            return this;
        }

        public HiscoresBuilder WithRankings(Activity activity, int count, Ranking[] rankings)
        {
            string apiUrl = ApiUrls.Hiscores.BuildRankingsUrl(activity, count);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, rankings);
            return this;
        }

        public HiscoresBuilder WithPlayerHiscores(GameMode gameMode, string playerName, IPlayerHiscores hiscores)
        {
            string apiUrl = ApiUrls.Hiscores.BuildHiscoresUrl(gameMode, playerName);

            Stream stream = new MemoryStream();

            StreamWriter writer = new StreamWriter(stream)
            {
                NewLine = "\n"
            };
            foreach (IHiscore hiscoresSkill in hiscores.Skills.Values)
            {
                writer.WriteLine(hiscoresSkill.ToString());
            }

            foreach (IHiscore hiscoresActivity in hiscores.Activities.Values)
            {
                writer.WriteLine(hiscoresActivity.ToString());
            }

            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, "application/txt", stream);

            return this;
        }

        public HiscoresBuilder WithSeasonalRankings(string playerName, SeasonType seasonType, SeasonalRanking[] rankings)
        {
            string apiUrl = ApiUrls.Hiscores.BuildSeasonalHiscore(playerName, seasonType);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, rankings);
            return this;
        }

        public HiscoresBuilder WithSeasons(SeasonType seasonType, Season[] seasons)
        {
            string apiUrl = ApiUrls.Hiscores.BuildSeasonUrl(seasonType);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, seasons);
            return this;
        }

        public HiscoresBuilder WithClanRankings(ClanRanking[] rankings)
        {
            MessageHandler.AddCustomHandler(ApiUrls.Hiscores.CLAN_RANKINGS, HttpStatusCode.OK, rankings);
            return this;
        }

        public HiscoresBuilder WithClanMembers(string clanName, ClanMember[] mebers)
        {
            string apiUrl = ApiUrls.Hiscores.BuildClanMembersUrl(clanName);

            Stream stream = new MemoryStream();

            StreamWriter writer = new StreamWriter(stream)
            {
                NewLine = "\n"
            };

            writer.WriteLine("This is a fake header row");
            foreach (ClanMember clanMember in mebers)
            {
                writer.WriteLine(clanMember.ToString());
            }

            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, "application/txt", stream);

            return this;
        }

        public HiscoresBuilder WithNetException()
        {
            MessageHandler = new ExceptionHttpMessageHandler();
            return this;
        }

        #endregion

        #region Overrides of Builder<IHiscores>

        public override IHiscores Finish()
        {
            return new Hiscores(new Options(CacheOptions), HttpClientFactory);
        }

        #endregion
    }
}
