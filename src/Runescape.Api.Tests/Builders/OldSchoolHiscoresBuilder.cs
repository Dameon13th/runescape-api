﻿using System.IO;
using System.Net;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    internal class OldSchoolHiscoresBuilder : Builder<IOldSchoolHiscores>
    {
        public OldSchoolHiscoresBuilder WithHiscores(OldSchoolGameMode gameMode, string playerName, IOldSchoolPlayerHiscores hiscores)
        {
            string apiUrl = ApiUrls.OldSchoolHiscores.BuildHiscoresUrl(gameMode, playerName);

            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream)
            {
                NewLine = "\n"
            };

            foreach (IHiscore hiscore in hiscores.Skills.Values)
            {
                writer.WriteLine(hiscore.ToString());
            }

            foreach (IHiscore hiscore in hiscores.Activities.Values)
            {
                writer.WriteLine(hiscore.ToString());
            }
            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, "application/txt", stream);
            return this;
        }

        #region Overrides of Builder<IHiscores>

        public override IOldSchoolHiscores Finish()
        {
            return new OldSchoolHiScores(new Options(CacheOptions), HttpClientFactory);
        }

        #endregion
    }
}