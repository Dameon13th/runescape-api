﻿using System.Net;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    internal class SolomonBuilder : Builder<ISolomonStoreApi>
    {
        #region "Public Methods"

        public SolomonBuilder WithResponse(SolomonResponseRaw response)
        {
            string apiUrl = ApiUrls.SolomonStore.GetUrl();
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, response);
            return this;
        }

        #endregion

        #region "Implementation of Builder<ISolomonStoreApi>"

        public override ISolomonStoreApi Finish()
        {
            return new SolomonStoreApi(new Options(CacheOptions), HttpClientFactory);
        }

        #endregion
    }
}
