﻿using System.Collections.Generic;
using System.Net;
using Runescape.Api.Model;

namespace Runescape.Api.Tests
{
    internal class BestiaryBuilder : Builder<IBestiary>
    {
        #region "Public Methods"

        public BestiaryBuilder WithBeastData(long beastId, BeastData beastData)
        {
            string apiUrl = ApiUrls.Bestiary.BuildBeastDataUrl(beastId);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, beastData);
            return this;
        }

        public BestiaryBuilder WithBadBeastId(int badId)
        {
            string apiUrl = ApiUrls.Bestiary.BuildBeastDataUrl(badId);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.NotFound, "");
            return this;
        }

        public BestiaryBuilder WithSearchTerms(BeastSearchResult[] searchResults, params string[] terms)
        {
            string apiUrl = ApiUrls.Bestiary.BuildBeastSearchUrl(terms);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, searchResults);
            return this;
        }

        public BestiaryBuilder WithBestiaryNames(char firstLetter, BeastSearchResult[] searchResults)
        {
            string apiUrl = ApiUrls.Bestiary.BuildBeastNameUrl(firstLetter);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, searchResults);
            return this;
        }

        public BestiaryBuilder WithAreaNames(string[] areaNames)
        {
            string apiUrl = ApiUrls.Bestiary.BuildAreaNamesUrl();
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, areaNames);
            return this;
        }

        public BestiaryBuilder WithAreaBeast(string areaName, BeastSearchResult[] searchResults)
        {
            string apiUrl = ApiUrls.Bestiary.BuildAreaBeastUrl(areaName);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, searchResults);
            return this;
        }

        public BestiaryBuilder WithSlayerCategories(Dictionary<string,int> categories)
        {
            string apiUrl = ApiUrls.Bestiary.BuildSlayerCategoriesUrl();
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, categories);
            return this;
        }

        public BestiaryBuilder WithSlayerBeast(int slayerCategoryId, BeastSearchResult[] searchResults)
        {
            string apiUrl = ApiUrls.Bestiary.BuildSlayerBeastUrl(slayerCategoryId);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, searchResults);
            return this;
        }

        public BestiaryBuilder WithWeaknesses(Dictionary<string, int> weaknesses)
        {
            string apiUrl = ApiUrls.Bestiary.BuildWeaknessUrl();
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, weaknesses);
            return this;
        }

        public BestiaryBuilder WithWeaknessBeasts(int weaknessId, BeastSearchResult[] searchResults)
        {
            string apiUrl = ApiUrls.Bestiary.BuildWeaknessBeastUrl(weaknessId);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, searchResults);
            return this;
        }

        public BestiaryBuilder WithLevelGroup(int low, int high, BeastSearchResult[] searchResults)
        {
            string apiUrl = ApiUrls.Bestiary.BuildLevelRangeUrl(low, high);
            MessageHandler.AddCustomHandler(apiUrl, HttpStatusCode.OK, searchResults);
            return this;
        }

        public BestiaryBuilder WithWebException()
        {
            MessageHandler = new ExceptionHttpMessageHandler();
            return this;
        }

        #endregion

        #region Overrides of Builder<IBestiary>

        public override IBestiary Finish()
        {
            return new Bestiary(new Options(CacheOptions), HttpClientFactory);
        }

        #endregion
    }
}