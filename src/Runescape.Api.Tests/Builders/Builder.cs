﻿using System.Net.Http;
using Moq;

namespace Runescape.Api.Tests
{
    internal abstract class Builder<T>
    {
        #region "Protected Members"

        private TestHttpMessageHandler _handler;

        protected TestHttpMessageHandler MessageHandler
        {
            get { return _handler; }
            set
            {
                _handler = value;
                OnHandlerChanged();
            }
        }


        protected IHttpClientFactory HttpClientFactory { get; private set; }

        protected CacheOptions CacheOptions { get; set; }

        #endregion

        #region "Constructor"

        protected Builder()
        {
            CacheOptions = CacheOptions.None;
            MessageHandler = new TestHttpMessageHandler();
        }

        #endregion

        #region "Private Methods"

        private void OnHandlerChanged()
        {
            Mock<IHttpClientFactory> moq = new Mock<IHttpClientFactory>();
            moq
                .Setup(h => h.CreateClient(It.IsAny<string>()))
                .Returns(new HttpClient(MessageHandler));
            HttpClientFactory = moq.Object;
        }

        #endregion

        #region "Abstract Members"

        public abstract T Finish();

        #endregion
    }
}