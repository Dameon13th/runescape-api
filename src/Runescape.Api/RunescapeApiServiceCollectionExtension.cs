﻿using Runescape.Api;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Extension methods to configure an <see cref="IServiceCollection"/> for the Runescape APIs.
    /// </summary>
    public static class RunescapeApiServiceCollectionExtension
    {
        /// <summary>
        /// Adds the Runescape APIs and related services to the <see cref="IServiceCollection"/>
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        /// <param name="cacheOptions">The <see cref="CacheOptions"/> to be passed into API instances.</param>
        /// <returns>The <see cref="IServiceCollection"/>.</returns>
        public static IServiceCollection AddRunescapeApiServices(this IServiceCollection services, CacheOptions cacheOptions = null)
        {
            services.Configure<RunescapeApiOptions>(options => { options.CacheOptions = cacheOptions ?? CacheOptions.None; });
            services.AddSingleton<IGrandExchange, GrandExchange>();
            services.AddSingleton<IHiscores, Hiscores>();
            services.AddSingleton<IBestiary, Bestiary>();
            services.AddSingleton<IOldSchoolHiscores, OldSchoolHiScores>();
            return services;
        }
    }
}