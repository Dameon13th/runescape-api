﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Runescape.Api.Archaeology
{
    [DebuggerDisplay("{Name}")]
    internal class Artifact : IArtifact
    {
        #region Implementation of IArtifact

        public Faction Faction { get; set; }

        public string Name { get; set; }

        public int Level { get; set; }

        public long Chronotes { get; set; }

        public double Experience { get; set; }

        internal Dictionary<IMaterial, int> RequiredMaterials { get; set; }

        IReadOnlyDictionary<IMaterial, int> IArtifact.RequiredMaterials
        {
            get { return RequiredMaterials; }
        }

        #endregion
    }
}