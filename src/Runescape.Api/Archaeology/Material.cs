﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Runescape.Api.Archaeology
{
    [DebuggerDisplay("{Name} ({Id})")]
    internal class Material : IMaterial
    {
        #region Implementation of IMaterial

        public Faction Faction { get; internal set; }

        public string Name { get; internal set; }

        public int Level { get; internal set; }

        public int Id { get; internal set; }

        public IReadOnlyList<string> Locations { get; internal set; }


        #endregion
    }
}