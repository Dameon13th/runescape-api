﻿using System.Collections.Generic;

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Class that allows the consumer to add artifacts to. Once artifacts are added this class will let you know the required amount of materials needs to
    /// repair the artifacts.
    /// </summary>
    /// <example>
    /// The following example demonstrates adding two different artifact at differnt amounts to the storage.
    /// It also shows that as artifacts are added the <see cref="MaterialStorage.TotalMaterials"/> is updated with the materials required to repair then.
    /// <code language="cs">
    ///<![CDATA[
    /// ArtifactStorage storage = new ArtifactStorage();
    /// storage.Add(Artifacts.ZarosianEwer); // Adds a single Zarosian Ewer
    /// storage.Add(Artifacts.ZarosianStein, 12); // Add twelve Zarosian Steins
    ///
    /// // This will be 2 (Zarosian Ewer and Stein)
    /// int totalArtifactTypeCount = storage.TotalArtifacts.Keys.Count();
    ///
    /// // This will be 13 (One ewer plus twelve steins)
    /// long totalArtifactCount = storage.TotalArtifacts.Sum(valuePair => valuePair.Value);
    ///
    /// // This will be 3 (Third age iron, Zarosian Insignia, and Imperial steel).
    /// // The three different types of materials required to repair the Zarosian Ewer and Stein.
    /// int totalMaterialTypeCount = storage.TotalMaterials.Keys.Count();
    /// ]]>
    /// </code>
    /// </example>
    public class ArtifactStorage : MaterialStorage
    {
        #region "Private Members"

        private readonly Dictionary<IArtifact, long> _artifactCount;

        #endregion

        #region "Public Properties"

        /// <summary>
        /// Gets a readonly dictionary where the key is an instance of <see cref="IArtifact"/> and the value is count of those artifacts in this storage.
        /// </summary>
        public IReadOnlyDictionary<IArtifact, long> TotalArtifacts
        {
            get { return _artifactCount; }
        }

        #endregion

        #region "Constructor"

        /// <summary>
        /// Creats a new instance of the <b>ArtifactStorage</b> class.
        /// </summary>
        public ArtifactStorage()
        {
            _artifactCount = new Dictionary<IArtifact, long>();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Adds the given <paramref name="artifact"/> to the storage. If the artifact is already in storage
        /// the count is incremented by the given <paramref name="count"/>.
        /// </summary>
        /// <param name="artifact">The artifact to add to storage</param>
        /// <param name="count">The number of artifacts to add.</param>
        public void Add(IArtifact artifact, long count = 1)
        {
            if (_artifactCount.TryGetValue(artifact, out long artifactCount))
            {
                artifactCount += count;
            }
            else
            {
                artifactCount = count;
            }
            _artifactCount[artifact] = artifactCount;

            foreach (KeyValuePair<IMaterial, int> material in artifact.RequiredMaterials)
            {
                Add(material.Key, material.Value * count);
            }
        }

        /// <summary>
        /// Removes the given <paramref name="artifact"/> from storage.
        /// If the artifact is not currently in storrage no action is taken.
        /// If the count for the given <paramref name="artifact"/> is less than ot equal to zero it is removed
        /// from storage.
        /// </summary>
        /// <param name="artifact">The artifact to remove from storage.</param>
        /// <param name="count">The number of artifacts to remove.</param>
        public void Remove(IArtifact artifact, long count = 1)
        {
            if (!_artifactCount.TryGetValue(artifact, out long artifactCount))
            {
                return;
            }

            artifactCount -= count;
            if (artifactCount <= 0)
            {
                _artifactCount.Remove(artifact);
                return;
            }

            _artifactCount[artifact] = artifactCount;

            foreach (KeyValuePair<IMaterial, int> kvp in artifact.RequiredMaterials)
            {
                Remove(kvp.Key, kvp.Value * count);
            }
        }

        /// <summary>
        /// Calculate the amount of experience you would get for repairing all the artifacts in storage.
        /// </summary>
        /// <returns>
        /// The total experience a player would gain if all the artifacts in storage where to be repaired.
        /// </returns>
        public double CalculateExperience()
        {
            double totalExp = 0;

            foreach (KeyValuePair<IArtifact, long> valuePair in _artifactCount)
            {
                totalExp += (valuePair.Key.Experience * valuePair.Value);
            }

            return totalExp;
        }

        #endregion

        #region "Operators"

        /// <summary>
        /// Adds all the artifacts locateded in <paramref name="b"/> into <paramref name="a"/>.
        /// </summary>
        /// <param name="a">The storage to add to.</param>
        /// <param name="b">The storage to add from.</param>
        /// <returns>Returns <paramref name="a"/> with all the artifacts from <paramref name="b"/> added.</returns>
        public static ArtifactStorage operator +(ArtifactStorage a, ArtifactStorage b)
        {
            foreach (KeyValuePair<IMaterial, long> kvp in b.TotalMaterials)
            {
                a.Add(kvp.Key, kvp.Value);
            }
            return a;
        }

        /// <summary>
        /// Adds all the martieals located in <paramref name="b"/> into <paramref name="a"/>.
        /// </summary>
        /// <param name="a">The storage to add to.</param>
        /// <param name="b">The storage to add from.</param>
        /// <returns>Returns <paramref name="a"/> with all the materials from <paramref name="b"/> added.</returns>
        public static ArtifactStorage operator +(ArtifactStorage a, MaterialStorage b)
        {
            foreach (KeyValuePair<IMaterial, long> kvp in b.TotalMaterials)
            {
                a.Add(kvp.Key, kvp.Value);
            }
            return a;
        }

        /// <summary>
        /// Removes all the artifacts located in <paramref name="b"/> into <paramref name="a"/>.
        /// </summary>
        /// <param name="a">The storage to remove from.</param>
        /// <param name="b">The storage to remove</param>
        /// <returns>Returns <paramref name="a"/> with all the materials from <paramref name="b"/> removed.</returns>
        public static ArtifactStorage operator -(ArtifactStorage a, MaterialStorage b)
        {
            foreach (KeyValuePair<IMaterial, long> kvp in b.TotalMaterials)
            {
                a.Remove(kvp.Key, kvp.Value);
            }
            return a;
        }

        /// <summary>
        /// Removes all the martials located in <paramref name="b"/> into <paramref name="a"/>.
        /// </summary>
        /// <param name="a">The storage to remove from.</param>
        /// <param name="b">The storage to remove</param>
        /// <returns>Returns <paramref name="a"/> with all the materials from <paramref name="b"/> removed.</returns>
        public static ArtifactStorage operator -(ArtifactStorage a, ArtifactStorage b)
        {
            foreach (KeyValuePair<IMaterial, long> kvp in b.TotalMaterials)
            {
                a.Remove(kvp.Key, kvp.Value);
            }
            return a;
        }

        #endregion
    }
}