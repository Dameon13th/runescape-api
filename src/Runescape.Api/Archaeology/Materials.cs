﻿using System.Collections.Generic;

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Class that contains a static instance of all known Archaeology materials.
    /// </summary>
    /// <remarks>
    /// Some artifacts require none Archaeology materials (Cut games for example).
    /// These none Archaeology items are still included in this class. This was done so that
    /// the <see cref="Artifacts"/> class con properly contain all required items for repare.
    /// </remarks>
    public static class Materials
    {
        #region "Private Members"

        private static readonly List<IMaterial> _all;

        #endregion

        #region "Public Properties"

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Aetherium alloy.
        /// </summary>
        public static IMaterial AetheriumAlloy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Ancient vis.
        /// </summary>
        public static IMaterial AncientVis { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Animal furs.
        /// </summary>
        public static IMaterial AnimalFurs { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Armadylean yellow.
        /// </summary>
        public static IMaterial ArmadyleanYellow { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Black mushroom ink.
        /// </summary>
        public static IMaterial BlackMushroomInk { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Blood of Orcus.
        /// </summary>
        public static IMaterial BloodofOrcus { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Bronze bar.
        /// </summary>
        public static IMaterial BronzeBar { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Cadmiun red.
        /// </summary>
        public static IMaterial CadmiumRed { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for clockwork.
        /// </summary>
        public static IMaterial Clockwork { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Cobalt blue.
        /// </summary>
        public static IMaterial CobaltBlue { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for death rune.
        /// </summary>
        public static IMaterial DeathRune { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for demonhide.
        /// </summary>
        public static IMaterial Demonhide { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for diamond.
        /// </summary>
        public static IMaterial Diamond { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for dragonstone.
        /// </summary>
        public static IMaterial Dragonstone { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for emerald.
        /// </summary>
        public static IMaterial Emerald { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Everlight silvthril.
        /// </summary>
        public static IMaterial EverlightSilvthril { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for eye of Dagon.
        /// </summary>
        public static IMaterial EyeofDagon { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Chaotic brimstone.
        /// </summary>
        public static IMaterial ChaoticBrimstone { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for fossilised bone.
        /// </summary>
        public static IMaterial FossilisedBone { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for goldrune.
        /// </summary>
        public static IMaterial Goldrune { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for hellfire metal.
        /// </summary>
        public static IMaterial HellfireMetal { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for imperial iron.
        /// </summary>
        public static IMaterial ImperialIron { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for imperial steel.
        /// </summary>
        public static IMaterial ImperialSteel { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Keramos.
        /// </summary>
        public static IMaterial Keramos { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for leather scraps/
        /// </summary>
        public static IMaterial LeatherScraps { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Maclachite green.
        /// </summary>
        public static IMaterial MalachiteGreen { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for mark of the Kyzaj.
        /// </summary>
        public static IMaterial MarkOfTheKyzaj { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for molten glass.
        /// </summary>
        public static IMaterial MoltenGlass { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for orthenglass.
        /// </summary>
        public static IMaterial Orthenglass { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Phoenix feather.
        /// </summary>
        public static IMaterial PhoenixFeather { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for purpleheart wood.
        /// </summary>
        public static IMaterial PurpleheartWood { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Quintessence.
        /// </summary>
        public static IMaterial Quintessence { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for rope.
        /// </summary>
        public static IMaterial Rope { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for ruby.
        /// </summary>
        public static IMaterial Ruby { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Samite silk.
        /// </summary>
        public static IMaterial SamiteSilk { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for sapphire.
        /// </summary>
        public static IMaterial Sapphire { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for silver bar.
        /// </summary>
        public static IMaterial SilverBar { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for soapstone.
        /// </summary>
        public static IMaterial Soapstone { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for star of Saradomin.
        /// </summary>
        public static IMaterial StarofSaradomin { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Stormguard steel.
        /// </summary>
        public static IMaterial StormguardSteel { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for third age iron.
        /// </summary>
        public static IMaterial ThirdAgeIron { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Tyrian purple.
        /// </summary>
        public static IMaterial TyrianPurple { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Vellum.
        /// </summary>
        public static IMaterial Vellum { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for vulcanized rubber.
        /// </summary>
        public static IMaterial VulcanisedRubber { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for warforged bronze.
        /// </summary>
        public static IMaterial WarforgedBronze { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for weapon poison.
        /// </summary>
        public static IMaterial WeaponPoison { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for white candle.
        /// </summary>
        public static IMaterial WhiteCandle { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for white marble.
        /// </summary>
        public static IMaterial WhiteMarble { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for white oak.
        /// </summary>
        public static IMaterial WhiteOak { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for wings of war.
        /// </summary>
        public static IMaterial WingsofWar { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Yu'biusk clay.
        /// </summary>
        public static IMaterial YubiuskClay { get; }

        /// <summary>
        /// Gets an instance of <see cref="IMaterial"/> that contains all the data for Zarosian insignia.
        /// </summary>
        public static IMaterial ZarosianInsignia { get; }

        /// <summary>
        ///  Gets an instance of <see cref="IMaterial"/> that contains all the data for Felt.
        /// </summary>
        public static IMaterial Felt { get; }

        /// <summary>
        ///  Gets an instance of <see cref="IMaterial"/> that contains all the data for Compass Rose.
        /// </summary>
        public static IMaterial CompassRose { get; }

        /// <summary>
        ///  Gets an instance of <see cref="IMaterial"/> that contains all the data for Dragon Metal.
        /// </summary>
        public static IMaterial DragonMetal { get; }

        /// <summary>
        ///  Gets an instance of <see cref="IMaterial"/> that contains all the data for Orgone.
        /// </summary>
        public static IMaterial Orgone { get; }

        /// <summary>
        ///  Gets an instance of <see cref="IMaterial"/> that contains all the data for Carbon Black.
        /// </summary>
        public static IMaterial CarbonBlack { get; }

        /// <summary>
        /// Gets a readonly list of all the materials.
        /// </summary>
        public static IReadOnlyList<IMaterial> All
        {
            get { return _all; }
        }

        #endregion

        #region "Constructor"

        static Materials()
        {
            AetheriumAlloy = new Material
            {
                Faction = Faction.Armadylean,
                Id = 49466,
                Name = "Aetherium alloy",
                Level = 85,
                Locations = new[]
                {
                    "Stormguard Citadel(Research & Development, north-east)",
                    "Stormguard Citadel(Research & Development, north-west)",
                    "Empyrean Citadel",
                }
            };
            AncientVis = new Material
            {
                Faction = Faction.Zarosian,
                Id = 49506,
                Name = "Ancient vis",
                Level = 25,
                Locations = new[]
                {
                    "Kharid-et(Culinarum)",
                    "Slayer Tower",
                }
            };
            AnimalFurs = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49446,
                Name = "Animal furs",
                Level = 76,
                Locations = new[]
                {
                    "Feldip",
                }
            };
            ArmadyleanYellow = new Material
            {
                Faction = Faction.Armadylean,
                Id = 49468,
                Name = "Armadylean yellow",
                Level = 76,
                Locations = new[]
                {
                    "Stormguard Citadel(Keshik memorial)",
                    "Stormguard Citadel(Relay station)",
                    "Empyrean Citadel",
                }
            };
            BlackMushroomInk = new Material
            {
                Id = 4622,
                Name = "Black mushroom ink"
            };
            BloodofOrcus = new Material
            {
                Faction = Faction.Zarosian,
                Id = 49508,
                Name = "Blood of Orcus",
                Level = 58,
                Locations = new[]
                {
                    "Kharid-et(Chapel)",
                    "Slayer Tower",
                }
            };
            BronzeBar = new Material
            {
                Id = 2349,
                Name = "Bronze bar"
            };
            CadmiumRed = new Material
            {
                Faction = Faction.Zamorakian,
                Id = 49496,
                Name = "Cadmium red",
                Level = 24,
                Locations = new[]
                {
                    "Infernal Source(Star Lodge cellar)",
                    "Infernal Source(Dagon Overlook south-west)",
                    "First Tower",
                }
            };
            Clockwork = new Material
            {
                Id = 8792,
                Name = "Clockwork"
            };
            CobaltBlue = new Material
            {
                Faction = Faction.Saradominist,
                Id = 49486,
                Name = "Cobalt blue",
                Level = 48,
                Locations = new[]
                {
                    "Everlight(Amphitheatre)",
                    "God Wars Dungeon(Saradominist, south-east)",
                }
            };
            DeathRune = new Material
            {
                Id = 560,
                Name = "Death rune"
            };
            Demonhide = new Material
            {
                Faction = Faction.Zamorakian,
                Id = 49500,
                Name = "Demonhide",
                Level = 29,
                Locations = new[]
                {
                    "Infernal Source(The Harrowing, north-east)",
                    "God Wars Dungeon(Zamorak's Fortress, east)",
                }
            };
            Diamond = new Material
            {
                Id = 1601,
                Name = "Diamond"
            };
            Dragonstone = new Material
            {
                Id = 1615,
                Name = "Dragonstone"
            };
            Emerald = new Material
            {
                Id = 1605,
                Name = "Emerald"
            };
            EverlightSilvthril = new Material
            {
                Faction = Faction.Saradominist,
                Id = 49488,
                Name = "Everlight silvthril",
                Level = 51,
                Locations = new[]
                {
                    "Everlight(Dominion Games stadium)",
                    "Barrows mounds",
                }
            };
            EyeofDagon = new Material
            {
                Faction = Faction.Zamorakian,
                Id = 49502,
                Name = "Eye of Dagon",
                Level = 36,
                Locations = new[]
                {
                    "Infernal Source(Dungeon of Disorder, south-east)",
                    "Daemonheim(south-west)",
                }
            };
            ChaoticBrimstone = new Material
            {
                Faction = Faction.Zamorakian,
                Id = 49498,
                Name = "Chaotic brimstone",
                Level = 29,
                Locations = new[]
                {
                    "Infernal Source(Vestibule of Futility, south)",
                    "Daemonheim(south-west)",
                }
            };
            FossilisedBone = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49448,
                Name = "Fossilised bone",
                Level = 81,
                Locations = new[]
                {
                    "Ancient cavern",
                    "Oddoldman",
                }
            };
            Goldrune = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49450,
                Name = "Goldrune",
                Level = 20,
                Locations = new[]
                {
                    "Kharid-et",
                    "Camdozaal",
                }
            };
            HellfireMetal = new Material
            {
                Faction = Faction.Zamorakian,
                Id = 49504,
                Name = "Hellfire metal",
                Level = 36,
                Locations = new[]
                {
                    "Infernal Source(Dungeon of Disorder)",
                    "Infernal Source(Vestibule of Futility, north-east)",
                    "Infernal Source(Dagon Overlook, north)",
                    "God Wars Dungeon(Zamorak's Fortress, north)",
                }
            };
            ImperialIron = new Material
            {
                Faction = Faction.Zarosian,
                Name = "Imperial Iron",
                Level = 1,
                Locations = new[] { "Tutorial" }
            };
            ImperialSteel = new Material
            {
                Faction = Faction.Zarosian,
                Id = 49510,
                Name = "Imperial steel",
                Level = 12,
                Locations = new[]
                {
                    "Kharid-et(barracks)",
                    "Empty Throne Room(east)",
                }
            };
            Keramos = new Material
            {
                Faction = Faction.Saradominist,
                Id = 49490,
                Name = "Keramos",
                Level = 42,
                Locations = new[]
                {
                    "Everlight(massgrave)",
                    "Everlight(Oikoi)",
                    "God Wars Dungeon(Saradominist, south-east)",
                }
            };
            LeatherScraps = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49452,
                Name = "Leather scraps",
                Level = 29,
                Locations = new[]
                {
                    "Morytania(north)",
                }
            };
            MalachiteGreen = new Material
            {
                Faction = Faction.Bandosian,
                Id = 49476,
                Name = "Malachite green",
                Level = 76,
                Locations = new[]
                {
                    "Warforge (Crucible arena)",
                    "Warforge (north goblin tunnels)",
                    "God Wars Dungeon(Bandos's Stronghold)",
                }
            };
            MarkOfTheKyzaj = new Material
            {
                Faction = Faction.Bandosian,
                Id = 49478,
                Name = "Mark of the Kyzaj",
                Level = 76,
                Locations = new[]
                {
                    "Warforge (north goblin tunnels)",
                    "God Wars Dungeon(Bandosian west)",
                }
            };
            MoltenGlass = new Material
            {
                Id = 1775,
                Name = "Molten glass"
            };
            Orthenglass = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49454,
                Name = "Orthenglass",
                Level = 20,
                Locations = new[]
                {
                    "Anachronia(north)",
                }
            };
            PhoenixFeather = new Material
            {
                Id = 4621,
                Name = "Phoenix feather"
            };
            PurpleheartWood = new Material
            {
                Faction = Faction.Zarosian,
                Name = "Purpleheart Wood",
                Level = 1,
                Locations = new[] { "Tutorial" }
            };
            Quintessence = new Material
            {
                Faction = Faction.Armadylean,
                Id = 49470,
                Name = "Quintessence",
                Level = 91,
                Locations = new[]
                {
                    "Stormguard Citadel(Research & Development, north-east)",
                    "Stormguard Citadel(Howl's workshop)",
                    "Empyrean Citadel",
                }
            };
            Rope = new Material
            {
                Id = 954,
                Name = "Rope"
            };
            Ruby = new Material
            {
                Id = 1603,
                Name = "Ruby"
            };
            SamiteSilk = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49456,
                Name = "Samite silk",
                Level = 12,
                Locations = new[]
                {
                    "Kharid-et(barracks)",
                    "Kharid-etentrance",
                }
            };
            Sapphire = new Material
            {
                Id = 1607,
                Name = "Sapphire"
            };
            SilverBar = new Material
            {
                Id = 2355,
                Name = "Silver bar"
            };
            Soapstone = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49458,
                Name = "Soapstone",
                Level = 98,
                Locations = new[]
                {
                    "Waiko",
                }
            };
            StarofSaradomin = new Material
            {
                Faction = Faction.Saradominist,
                Id = 49492,
                Name = "Star of Saradomin",
                Level = 51,
                Locations = new[]
                {
                    "Everlight(Acropolis)",
                    "Barrows mounds",
                }
            };
            StormguardSteel = new Material
            {
                Faction = Faction.Armadylean,
                Id = 49472,
                Name = "Stormguard steel",
                Level = 70,
                Locations = new[]
                {
                    "Stormguard Citadel(Research & Development, south-west)",
                    "God Wars Dungeon(Armadylean, south-west)",
                }
            };
            ThirdAgeIron = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49460,
                Name = "Third Age iron",
                Level = 5,
                Locations = new[]
                {
                    "Varrock Dig Site",
                    "Kharid-et",
                    "Infernal Source",
                    "Everlight",
                    "Stormguard Citadel",
                    "Warforge",
                }
            };
            TyrianPurple = new Material
            {
                Faction = Faction.Zarosian,
                Id = 49512,
                Name = "Tyrian purple",
                Level = 25,
                Locations = new[]
                {
                    "Kharid-et(barracks)",
                    "Empty Throne Room(south)",
                }
            };
            Vellum = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49462,
                Name = "Vellum",
                Level = 24,
                Locations = new[]
                {
                    "First Tower",
                }
            };
            VulcanisedRubber = new Material
            {
                Faction = Faction.Bandosian,
                Id = 49480,
                Name = "Vulcanised rubber",
                Level = 76,
                Locations = new[]
                {
                    "Warforge (south goblin tunnels)",
                    "Feldip(shores)",
                }
            };
            WarforgedBronze = new Material
            {
                Faction = Faction.Bandosian,
                Id = 49482,
                Name = "Warforged bronze",
                Level = 76,
                Locations = new[]
                {
                    "Warforge (Crucible arena)",
                    "Warforge (north goblin tunnels)",
                    "God Wars Dungeon(Bandosian, north)",
                    "God Wars Dungeon(Bandos's Stronghold)",
                }
            };
            WeaponPoison = new Material
            {
                Id = 25487,
                Name = "Weapon Poison (3)"
            };
            WhiteCandle = new Material
            {
                Id = 33,
                Name = "White candle"
            };
            WhiteMarble = new Material
            {
                Faction = Faction.Saradominist,
                Id = 49494,
                Name = "White marble",
                Level = 42,
                Locations = new[]
                {
                    "Everlight(Dominion Games stadium)",
                    "Everlight(Acroplois)",
                    "First Tower",
                }
            };
            WhiteOak = new Material
            {
                Faction = Faction.Agnostic,
                Id = 49464,
                Name = "White oak",
                Level = 17,
                Locations = new[]
                {
                    "Ice Mountain",
                    "Kharid-et",
                    "Infernal Source",
                    "Everlight",
                    "Stormguard Citadel",
                    "Warforge",
                }
            };
            WingsofWar = new Material
            {
                Faction = Faction.Armadylean,
                Id = 49474,
                Name = "Wings of War",
                Level = 70,
                Locations = new[]
                {
                    "Stormguard Citadel(Dayguardtower)",
                    "Stormguard Citadel(Nightguard)",
                    "God Wars Dungeon(Armadylean,south-west)",
                }
            };
            YubiuskClay = new Material
            {
                Faction = Faction.Bandosian,
                Id = 49484,
                Name = "Yu'biusk clay",
                Level = 83,
                Locations = new[]
                {
                    "Warforge (animal pens)",
                    "Feldip(shores)",
                }
            };
            ZarosianInsignia = new Material
            {
                Faction = Faction.Zarosian,
                Id = 49514,
                Name = "Zarosian insignia",
                Level = 5,
                Locations = new[]
                {
                    "Kharid-et(barracks)",
                    "Empty Throne Room",
                }
            };
            Felt = new Material
            {
                Faction = Faction.Dragonkin,
                Id = 50692,
                Name = "Felt",
                Level = 90,
                Locations = new[]
                {
                    "Anachronia west excavation site",
                    "Orthen - Crypt of Varanus excavation site",
                    "Orthen - Observation outpost excavation site"
                }
            };
            CompassRose = new Material
            {
                Faction = Faction.Dragonkin,
                Id = 50688,
                Name = "Compass Rose",
                Level = 90,
                Locations = new[]
                {
                    "Ancient cavern excavation site",
                    "Orthen - Crypt of Varanus excavation site"
                }
            };
            DragonMetal = new Material
            {
                Faction = Faction.Dragonkin,
                Id = 50690,
                Name = "Dragon Metal",
                Level = 96,
                Locations = new[]
                {
                    "Ancient cavern excavation site",
                    "Orthen - Observation outpost excavation site",
                    "Orthen - Xolo city excavation site"
                }
            };
            Orgone = new Material
            {
                Faction = Faction.Dragonkin,
                Id = 50694,
                Name = "Orgone",
                Level = 99,
                Locations = new[]
                {
                    "Anachronia west excavation site",
                    "Orthen - Moksha ritual site excavation site",
                    "Orthen - Xolo city excavation site"
                }
            };
            CarbonBlack = new Material
            {
                Faction = Faction.Dragonkin,
                Id = 50686,
                Name = "Carbon Black",
                Level = 99,
                Locations = new[]
                {
                    "Mount Firewake excavation site",
                    "Orthen - Moksha ritual site excavation site"
                }
            };


            _all = new List<IMaterial>
            {
                AetheriumAlloy, AncientVis, AnimalFurs, ArmadyleanYellow, BlackMushroomInk, BloodofOrcus, BronzeBar,
                CadmiumRed, Clockwork, CobaltBlue, DeathRune, Demonhide, Diamond, Dragonstone,
                Emerald, EverlightSilvthril, EyeofDagon, ChaoticBrimstone, FossilisedBone, Goldrune, HellfireMetal,
                ImperialIron, ImperialSteel, Keramos, LeatherScraps, MalachiteGreen, MarkOfTheKyzaj,
                MoltenGlass, Orthenglass, PhoenixFeather, PurpleheartWood, Quintessence, Rope, Ruby, SamiteSilk,
                Sapphire, SilverBar, Soapstone, StarofSaradomin,
                StormguardSteel, ThirdAgeIron, TyrianPurple, Vellum, VulcanisedRubber, WarforgedBronze, WeaponPoison,
                WhiteCandle, WhiteMarble, WhiteOak, WingsofWar, YubiuskClay, ZarosianInsignia,
                Felt, CompassRose, DragonMetal, Orgone, CarbonBlack
            };
        }

        #endregion
    }
}
