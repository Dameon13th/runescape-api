﻿namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Enum containing a value for the Runescape gods that have a Archaeology digsite for
    /// </summary>
    public enum Faction
    {
        /// <summary>
        /// This value is not alligned with any of the gods.
        /// </summary>
        Agnostic,
        /// <summary>
        /// This value represnts the experienced god Armadyl.
        /// </summary>
        Armadylean,
        /// <summary>
        /// This value represents the hight god Bandos.
        /// </summary>
        Bandosian,
        /// <summary>
        /// This value represents the high god Saradomin.
        /// </summary>
        Saradominist,
        /// <summary>
        /// This value represents the experienced god Zamorak.
        /// </summary>
        Zamorakian,
        /// <summary>
        /// This value represents the transcedent god Zaros.
        /// </summary>
        Zarosian,
        /// <summary>
        /// This value represents the
        /// </summary>
        Dragonkin
    }
}