﻿using System.Collections.Generic;

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Class that contains a static instance of all known Archeology artifacts.
    /// </summary>
    public static class Artifacts
    {
        #region "Private Members"

        private static readonly List<IArtifact> _all;

        #endregion

        #region "Public Properties"

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Cenurions Dress Sword.
        /// </summary>
        public static IArtifact CenturionsDressSword { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Venator dagger.
        /// </summary>
        public static IArtifact VenatorDagger { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Venator light crossbow
        /// </summary>
        public static IArtifact VenatorLightCrossBow { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Centurion seal.
        /// </summary>
        public static IArtifact CenturionsSeal { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Legionary gladius.
        /// </summary>
        public static IArtifact LegionaryGladius { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Legionary square shield.
        /// </summary>
        public static IArtifact LegionarySquareShield { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Primis Elementis standard.
        /// </summary>
        public static IArtifact PrimisElementisStandard { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Zaros effigy.
        /// </summary>
        public static IArtifact ZarosEffigy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Zarosian training dummy.
        /// </summary>
        public static IArtifact ZarosianTrainingDummy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hookah pipe.
        /// </summary>
        public static IArtifact HookahPipe { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Opulent wine goblet,
        /// </summary>
        public static IArtifact OpulentWineGoblet { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Crest of dagon.
        /// </summary>
        public static IArtifact CrestOfDagon { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Disorder painting.
        /// </summary>
        public static IArtifact DisorderPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Legatus Maximus figurine.
        /// </summary>
        public static IArtifact LegatusMaximusFigurine { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Solem In Umbra painting.
        /// </summary>
        public static IArtifact SolemInUmbraPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Imp mask.
        /// </summary>
        public static IArtifact ImpMask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Lesser Demon mask.
        /// </summary>
        public static IArtifact LesserDemonMask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Greater Demon mask.
        /// </summary>
        public static IArtifact GreaterDemonMask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Order of Dis robes.
        /// </summary>
        public static IArtifact OrderOfDisRobes { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ritual dagger.
        /// </summary>
        public static IArtifact RitualDagger { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Frying pan.
        /// </summary>
        public static IArtifact FryingPan { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hallowed lantern.
        /// </summary>
        public static IArtifact HallowedLantern { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Branding Iron.
        /// </summary>
        public static IArtifact BrandingIron { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Manacles.
        /// </summary>
        public static IArtifact Manacles { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ancient timepiece.
        /// </summary>
        public static IArtifact AncientTimepiece { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Legatus pendant.
        /// </summary>
        public static IArtifact LegatusPendant { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ceremonial Unicorn ornament.
        /// </summary>
        public static IArtifact CeremonialUnicornOrnament { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ceremonial Unicorn saddle.
        /// </summary>
        public static IArtifact CeremonialUnicornSaddle { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Tetra compass.
        /// </summary>
        public static IArtifact Tetracompass { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Everlight harp.
        /// </summary>
        public static IArtifact EverlightHarp { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Everlight trumpet.
        /// </summary>
        public static IArtifact EverlightTrumpet { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Everlight violin.
        /// </summary>
        public static IArtifact EverlightViolin { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Folded Arm figurine (female).
        /// </summary>
        public static IArtifact FoldedArmFigurineFemale { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Folded Arm figurine (male).
        /// </summary>
        public static IArtifact FoldedArmFigurineMale { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pontifex signet ring.
        /// </summary>
        public static IArtifact PontifexSignetRing { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Incite fear spell scroll.
        /// </summary>
        public static IArtifact InciteFearSpellScroll { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dominion discuss.
        /// </summary>
        public static IArtifact DominionDiscus { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dominion javelin.
        /// </summary>
        public static IArtifact DominionJavelin { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dominion pelte shield.
        /// </summary>
        public static IArtifact DominionPelteShield { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Lake of fire painting.
        /// </summary>
        public static IArtifact TheLakeOfFirePainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Lust metal sculpture.
        /// </summary>
        public static IArtifact LustMetalSculpture { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Chaos star.
        /// </summary>
        public static IArtifact ChaosStar { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the spiked dog collar.
        /// </summary>
        public static IArtifact SpikedDogCollar { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the bronze Dominion medal.
        /// </summary>
        public static IArtifact BronzeDominionMedal { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the silver Dominion medal.
        /// </summary>
        public static IArtifact SilverDominionMedal { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dominion torch.
        /// </summary>
        public static IArtifact DominionTorch { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ikovian geregre.
        /// </summary>
        public static IArtifact IkovianGerege { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Toy glider.
        /// </summary>
        public static IArtifact ToyGlider { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Toy war golem.
        /// </summary>
        public static IArtifact ToyWarGolem { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Decorative vase.
        /// </summary>
        public static IArtifact DecorativeVase { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Patera bowl.
        /// </summary>
        public static IArtifact PateraBowl { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kantharos cup.
        /// </summary>
        public static IArtifact KantharosCup { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ceremonial mace/
        /// </summary>
        public static IArtifact CeremonialMace { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Consensus Ad Idem painting.
        /// </summary>
        public static IArtifact ConsensusAdIdemPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pontifex Maximus figurine.
        /// </summary>
        public static IArtifact PontifexMaximusFigurine { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Avian song egg player.
        /// </summary>
        public static IArtifact AvianSongEggPlayer { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Keshik drum.
        /// </summary>
        public static IArtifact KeshikDrum { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Moring khuur.
        /// </summary>
        public static IArtifact MorinKhuur { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ekelshunn binder mask.
        /// </summary>
        public static IArtifact EkeleshuunBinderMask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Narogoshuun Hobdagob ball.
        /// </summary>
        public static IArtifact NarogoshuunHobdaGobBall { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Rakeshuun war tether.
        /// </summary>
        public static IArtifact RekeshuunWarTether { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Avianse dreamcoat.
        /// </summary>
        public static IArtifact AviansieDreamcoat { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ceremonial plume.
        /// </summary>
        public static IArtifact CeremonialPlume { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Peacocking parasol.
        /// </summary>
        public static IArtifact PeacockingParasol { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ogre Kyzaj axe.
        /// </summary>
        public static IArtifact OgreKyzajAxe { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ork cleaver sword.
        /// </summary>
        public static IArtifact OrkCleaverSword { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Larupia trophy.
        /// </summary>
        public static IArtifact LarupiaTrophy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Lion trophy.
        /// </summary>
        public static IArtifact LionTrophy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the She wolf trophy.
        /// </summary>
        public static IArtifact SheWolfTrophy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pontifex censer.
        /// </summary>
        public static IArtifact PontifexCenser { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pontifex crozier.
        /// </summary>
        public static IArtifact PontifexCrozier { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pontifex mitre.
        /// </summary>
        public static IArtifact PontifexMitre { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Thorobshuun battle standard.
        /// </summary>
        public static IArtifact ThorobshuunBattleStandard { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Yurkolgokh stink grendae.
        /// </summary>
        public static IArtifact YurkolgokhStinkGrenade { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Cominarian device.
        /// </summary>
        public static IArtifact DominarianDevice { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Fishing trident.
        /// </summary>
        public static IArtifact FishingTrident { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hakweye lense multivision scope.
        /// </summary>
        public static IArtifact HawkeyeLensMultiVisionScope { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Talon 3 razor wing/
        /// </summary>
        public static IArtifact Talon3RazorWing { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Necrpmantic focu.
        /// </summary>
        public static IArtifact NecromanticFocus { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Exanguinate spell scroll.
        /// </summary>
        public static IArtifact ExsanguinateSpellScroll { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the High priest crozier.
        /// </summary>
        public static IArtifact HighPriestCrozier { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the High priest mitre.
        /// </summary>
        public static IArtifact HighPriestMitre { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the High priest orb.
        /// </summary>
        public static IArtifact HighPriestOrb { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pandemonium tapestry.
        /// </summary>
        public static IArtifact PandemoniumTapestry { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Tormented metal sculpture/
        /// </summary>
        public static IArtifact TormentMetalSculpture { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Prototype Gravimeter.
        /// </summary>
        public static IArtifact PrototypeGravimeter { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Songbird recorder.
        /// </summary>
        public static IArtifact SongbirdRecorder { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Amphora.
        /// </summary>
        public static IArtifact Amphora { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Rod of Asclepius.
        /// </summary>
        public static IArtifact RodOfAsclepius { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Zarosian ewer.
        /// </summary>
        public static IArtifact ZarosianEwer { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Zarosian stein.
        /// </summary>
        public static IArtifact ZarosianStein { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Beastkeeper helm.
        /// </summary>
        public static IArtifact BeastkeeperHelm { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Idithuun horn ring.
        /// </summary>
        public static IArtifact IdithuunHornRing { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Nosorog sculpture.
        /// </summary>
        public static IArtifact NosorogSculpture { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Stormguard gerege.
        /// </summary>
        public static IArtifact StormguardGerege { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dayguard shield.
        /// </summary>
        public static IArtifact DayguardShield { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Garagorshuun anchor.
        /// </summary>
        public static IArtifact GaragorshuunAnchor { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ourg megahitter.
        /// </summary>
        public static IArtifact OurgMegahitter { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ourg tower\ Goblin cower shield/
        /// </summary>
        public static IArtifact OurgTowerGoblinCowerShield { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Golem heart.
        /// </summary>
        public static IArtifact GolemHeart { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Golem instruction.
        /// </summary>
        public static IArtifact GolemInstruction { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hellfire haladie.
        /// </summary>
        public static IArtifact HellfireHaladie { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hellfire katar.
        /// </summary>
        public static IArtifact HellfireKatar { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hellfire zaghnal.
        /// </summary>
        public static IArtifact HellfireZaghnal { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dorgeshuun spear.
        /// </summary>
        public static IArtifact DorgeshuunSpear { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Forged in war sculpture/
        /// </summary>
        public static IArtifact ForgedInWarSculpture { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kopis dagger.
        /// </summary>
        public static IArtifact KopisDagger { get; }
        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Xiphos short sword.
        /// </summary>
        public static IArtifact XiphosShortSword { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Smoke cloud spell scroll.
        /// </summary>
        public static IArtifact SmokeCloudSpellScroll { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Virgorem vial.
        /// </summary>
        public static IArtifact VigoremVial { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Blackfire lance.
        /// </summary>
        public static IArtifact BlackfireLance { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Nightguard shield.
        /// </summary>
        public static IArtifact NightguardShield { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Huzamogaarb choas crown/
        /// </summary>
        public static IArtifact HuzamogaarbChoasCrown { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Saragotgak start crown.
        /// </summary>
        public static IArtifact SaragorgakStarCrown { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Possessopm metal sculpture/
        /// </summary>
        public static IArtifact PossessionMetalSculpture { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Trishula.
        /// </summary>
        public static IArtifact Trishula { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Tsutsaroth piercing.
        /// </summary>
        public static IArtifact TsutsarothPiercing { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the pride of Panosan painting.
        /// </summary>
        public static IArtifact ThePrideOfPadosanPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hallowed be the Everlight painting.
        /// </summary>
        public static IArtifact HallowedBeTheEverlightPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the lord of light painting.
        /// </summary>
        public static IArtifact TheLordOfLightPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ancien magic tablet.
        /// </summary>
        public static IArtifact AncientMagicTablet { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the portable phylactery.
        /// </summary>
        public static IArtifact PortablePhylactery { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the animate dead spell scroll.
        /// </summary>
        public static IArtifact AnimateDeadSpellSCroll { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Enlighted sould scroll.
        /// </summary>
        public static IArtifact TheEnlightendSoulSCroll { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Eudoxian element tablet.
        /// </summary>
        public static IArtifact TheEudoxianElementTablet { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Drogokishuun hook sword.
        /// </summary>
        public static IArtifact DrogokishuunHookSword { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hobgoblin mansticker.
        /// </summary>
        public static IArtifact HobgoblinMansticker { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Choas elemental trophy.
        /// </summary>
        public static IArtifact ChaosElementalTrophy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Virius trophy.
        /// </summary>
        public static IArtifact ViriusTrophy { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Flat cap.
        /// </summary>
        public static IArtifact FlatCap { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Night owl flight goggles.
        /// </summary>
        public static IArtifact NightOwnFlightGoggles { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Prototype god bow.
        /// </summary>
        public static IArtifact PrototypeGodBow { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Prototype god staff.
        /// </summary>
        public static IArtifact PrototypeGodStaff { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the prototype god sword.
        /// </summary>
        public static IArtifact PrototypeGodSword { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Praetorian hood.
        /// </summary>
        public static IArtifact PraetorianHood { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Praetorian robes.
        /// </summary>
        public static IArtifact PraetorianRobes { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Praetorian staff.
        /// </summary>
        public static IArtifact PraetorianStaff { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kali Kra chieftan crown.
        /// </summary>
        public static IArtifact KaliKraChieftainCrown { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kali Kra mace.
        /// </summary>
        public static IArtifact KaliKraMace { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kali Kra war horn.
        /// </summary>
        public static IArtifact KaliKraWarHorn { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Spear of Annihilation.
        /// </summary>
        public static IArtifact SpearOfAnnihilation { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Tsutsaroth helm.
        /// </summary>
        public static IArtifact TsutsarothHelm { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Tsutsaroth pauldron.
        /// </summary>
        public static IArtifact TsutsarothPauldron { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Tsutsaroth urumi.
        /// </summary>
        public static IArtifact TsutsarothUrumi { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kantos lance.
        /// </summary>
        public static IArtifact KontosLane { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Doru spear.
        /// </summary>
        public static IArtifact DoruSpear { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Chuluu stone.
        /// </summary>
        public static IArtifact ChuluuStone { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Quintessence counter.
        /// </summary>
        public static IArtifact QuintessenceCounter { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Sperical astrolabe.
        /// </summary>
        public static IArtifact SphericalAstrolabe { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ancient globe.
        /// </summary>
        public static IArtifact AncientGlobe { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Battle plans.
        /// </summary>
        public static IArtifact BattlePlans { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Prima Legio painting.
        /// </summary>
        public static IArtifact PrimaLegioPainting { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Horogothar cooking pot.
        /// </summary>
        public static IArtifact HorogotharCookingPot { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Da Boss man sculpture.
        /// </summary>
        public static IArtifact DaBossManSculture { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Apex Cap.
        /// </summary>
        public static IArtifact ApexCap { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Curse Tablet.
        /// </summary>
        public static IArtifact CurseTablet { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Funerary Urn Of Shadow.
        /// </summary>
        public static IArtifact FuneraryUrnOfShadow { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Infula Robes.
        /// </summary>
        public static IArtifact InfulaRobes { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Funerary Urn Of Smoke.
        /// </summary>
        public static IArtifact FuneraryUrnOfSmoke { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Hand Of The Ancients.
        /// </summary>
        public static IArtifact HandOfTheAncients { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Decorative Amphora.
        /// </summary>
        public static IArtifact DecorativeAmphora { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Funerary Urn Of Ice.
        /// </summary>
        public static IArtifact FuneraryUrnOfIce { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Loarnab Rod.
        /// </summary>
        public static IArtifact LoarnabRod { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Inquisitors Seal.
        /// </summary>
        public static IArtifact InquisitorsSeal { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Inquisitors Ceremonial Mask.
        /// </summary>
        public static IArtifact InquisitorsCeremonialMask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Inquisitors Ceremonial Armour.
        /// </summary>
        public static IArtifact InquisitorsCeremonialArmour { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Gladiator Sword.
        /// </summary>
        public static IArtifact GladiatorSword { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Gladiator Helmet.
        /// </summary>
        public static IArtifact GladiatorHelmet { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Funerary Urn Of Blood.
        /// </summary>
        public static IArtifact FuneraryUrnOfBlood { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the The Serpant's Fall Carving.
        /// </summary>
        public static IArtifact TheSerpantsFallCarving { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the ModelChariot.
        /// </summary>
        public static IArtifact ModelChariot { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Funerary Urn Of Miasma.
        /// </summary>
        public static IArtifact FuneraryUrnOfMiasma { get; }





        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Pasaha.
        /// </summary>
        public static IArtifact Pasaha { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Ritual Bell.
        /// </summary>
        public static IArtifact RitualBell { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Kilaya.
        /// </summary>
        public static IArtifact Kilaya { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Vazara.
        /// </summary>
        public static IArtifact Vazara { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Death Mask.
        /// </summary>
        public static IArtifact DeathMask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dragonkin Calender.
        /// </summary>
        public static IArtifact DragonkinCalender { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dragonkin Staff.
        /// </summary>
        public static IArtifact DragonkinStaff { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dragon Scalpel.
        /// </summary>
        public static IArtifact DragonScalpel { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Protective Goggles.
        /// </summary>
        public static IArtifact ProtectiveGoggles { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Dragon Burner.
        /// </summary>
        public static IArtifact DragonBurner { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Orthenglass Flask.
        /// </summary>
        public static IArtifact OrthenglassFlask { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Meditation Pipe.
        /// </summary>
        public static IArtifact MeditationPipe { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Personal Totem.
        /// </summary>
        public static IArtifact PersonalTotem { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Singing Bowl.
        /// </summary>
        public static IArtifact SingingBowl { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Lingam Stone.
        /// </summary>
        public static IArtifact LingamStone { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Master Control.
        /// </summary>
        public static IArtifact MasterControl { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Xolo Hard Hat.
        /// </summary>
        public static IArtifact XoloHardHat { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Xolo Pickaxe.
        /// </summary>
        public static IArtifact XoloPickaxe { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Xolo Shield.
        /// </summary>
        public static IArtifact XoloShield { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Xolo Spear.
        /// </summary>
        public static IArtifact XoloSpear { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Gold Dish.
        /// </summary>
        public static IArtifact GoldDish { get; }

        /// <summary>
        /// Gets an instance of <see cref="IArtifact"/> that contains all the data for the Raksha Idol.
        /// </summary>
        public static IArtifact RakshaIdol { get; }

        /// <summary>
        /// Gets read only list of all the artifacts.
        /// </summary>
        public static IReadOnlyList<IArtifact> All
        {
            get { return _all; }
        }

        #endregion

        #region "Constructor"

        static Artifacts()
        {
            CenturionsDressSword = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Centurion's Dress Sword",
                Level = 1,
                Experience = 250,
                Chronotes = 0,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialIron, 5},
                    {Materials.PurpleheartWood, 5}
                }
            };
            VenatorDagger = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Venator Dagger",
                Level = 5,
                Experience = 305.1,
                Chronotes = 206,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 16},
                    {Materials.ZarosianInsignia, 12}
                }
            };
            VenatorLightCrossBow = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Venator Light Crossbow",
                Level = 5,
                Experience = 305.1,
                Chronotes = 206,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 12},
                    {Materials.ZarosianInsignia, 16}
                }
            };
            CenturionsSeal = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Centurion's Seal",
                Level = 12,
                Experience = 430.8,
                Chronotes = 0,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 6},
                    {Materials.ZarosianInsignia, 2}
                }
            };
            LegionaryGladius = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Legionary Gladius",
                Level = 12,
                Experience = 430.8,
                Chronotes = 220,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 10},
                    {Materials.ZarosianInsignia, 6},
                    {Materials.ImperialSteel,12 }
                }
            };
            LegionarySquareShield = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Legionary Square Shield",
                Level = 12,
                Experience = 430.8,
                Chronotes = 220,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 8},
                    {Materials.ZarosianInsignia, 8},
                    {Materials.ImperialSteel,12 }
                }
            };
            PrimisElementisStandard = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Primis Elementis Standard",
                Level = 12,
                Experience = 430.8,
                Chronotes = 220,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 16},
                    {Materials.ThirdAgeIron, 12}
                }
            };
            ZarosEffigy = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Zaros Effigy",
                Level = 17,
                Experience = 520.5,
                Chronotes = 244,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 8},
                    {Materials.WhiteOak, 10},
                    {Materials.ZarosianInsignia,12 }
                }
            };
            ZarosianTrainingDummy = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Zarosian Training Dummy",
                Level = 17,
                Experience = 520.5,
                Chronotes = 244,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 16},
                    {Materials.WhiteOak, 14}
                }
            };
            HookahPipe = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Hookah Pipe",
                Level = 20,
                Experience = 574.4,
                Chronotes = 250,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 10},
                    {Materials.Goldrune, 12},
                    {Materials.Orthenglass, 8}
                }
            };
            OpulentWineGoblet = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Opulent Wine Goblet",
                Level = 20,
                Experience = 574.4,
                Chronotes = 250,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 14},
                    {Materials.Goldrune, 16}
                }
            };
            CrestOfDagon = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Crest of Dagon",
                Level = 24,
                Experience = 646.2,
                Chronotes = 272,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 14},
                    {Materials.Orthenglass, 18}
                }
            };
            DisorderPainting = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'Disorder' painting ",
                Level = 24,
                Experience = 646.2,
                Chronotes = 272,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 6},
                    {Materials.WhiteOak, 6},
                    {Materials.Vellum, 6},
                    {Materials.CadmiumRed, 14 }
                }
            };
            LegatusMaximusFigurine = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Legatus Maximus Figurine",
                Level = 25,
                Experience = 664.1,
                Chronotes = 274,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 8},
                    {Materials.ZarosianInsignia, 14},
                    {Materials.AncientVis, 10}
                }
            };
            SolemInUmbraPainting = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Solem in Umbra' painting",
                Level = 25,
                Experience = 664.1,
                Chronotes = 274,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 8},
                    {Materials.WhiteOak, 10},
                    {Materials.TyrianPurple, 14}
                }
            };
            ImpMask = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Imp Mask",
                Level = 29,
                Experience = 735.9,
                Chronotes = 282,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.LeatherScraps, 10},
                    {Materials.ChaoticBrimstone, 10},
                    {Materials.Demonhide, 12}
                }
            };
            LesserDemonMask = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Lesser Demon Mask",
                Level = 29,
                Experience = 735.9,
                Chronotes = 282,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.LeatherScraps, 6},
                    {Materials.ChaoticBrimstone, 8},
                    {Materials.Demonhide, 12},
                    {Materials.CadmiumRed, 6}
                }
            };
            GreaterDemonMask = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Greater Demon Mask",
                Level = 29,
                Experience = 735.9,
                Chronotes = 282,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 6},
                    {Materials.LeatherScraps, 6},
                    {Materials.ChaoticBrimstone, 8},
                    {Materials.Demonhide, 12}
                }
            };
            OrderOfDisRobes = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Order of Dis Robes",
                Level = 36,
                Experience = 861.5,
                Chronotes = 352,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 16},
                    {Materials.CadmiumRed, 10},
                    {Materials.EyeofDagon, 14}
                }
            };
            RitualDagger = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Ritual Dagger",
                Level = 36,
                Experience = 861,
                Chronotes = 352,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 16},
                    {Materials.HellfireMetal, 24},
                    {Materials.Ruby, 1}
                }
            };
            FryingPan = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "'Frying pan'",
                Level = 42,
                Experience = 1073.3,
                Chronotes = 392,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 20},
                    {Materials.WhiteMarble, 24},
                }
            };
            HallowedLantern = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Hallowed Lantern",
                Level = 42,
                Experience = 1073.3,
                Chronotes = 392,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 20},
                    {Materials.Keramos, 24},
                    {Materials.WhiteCandle, 1 }
                }
            };
            BrandingIron = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Branding Iron",
                Level = 45,
                Experience = 1283.3,
                Chronotes = 412,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 14},
                    {Materials.EyeofDagon, 12},
                    {Materials.HellfireMetal,20 }
                }
            };
            Manacles = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Manacles",
                Level = 42,
                Experience = 1283.3,
                Chronotes = 392,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 14},
                    {Materials.ChaoticBrimstone, 18},
                    {Materials.EyeofDagon, 14}
                }
            };
            AncientTimepiece = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Ancient Timepiece",
                Level = 47,
                Experience = 1423.3,
                Chronotes = 416,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 12},
                    {Materials.ImperialSteel, 16},
                    {Materials.AncientVis, 18}
                }
            };
            LegatusPendant = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Legatus Pendant",
                Level = 47,
                Experience = 1423.3,
                Chronotes = 416,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 16},
                    {Materials.Goldrune, 18},
                    {Materials.AncientVis, 12},
                    {Materials.Dragonstone, 1}
                }
            };
            CeremonialUnicornOrnament = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Ceremonial Unicorn Ornament",
                Level = 48,
                Experience = 1493.3,
                Chronotes = 418,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Keramos, 26},
                    {Materials.CobaltBlue, 20}
                }
            };
            CeremonialUnicornSaddle = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Ceremonial Unicorn Saddle",
                Level = 48,
                Experience = 1493.3,
                Chronotes = 418,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.LeatherScraps, 24},
                    {Materials.CobaltBlue, 22}
                }
            };
            Tetracompass = new Artifact
            {
                Faction = Faction.Agnostic,
                Name = "Tetracompass (unpowered)",
                Level = 50,
                Experience = 2065.0,
                Chronotes = 0,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MalachiteGreen, 30},
                    {Materials.CadmiumRed, 30},
                    {Materials.CobaltBlue, 30},
                    {Materials.ArmadyleanYellow, 30},
                    {Materials.TyrianPurple, 30}
                }
            };
            EverlightHarp = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Everlight Harp",
                Level = 51,
                Experience = 1703.3,
                Chronotes = 466,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 30},
                    {Materials.WhiteOak, 22}
                }
            };
            EverlightTrumpet = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Everlight Trumpet",
                Level = 51,
                Experience = 1703.3,
                Chronotes = 466,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 28},
                    {Materials.Goldrune, 24}
                }
            };
            EverlightViolin = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Everlight Violin",
                Level = 51,
                Experience = 1703.3,
                Chronotes = 466,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StarofSaradomin, 16},
                    {Materials.WhiteOak, 20},
                    {Materials.SamiteSilk, 16}
                }
            };
            FoldedArmFigurineFemale = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Folded-arm figurine (female)",
                Level = 56,
                Experience = 2053.3,
                Chronotes = 490,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteMarble, 30},
                    {Materials.Goldrune, 24}
                }
            };
            FoldedArmFigurineMale = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Folded-arm figurine (male)",
                Level = 56,
                Experience = 2053.3,
                Chronotes = 490,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteMarble, 30},
                    {Materials.Goldrune, 24}
                }
            };
            PontifexSignetRing = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Pontifex Signet Ring",
                Level = 58,
                Experience = 2193.3,
                Chronotes = 508,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 16},
                    {Materials.Goldrune, 18},
                    {Materials.AncientVis, 22},
                    {Materials.Dragonstone, 1}
                }
            };
            InciteFearSpellScroll = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Incite Fear' spell scroll",
                Level = 58,
                Experience = 2193.3,
                Chronotes = 508,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 20},
                    {Materials.AncientVis, 18},
                    {Materials.BloodofOrcus, 18}
                }
            };
            DominionDiscus = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Dominion Discus",
                Level = 61,
                Experience = 2566.7,
                Chronotes = 556,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Keramos, 34},
                    {Materials.StarofSaradomin, 28}
                }
            };
            DominionJavelin = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Dominion Javelin",
                Level = 61,
                Experience = 2566.7,
                Chronotes = 556,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Keramos, 32},
                    {Materials.ThirdAgeIron, 30}
                }
            };
            DominionPelteShield = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Dominion Pelte Shield",
                Level = 61,
                Experience = 2566.7,
                Chronotes = 556,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StarofSaradomin, 34},
                    {Materials.SamiteSilk, 28}
                }
            };
            TheLakeOfFirePainting = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'The Lake of Fie' painting",
                Level = 65,
                Experience = 3500,
                Chronotes = 578,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 10},
                    {Materials.WhiteOak, 10},
                    {Materials.Vellum, 10},
                    {Materials.CadmiumRed, 34},
                }
            };
            LustMetalSculpture = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'Lust' metal sculpture",
                Level = 65,
                Experience = 3500,
                Chronotes = 578,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 16},
                    {Materials.EyeofDagon, 24},
                    {Materials.Goldrune, 24},
                    {Materials.Ruby, 1},
                }
            };
            ChaosStar = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Chaos Star",
                Level = 68,
                Experience = 4200,
                Chronotes = 584,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ChaoticBrimstone, 28},
                    {Materials.HellfireMetal, 36}
                }
            };
            SpikedDogCollar = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Spiked dog collar",
                Level = 68,
                Experience = 4200,
                Chronotes = 584,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 24},
                    {Materials.LeatherScraps, 24},
                    {Materials.ChaoticBrimstone, 16}
                }
            };
            BronzeDominionMedal = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Bronze Dominion medal",
                Level = 69,
                Experience = 4433.3,
                Chronotes = 572,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 36},
                    {Materials.StarofSaradomin, 26},
                    {Materials.BronzeBar, 1}
                }
            };
            SilverDominionMedal = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Silver Dominion medal",
                Level = 69,
                Experience = 4433.3,
                Chronotes = 572,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 36},
                    {Materials.StarofSaradomin, 26},
                    {Materials.SilverBar, 1}
                }
            };
            DominionTorch = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Dominion torch",
                Level = 69,
                Experience = 4433.3,
                Chronotes = 572,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 12},
                    {Materials.Orthenglass, 12},
                    {Materials.EverlightSilvthril, 20},
                    {Materials.StarofSaradomin, 18}
                }
            };
            IkovianGerege = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Ikovian Gerege",
                Level = 70,
                Experience = 4666.7,
                Chronotes = 602,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 36},
                    {Materials.WingsofWar, 30}
                }
            };
            ToyGlider = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Toy Glider",
                Level = 70,
                Experience = 4666.7,
                Chronotes = 602,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 36},
                    {Materials.WhiteOak, 30}
                }
            };
            ToyWarGolem = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Toy War Golem",
                Level = 70,
                Experience = 4666.7,
                Chronotes = 602,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 36},
                    {Materials.WhiteOak, 30},
                    {Materials.Clockwork, 1}
                }
            };
            DecorativeVase = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Decorative Vase",
                Level = 72,
                Experience = 5133.3,
                Chronotes = 606,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteMarble, 36},
                    {Materials.CobaltBlue, 30}
                }
            };
            PateraBowl = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Patera bowl",
                Level = 72,
                Experience = 5133.3,
                Chronotes = 606,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Keramos, 36},
                    {Materials.Goldrune, 30},
                    {Materials.Sapphire, 1}
                }
            };
            KantharosCup = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Kantharos cup",
                Level = 72,
                Experience = 5133.3,
                Chronotes = 606,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 30},
                    {Materials.Orthenglass, 36},
                    {Materials.Sapphire, 1}
                }
            };
            CeremonialMace = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Ceremonial mace",
                Level = 74,
                Experience = 5600,
                Chronotes = 624,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 20},
                    {Materials.ThirdAgeIron, 20},
                    {Materials.Goldrune, 28}
                }
            };
            ConsensusAdIdemPainting = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Consensus ad Idem' painting",
                Level = 74,
                Experience = 5600,
                Chronotes = 624,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteOak, 10},
                    {Materials.SamiteSilk, 10},
                    {Materials.TyrianPurple, 50}
                }
            };
            PontifexMaximusFigurine = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Pontifex Maximus figurine",
                Level = 74,
                Experience = 5600,
                Chronotes = 624,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ZarosianInsignia, 24},
                    {Materials.AncientVis, 16},
                    {Materials.Goldrune, 28},
                    {Materials.Dragonstone, 1}
                }
            };
            AvianSongEggPlayer = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Avian song-egg player",
                Level = 76,
                Experience = 6066.7,
                Chronotes = 628,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 36},
                    {Materials.ArmadyleanYellow, 32},
                    {Materials.Diamond, 1}
                }
            };
            KeshikDrum = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Keshik drum",
                Level = 76,
                Experience = 6066.7,
                Chronotes = 628,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WingsofWar, 16},
                    {Materials.AnimalFurs, 16},
                    {Materials.WhiteOak, 20},
                    {Materials.LeatherScraps, 16}
                }
            };
            MorinKhuur = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Morin khuur",
                Level = 76,
                Experience = 6066.7,
                Chronotes = 628,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ArmadyleanYellow, 36},
                    {Materials.WhiteOak, 32}
                }
            };
            EkeleshuunBinderMask = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Ekelshuun blinder mask",
                Level = 76,
                Experience = 6066.7,
                Chronotes = 628,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.VulcanisedRubber, 36},
                    {Materials.MalachiteGreen, 32},
                    {Materials.Vellum, 24}
                }
            };
            NarogoshuunHobdaGobBall = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Narogoshuun 'Hob-da-Gob' ball",
                Level = 76,
                Experience = 6066.7,
                Chronotes = 628,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.VulcanisedRubber, 36},
                    {Materials.MarkOfTheKyzaj, 32}
                }
            };
            RekeshuunWarTether = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Rekeshuun war tether",
                Level = 76,
                Experience = 6066.7,
                Chronotes = 628,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 20},
                    {Materials.VulcanisedRubber, 22},
                    {Materials.LeatherScraps, 26}
                }
            };
            AviansieDreamcoat = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Aviansie dreamcoat",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ArmadyleanYellow, 20},
                    {Materials.SamiteSilk, 30},
                    {Materials.AnimalFurs, 22}
                }
            };
            CeremonialPlume = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Ceremonial plume",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ArmadyleanYellow, 38},
                    {Materials.Goldrune, 34},
                    {Materials.PhoenixFeather, 1}
                }
            };
            PeacockingParasol = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Peacocking parasol",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ArmadyleanYellow, 22},
                    {Materials.SamiteSilk, 30},
                    {Materials.WhiteOak, 20}
                }
            };
            OgreKyzajAxe = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Ogre Kyzaj axe",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MarkOfTheKyzaj, 20},
                    {Materials.FossilisedBone, 24}
                }
            };
            OrkCleaverSword = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Ork cleaver sword",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 36},
                    {Materials.FossilisedBone, 36}
                }
            };
            LarupiaTrophy = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Larupia trophy",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.CadmiumRed, 18},
                    {Materials.AnimalFurs, 28},
                    {Materials.Orthenglass, 26}
                }
            };
            LionTrophy = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Lion trophy",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.CadmiumRed, 18},
                    {Materials.AnimalFurs, 28},
                    {Materials.WhiteOak, 26}
                }
            };
            SheWolfTrophy = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "She-wolf trophy",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ChaoticBrimstone, 26},
                    {Materials.CadmiumRed, 18},
                    {Materials.AnimalFurs, 28}
                }
            };
            PontifexCenser = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Pontifex censer",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 20},
                    {Materials.AncientVis, 20},
                    {Materials.Goldrune, 32},
                    {Materials.Dragonstone, 1}
                }
            };
            PontifexCrozier = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Pontifex crozier",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 20},
                    {Materials.ZarosianInsignia, 20},
                    {Materials.Goldrune, 32}
                }
            };
            PontifexMitre = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Pontifex mitre",
                Level = 81,
                Experience = 7388.9,
                Chronotes = 666,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 32},
                    {Materials.AncientVis, 20},
                    {Materials.ZarosianInsignia, 20}
                }
            };
            ThorobshuunBattleStandard = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Thorobshuun battle standard",
                Level = 83,
                Experience = 8166.7,
                Chronotes = 684,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MarkOfTheKyzaj, 16},
                    {Materials.MalachiteGreen, 22},
                    {Materials.WhiteOak, 16},
                    {Materials.SamiteSilk, 20}
                }
            };
            YurkolgokhStinkGrenade = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Yurkolgokh stink grenade",
                Level = 83,
                Experience = 8166.7,
                Chronotes = 684,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.YubiuskClay, 38},
                    {Materials.VulcanisedRubber, 36},
                    {Materials.WeaponPoison, 1},
                }
            };
            DominarianDevice = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Dominarian device",
                Level = 84,
                Experience = 8555.6,
                Chronotes = 686,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 30},
                    {Materials.Keramos, 22},
                    {Materials.ThirdAgeIron, 22},
                    {Materials.Clockwork, 1}
                }
            };
            FishingTrident = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Fishing trident",
                Level = 84,
                Experience = 8555.6,
                Chronotes = 686,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StarofSaradomin, 22},
                    {Materials.ThirdAgeIron, 30},
                    {Materials.Goldrune, 22}
                }
            };
            HawkeyeLensMultiVisionScope = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Hawkeye lens multi-vision scope",
                Level = 85,
                Experience = 8944.4,
                Chronotes = 688,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 40},
                    {Materials.Orthenglass, 34}
                }
            };
            Talon3RazorWing = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Talon-3 razor wing",
                Level = 85,
                Experience = 8944.4,
                Chronotes = 688,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 40},
                    {Materials.WingsofWar, 34},
                    {Materials.Rope, 1}
                }
            };
            NecromanticFocus = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Necromantic focus",
                Level = 86,
                Experience = 9333.3,
                Chronotes = 704,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 20},
                    {Materials.BloodofOrcus, 26},
                    {Materials.AncientVis, 30}
                }
            };
            ExsanguinateSpellScroll = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Exsanguinate' spell scroll",
                Level = 86,
                Experience = 9333.3,
                Chronotes = 704,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 40},
                    {Materials.BloodofOrcus, 36}
                }
            };
            HighPriestCrozier = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "High priest crozier",
                Level = 89,
                Experience = 10500.0,
                Chronotes = 724,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MarkOfTheKyzaj, 26},
                    {Materials.MalachiteGreen, 24},
                    {Materials.Goldrune, 28}
                }
            };
            HighPriestMitre = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "High priest mitre",
                Level = 89,
                Experience = 10500.0,
                Chronotes = 724,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MarkOfTheKyzaj, 26},
                    {Materials.MalachiteGreen, 24},
                    {Materials.SamiteSilk, 28}
                }
            };
            HighPriestOrb = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "High priest orb",
                Level = 89,
                Experience = 10500.0,
                Chronotes = 724,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MarkOfTheKyzaj, 26},
                    {Materials.MalachiteGreen, 24},
                    {Materials.Goldrune, 28}
                }
            };
            PandemoniumTapestry = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'Pandemonium' tapestry",
                Level = 89,
                Experience = 10500.0,
                Chronotes = 724,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteOak, 12},
                    {Materials.SamiteSilk, 12},
                    {Materials.Vellum, 12},
                    {Materials.CadmiumRed, 42}
                }
            };
            TormentMetalSculpture = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'Torment' metal sculpture",
                Level = 89,
                Experience = 10500.0,
                Chronotes = 724,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EyeofDagon, 20},
                    {Materials.ThirdAgeIron, 20},
                    {Materials.HellfireMetal, 38}
                }
            };
            PrototypeGravimeter = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Prototype gravimeter",
                Level = 91,
                Experience = 11277.8,
                Chronotes = 742,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Quintessence, 34},
                    {Materials.LeatherScraps, 20},
                    {Materials.ThirdAgeIron, 26}
                }
            };
            SongbirdRecorder = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Songbird recorder",
                Level = 91,
                Experience = 11277.8,
                Chronotes = 742,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 44},
                    {Materials.Orthenglass, 36},
                    {Materials.Diamond, 1}
                }
            };
            Amphora = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Amphora",
                Level = 92,
                Experience = 11666.7,
                Chronotes = 742,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 34},
                    {Materials.Keramos, 46}
                }
            };
            RodOfAsclepius = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Rod of Asclepius",
                Level = 92,
                Experience = 11666.7,
                Chronotes = 742,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteMarble, 30},
                    {Materials.StarofSaradomin, 24},
                    {Materials.Goldrune, 26}
                }
            };
            ZarosianEwer = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Zarosian ewer",
                Level = 93,
                Experience = 12500,
                Chronotes = 760,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 52},
                    {Materials.ZarosianInsignia, 30}
                }
            };
            ZarosianStein = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Zarosian stein",
                Level = 93,
                Experience = 12500,
                Chronotes = 760,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ThirdAgeIron, 16},
                    {Materials.ImperialSteel, 36},
                    {Materials.ZarosianInsignia, 30}
                }
            };
            BeastkeeperHelm = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Beastkeeper helm",
                Level = 94,
                Experience = 13333.3,
                Chronotes = 776,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 16},
                    {Materials.VulcanisedRubber, 24},
                    {Materials.AnimalFurs, 20},
                    {Materials.FossilisedBone, 24}
                }
            };
            IdithuunHornRing = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Idithuun horn ring",
                Level = 94,
                Experience = 13333.3,
                Chronotes = 776,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.YubiuskClay, 40},
                    {Materials.VulcanisedRubber, 44}
                }
            };
            NosorogSculpture = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "'Nosorog' sculpture",
                Level = 94,
                Experience = 13333.3,
                Chronotes = 776,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.YubiuskClay, 30},
                    {Materials.MalachiteGreen, 24},
                    {Materials.WarforgedBronze, 30}
                }
            };
            StormguardGerege = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Stormguard gerege",
                Level = 95,
                Experience = 14166.7,
                Chronotes = 778,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 36},
                    {Materials.WingsofWar, 28},
                    {Materials.Goldrune, 20}
                }
            };
            DayguardShield = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Dayguard shield",
                Level = 95,
                Experience = 14166.7,
                Chronotes = 778,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 36},
                    {Materials.WingsofWar, 28},
                    {Materials.WhiteOak, 20}
                }
            };
            GaragorshuunAnchor = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Garagorshuun anchor",
                Level = 97,
                Experience = 15833.3,
                Chronotes = 810,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 32},
                    {Materials.MarkOfTheKyzaj, 26},
                    {Materials.ThirdAgeIron, 30}
                }
            };
            OurgMegahitter = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Ourg megahitter",
                Level = 97,
                Experience = 15833.3,
                Chronotes = 810,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteOak, 20},
                    {Materials.LeatherScraps, 20},
                    {Materials.Orthenglass, 26},
                    {Materials.MalachiteGreen, 22}
                }
            };
            OurgTowerGoblinCowerShield = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Ourg tower/Goblin cower shield",
                Level = 97,
                Experience = 15833.3,
                Chronotes = 810,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.MarkOfTheKyzaj, 20},
                    {Materials.ThirdAgeIron, 26},
                    {Materials.LeatherScraps, 22},
                    {Materials.WhiteOak, 20}
                }
            };
            GolemHeart = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Golem heart",
                Level = 98,
                Experience = 16666.7,
                Chronotes = 826,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 34},
                    {Materials.Quintessence, 24},
                    {Materials.Orthenglass, 16},
                    {Materials.Soapstone, 16}
                }
            };
            GolemInstruction = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Golem instruction",
                Level = 98,
                Experience = 16666.7,
                Chronotes = 826,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Quintessence, 46},
                    {Materials.Vellum, 44},
                    {Materials.BlackMushroomInk, 1}
                }
            };
            HellfireHaladie = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Hellfire haladie",
                Level = 98,
                Experience = 16666.7,
                Chronotes = 826,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 44},
                    {Materials.ThirdAgeIron, 26},
                    {Materials.LeatherScraps, 20}
                }
            };
            HellfireKatar = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Hellfire katar",
                Level = 98,
                Experience = 16666.7,
                Chronotes = 826,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 50},
                    {Materials.LeatherScraps, 40}
                }
            };
            HellfireZaghnal = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Hellfire zaghnal",
                Level = 98,
                Experience = 16666.7,
                Chronotes = 826,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 38},
                    {Materials.WhiteOak, 26},
                    {Materials.Orthenglass, 26}
                }
            };
            DorgeshuunSpear = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Dorgeshuun spear",
                Level = 100,
                Experience = 18666.7,
                Chronotes = 844,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 50},
                    {Materials.WhiteOak, 42}
                }
            };
            ForgedInWarSculpture = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "'Forged in War' sculpture",
                Level = 100,
                Experience = 18666.7,
                Chronotes = 844,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 50},
                    {Materials.YubiuskClay, 42},
                    {Materials.Emerald, 1}
                }
            };
            KopisDagger = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Kophis dagger",
                Level = 100,
                Experience = 18666.7,
                Chronotes = 844,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 50},
                    {Materials.LeatherScraps, 42}
                }
            };
            XiphosShortSword = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Xiphos short sword",
                Level = 100,
                Experience = 18666.7,
                Chronotes = 844,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 46},
                    {Materials.LeatherScraps, 46}
                }
            };
            SmokeCloudSpellScroll = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Smoke Cloud' spell scroll",
                Level = 100,
                Experience = 18666.7,
                Chronotes = 844,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 40},
                    {Materials.AncientVis, 20},
                    {Materials.BloodofOrcus, 32}
                }
            };
            VigoremVial = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Vigorem vial",
                Level = 100,
                Experience = 18666.7,
                Chronotes = 844,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 54},
                    {Materials.AncientVis, 38},
                    {Materials.MoltenGlass, 1}
                }
            };
            BlackfireLance = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Blackfire lance",
                Level = 103,
                Experience = 22166.7,
                Chronotes = 878,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 50},
                    {Materials.Quintessence, 46}
                }
            };
            NightguardShield = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Nightguard shield",
                Level = 103,
                Experience = 22166.7,
                Chronotes = 878,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StormguardSteel, 30},
                    {Materials.WingsofWar, 30},
                    {Materials.WhiteOak, 30}
                }
            };
            HuzamogaarbChoasCrown = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Huzamogaarb chaos crown",
                Level = 104,
                Experience = 23333.3,
                Chronotes = 894,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 44},
                    {Materials.ThirdAgeIron, 34},
                    {Materials.EyeofDagon, 20}
                }
            };
            SaragorgakStarCrown = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Saragorgak star crown",
                Level = 104,
                Experience = 23333.3,
                Chronotes = 894,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 44},
                    {Materials.ThirdAgeIron, 34},
                    {Materials.StarofSaradomin, 20}
                }
            };
            PossessionMetalSculpture = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'Possession' metal sculpture",
                Level = 104,
                Experience = 23333.3,
                Chronotes = 894,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EyeofDagon, 24},
                    {Materials.ChaoticBrimstone, 30},
                    {Materials.ThirdAgeIron, 40}
                }
            };
            Trishula = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Trishula",
                Level = 104,
                Experience = 23333.3,
                Chronotes = 894,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 48},
                    {Materials.EyeofDagon, 30},
                    {Materials.ThirdAgeIron, 20}
                }
            };
            TsutsarothPiercing = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "'Tsutsaroth piercing",
                Level = 104,
                Experience = 23333.3,
                Chronotes = 894,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 44},
                    {Materials.ChaoticBrimstone, 30},
                    {Materials.CadmiumRed, 24}
                }
            };
            ThePrideOfPadosanPainting = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "'The Pride of Padosan' painting",
                Level = 106,
                Experience = 24500,
                Chronotes = 910,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.CobaltBlue, 52},
                    {Materials.WhiteOak, 16},
                    {Materials.SamiteSilk, 16},
                    {Materials.Vellum, 16}
                }
            };
            HallowedBeTheEverlightPainting = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "'Hallowed Be the Everlight' painting",
                Level = 106,
                Experience = 24500,
                Chronotes = 910,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.CobaltBlue, 52},
                    {Materials.WhiteOak, 16},
                    {Materials.SamiteSilk, 16},
                    {Materials.Vellum, 16}
                }
            };
            TheLordOfLightPainting = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "'The Lord of Light' painting",
                Level = 106,
                Experience = 24500,
                Chronotes = 910,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.CobaltBlue, 52},
                    {Materials.WhiteOak, 16},
                    {Materials.SamiteSilk, 16},
                    {Materials.Vellum, 16}
                }
            };
            AncientMagicTablet = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Ancient magic tablet",
                Level = 107,
                Experience = 27000,
                Chronotes = 942,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AncientVis, 40},
                    {Materials.BloodofOrcus, 64}
                }
            };
            PortablePhylactery = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Portable Phylactery",
                Level = 107,
                Experience = 27000,
                Chronotes = 942,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 48},
                    {Materials.BloodofOrcus, 36},
                    {Materials.AncientVis, 20}
                }
            };
            AnimateDeadSpellSCroll = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Animate Dead' spell scroll",
                Level = 107,
                Experience = 27000,
                Chronotes = 942,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 40},
                    {Materials.AncientVis, 24},
                    {Materials.BloodofOrcus, 40}
                }
            };
            TheEnlightendSoulSCroll = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "'The Enlightened Soul' scroll",
                Level = 109,
                Experience = 29666.7,
                Chronotes = 988,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.StarofSaradomin, 50},
                    {Materials.Vellum, 60}
                }
            };
            TheEudoxianElementTablet = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "'The Eudoxian Elements' tablet",
                Level = 109,
                Experience = 29666.7,
                Chronotes = 988,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteMarble, 60},
                    {Materials.Goldrune, 50}
                }
            };
            DrogokishuunHookSword = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Drogokishuun hook sword",
                Level = 110,
                Experience = 31000,
                Chronotes = 1004,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 44},
                    {Materials.MalachiteGreen, 36},
                    {Materials.FossilisedBone, 32}
                }
            };
            HobgoblinMansticker = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Hobgoblin mansticker",
                Level = 110,
                Experience = 31000,
                Chronotes = 1004,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WarforgedBronze, 66},
                    {Materials.FossilisedBone, 46}
                }
            };
            ChaosElementalTrophy = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Chaos Elemental trophy",
                Level = 110,
                Experience = 31000,
                Chronotes = 1004,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ChaoticBrimstone, 52},
                    {Materials.WhiteOak, 30},
                    {Materials.HellfireMetal, 30}
                }
            };
            ViriusTrophy = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Virius trophy",
                Level = 110,
                Experience = 31000,
                Chronotes = 1004,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Demonhide, 44},
                    {Materials.WhiteOak, 34},
                    {Materials.Orthenglass, 34}
                }
            };
            FlatCap = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Flat cap",
                Level = 111,
                Experience = 32333.3,
                Chronotes = 1004,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ArmadyleanYellow, 60},
                    {Materials.SamiteSilk, 54}
                }
            };
            NightOwnFlightGoggles = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Night owl flight goggles",
                Level = 111,
                Experience = 32333.3,
                Chronotes = 1004,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ArmadyleanYellow, 44},
                    {Materials.LeatherScraps, 40},
                    {Materials.Orthenglass, 30}
                }
            };
            PrototypeGodBow = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Prototype godbow",
                Level = 112,
                Experience = 33666.7,
                Chronotes = 1050,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 50},
                    {Materials.Quintessence, 34},
                    {Materials.WingsofWar, 34}
                }
            };
            PrototypeGodStaff = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Prototype godstaff",
                Level = 112,
                Experience = 33666.7,
                Chronotes = 1050,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 50},
                    {Materials.Quintessence, 34},
                    {Materials.WingsofWar, 34}
                }
            };
            PrototypeGodSword = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Prototype godsword",
                Level = 112,
                Experience = 33666.7,
                Chronotes = 1050,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 50},
                    {Materials.WingsofWar, 34},
                    {Materials.Goldrune, 34}
                }
            };
            PraetorianHood = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Praetorian hood",
                Level = 114,
                Experience = 36666.7,
                Chronotes = 1096,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AncientVis, 36},
                    {Materials.SamiteSilk, 48},
                    {Materials.ZarosianInsignia, 40},
                    {Materials.DeathRune, 30}
                }
            };
            PraetorianRobes = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Praetorian robes",
                Level = 114,
                Experience = 36666.7,
                Chronotes = 1096,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AncientVis, 30},
                    {Materials.SamiteSilk, 54},
                    {Materials.ZarosianInsignia, 40},
                    {Materials.DeathRune, 50}
                }
            };
            PraetorianStaff = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Praetorian staff",
                Level = 114,
                Experience = 36666.7,
                Chronotes = 1096,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 36},
                    {Materials.AncientVis, 58},
                    {Materials.ZarosianInsignia, 30},
                    {Materials.DeathRune, 100}
                }
            };
            KaliKraChieftainCrown = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Kal-i-kra chieftain crown",
                Level = 115,
                Experience = 38333.3,
                Chronotes = 1112,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.YubiuskClay, 66},
                    {Materials.AnimalFurs, 60}
                }
            };
            KaliKraMace = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Kal-i-kra mace",
                Level = 115,
                Experience = 38333.3,
                Chronotes = 1112,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.VulcanisedRubber, 42},
                    {Materials.ThirdAgeIron, 44},
                    {Materials.FossilisedBone, 40}
                }
            };
            KaliKraWarHorn = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Kal-i-kra warhorn",
                Level = 115,
                Experience = 38333.3,
                Chronotes = 1112,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.VulcanisedRubber, 44},
                    {Materials.FossilisedBone, 42},
                    {Materials.AnimalFurs, 40}
                }
            };
            SpearOfAnnihilation = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Spear of Annihilation",
                Level = 115,
                Experience = 38333.3,
                Chronotes = 0,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.VulcanisedRubber, 500},
                    {Materials.MalachiteGreen, 500},
                    {Materials.Goldrune, 500}
                }
            };
            TsutsarothHelm = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Tsutsaroth helm",
                Level = 116,
                Experience = 40000,
                Chronotes = 1142,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 50},
                    {Materials.EyeofDagon, 40},
                    {Materials.Goldrune, 40}
                }
            };
            TsutsarothPauldron = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Tsutsaroth pauldron",
                Level = 116,
                Experience = 40000,
                Chronotes = 1142,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 40},
                    {Materials.Goldrune, 50},
                    {Materials.EyeofDagon, 40}
                }
            };
            TsutsarothUrumi = new Artifact
            {
                Faction = Faction.Zamorakian,
                Name = "Tsutsaroth urumi",
                Level = 116,
                Experience = 40000,
                Chronotes = 1142,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.HellfireMetal, 50},
                    {Materials.EyeofDagon, 40},
                    {Materials.ThirdAgeIron, 40}
                }
            };
            KontosLane = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Kontos lance",
                Level = 117,
                Experience = 41666.7,
                Chronotes = 1142,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 70},
                    {Materials.SamiteSilk, 62}
                }
            };
            DoruSpear = new Artifact
            {
                Faction = Faction.Saradominist,
                Name = "Doru spear",
                Level = 117,
                Experience = 41666.7,
                Chronotes = 1142,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.EverlightSilvthril, 70},
                    {Materials.WhiteOak, 62}
                }
            };
            ChuluuStone = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Chuluu stone",
                Level = 118,
                Experience = 43333.3,
                Chronotes = 1174,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 40},
                    {Materials.Quintessence, 30},
                    {Materials.Soapstone, 40},
                    {Materials.Goldrune, 24}
                }
            };
            QuintessenceCounter = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Quintessence counter",
                Level = 118,
                Experience = 43333.3,
                Chronotes = 1174,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Quintessence, 54},
                    {Materials.StormguardSteel, 40},
                    {Materials.WhiteOak, 40}
                }
            };
            SphericalAstrolabe = new Artifact
            {
                Faction = Faction.Armadylean,
                Name = "Spherical astrolabe",
                Level = 118,
                Experience = 43333.3,
                Chronotes = 1174,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AetheriumAlloy, 46},
                    {Materials.ArmadyleanYellow, 40},
                    {Materials.Orthenglass, 48}
                }
            };
            AncientGlobe = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Ancient globe",
                Level = 118,
                Experience = 43333.3,
                Chronotes = 1174,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteOak, 20},
                    {Materials.TyrianPurple, 54},
                    {Materials.AncientVis, 60}
                }
            };
            BattlePlans = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Battle plans",
                Level = 118,
                Experience = 43333.3,
                Chronotes = 1174,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 40},
                    {Materials.TyrianPurple, 60},
                    {Materials.AncientVis, 34}
                }
            };
            PrimaLegioPainting = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'Prima Legio' painting",
                Level = 118,
                Experience = 43333.3,
                Chronotes = 1174,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteOak, 20},
                    {Materials.SamiteSilk, 20},
                    {Materials.TyrianPurple, 74},
                    {Materials.ZarosianInsignia, 20}
                }
            };
            HorogotharCookingPot = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "Horogothar cooking pot",
                Level = 119,
                Experience = 45000,
                Chronotes = 1204,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.YubiuskClay, 60},
                    {Materials.MalachiteGreen, 38},
                    {Materials.Soapstone, 40}
                }
            };
            DaBossManSculture = new Artifact
            {
                Faction = Faction.Bandosian,
                Name = "'Da Boss Man' sculpture",
                Level = 119,
                Experience = 45000,
                Chronotes = 1204,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.YubiuskClay, 50},
                    {Materials.MalachiteGreen, 44},
                    {Materials.Soapstone, 44}
                }
            };
            ApexCap = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Apex Cap",
                Level = 60,
                Experience = 2333.3,
                Chronotes = 540,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 28 },
                    {Materials.LeatherScraps, 12 },
                    {Materials.AncientVis, 20 }
                }
            };
            CurseTablet = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Curse Tablet",
                Level = 60,
                Experience = 2333.3,
                Chronotes = 540,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 16 },
                    {Materials.ZarosianInsignia, 12 },
                    {Materials.Soapstone, 20 },
                    {Materials.BloodofOrcus, 12 }
                }
            };
            FuneraryUrnOfShadow = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Funerary urn of shadow",
                Level = 60,
                Experience = 2333.3,
                Chronotes = 540,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Soapstone, 26 },
                    {Materials.TyrianPurple, 14 },
                    {Materials.AncientVis, 20 }
                }
            };
            InfulaRobes = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Infula Robes",
                Level = 62,
                Experience = 2800,
                Chronotes = 558,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.SamiteSilk, 26 },
                    {Materials.LeatherScraps, 12 },
                    {Materials.Goldrune, 12 },
                    {Materials.TyrianPurple, 12 }
                }
            };
            FuneraryUrnOfSmoke = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Funerary urn of smoke",
                Level = 62,
                Experience = 2800,
                Chronotes = 558,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Soapstone, 28 },
                    {Materials.TyrianPurple, 14 },
                    {Materials.AncientVis, 20 }
                }
            };
            HandOfTheAncients = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Hand of the Ancients",
                Level = 62,
                Experience = 2800,
                Chronotes = 558,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.BloodofOrcus, 12 },
                    {Materials.WhiteOak, 18 },
                    {Materials.AncientVis, 14 },
                    {Materials.Goldrune, 18 }
                }
            };
            DecorativeAmphora = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Decorative Amphora",
                Level = 63,
                Experience = 3033.3,
                Chronotes = 560,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.TyrianPurple, 16 },
                    {Materials.AncientVis, 18 },
                    {Materials.Soapstone, 28 }
                }
            };
            FuneraryUrnOfIce = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Funerary urn of ice",
                Level = 63,
                Experience = 3033.3,
                Chronotes = 560,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Soapstone, 28 },
                    {Materials.TyrianPurple, 14 },
                    {Materials.AncientVis, 20 }
                }
            };
            LoarnabRod = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Loarnab rod",
                Level = 63,
                Experience = 3033.3,
                Chronotes = 560,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.WhiteOak, 28 },
                    {Materials.BloodofOrcus, 16 },
                    {Materials.ImperialSteel, 18 }
                }
            };
            InquisitorsSeal = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Inquisitor's Seal",
                Level = 64,
                Experience = 3266.7,
                Chronotes = 562,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.TyrianPurple, 14 },
                    {Materials.ZarosianInsignia, 20 },
                    {Materials.AncientVis, 14 },
                    {Materials.Goldrune, 14 }
                }
            };
            InquisitorsCeremonialMask = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Inquisitor's Ceremonial Mask",
                Level = 64,
                Experience = 3266.7,
                Chronotes = 562,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.AncientVis, 14 },
                    {Materials.LeatherScraps, 12 },
                    {Materials.BloodofOrcus, 14 },
                    {Materials.SamiteSilk, 22 }
                }
            };
            InquisitorsCeremonialArmour = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Inquisitor's Ceremonial Armour",
                Level = 64,
                Experience = 3266.7,
                Chronotes = 562,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.LeatherScraps, 14 },
                    {Materials.SamiteSilk, 30 },
                    {Materials.TyrianPurple, 18 }
                }
            };
            GladiatorSword = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Gladiator Sword",
                Level = 66,
                Experience = 3773.3,
                Chronotes = 580,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 30 },
                    {Materials.Goldrune, 18 },
                    {Materials.ZarosianInsignia, 16 }
                }
            };
            GladiatorHelmet = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Gladiator Helmet",
                Level = 66,
                Experience = 3773.3,
                Chronotes = 580,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.ImperialSteel, 30 },
                    {Materials.BloodofOrcus, 16 },
                    {Materials.LeatherScraps, 18 }
                }
            };
            FuneraryUrnOfBlood = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Funerary urn of blood",
                Level = 66,
                Experience = 3773.3,
                Chronotes = 580,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Soapstone, 30 },
                    {Materials.TyrianPurple, 14 },
                    {Materials.BloodofOrcus, 20 }
                }
            };
            TheSerpantsFallCarving = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "'The Serpent's Fall' carving",
                Level = 67,
                Experience = 3966.7,
                Chronotes = 582,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 16 },
                    {Materials.TyrianPurple, 24 },
                    {Materials.BloodofOrcus, 12 },
                    {Materials.WhiteOak, 12 }
                }
            };
            ModelChariot = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Model chariot",
                Level = 67,
                Experience = 3966.7,
                Chronotes = 582,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Vellum, 12 },
                    {Materials.ImperialSteel, 18 },
                    {Materials.Goldrune, 20 },
                    {Materials.ZarosianInsignia, 14 }
                }
            };
            FuneraryUrnOfMiasma = new Artifact
            {
                Faction = Faction.Zarosian,
                Name = "Funerary urn of misama",
                Level = 67,
                Experience = 3966.7,
                Chronotes = 582,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Soapstone, 30 },
                    {Materials.TyrianPurple, 14 },
                    {Materials.AncientVis, 20 }
                }
            };
            Pasaha = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Pasaha",
                Level = 90,
                Experience = 10888.9,
                Chronotes = 726,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Felt, 40 },
                    {Materials.Goldrune, 38 }
                }
            };
            RitualBell = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Ritual Bell",
                Level = 90,
                Experience = 10888.9,
                Chronotes = 726,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 40 },
                    {Materials.CompassRose, 38 }
                }
            };
            Kilaya = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Kilaya",
                Level = 96,
                Experience = 15000,
                Chronotes = 794,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.DragonMetal, 46 },
                    {Materials.CompassRose, 40 }
                }
            };
            Vazara = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Vazara",
                Level = 96,
                Experience = 15000,
                Chronotes = 794,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.DragonMetal, 30 },
                    {Materials.CompassRose, 28 },
                    {Materials.Goldrune, 28 }
                }
            };
            DeathMask = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Death mask",
                Level = 99,
                Experience = 17500,
                Chronotes = 828,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 56 },
                    {Materials.Soapstone, 34 }
                }
            };
            DragonkinCalender = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Dragonkin Calender",
                Level = 99,
                Experience = 17500,
                Chronotes = 828,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 34 },
                    {Materials.CarbonBlack, 28 },
                    {Materials.CompassRose, 28 }
                }
            };
            DragonkinStaff = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Dragonkin Staff",
                Level = 99,
                Experience = 17500,
                Chronotes = 828,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 56 },
                    {Materials.CompassRose, 34 }
                }
            };
            DragonScalpel = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Dragon Scalpel",
                Level = 101,
                Experience = 19833.3,
                Chronotes = 860,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.DragonMetal, 52 },
                    {Materials.Felt, 42 }
                }
            };
            ProtectiveGoggles = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Protective Goggles",
                Level = 101,
                Experience = 19833.3,
                Chronotes = 860,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Felt, 42 },
                    {Materials.Orthenglass, 52 }
                }
            };
            DragonBurner = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Dragon Burner",
                Level = 102,
                Experience = 21000,
                Chronotes = 862,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.DragonMetal, 34 },
                    {Materials.Orgone, 42 }
                }
            };
            OrthenglassFlask = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Orthenglass Flask",
                Level = 102,
                Experience = 21000,
                Chronotes = 862,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.DragonMetal, 34 },
                    {Materials.Orthenglass, 60 }
                }
            };
            MeditationPipe = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Meditation Pipe",
                Level = 106,
                Experience = 25666.7,
                Chronotes = 912,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 60 },
                    {Materials.DragonMetal, 40 }
                }
            };
            PersonalTotem = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Personal Totem",
                Level = 106,
                Experience = 25666.7,
                Chronotes = 912,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 48 },
                    {Materials.CarbonBlack, 26 },
                    {Materials.CompassRose, 26 }
                }
            };
            SingingBowl = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Singing Bowl",
                Level = 106,
                Experience = 25666.7,
                Chronotes = 912,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 60 },
                    {Materials.CompassRose, 40 }
                }
            };
            LingamStone = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Lingam Stone",
                Level = 108,
                Experience = 28333.3,
                Chronotes = 958,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 44 },
                    {Materials.CarbonBlack, 30 },
                    {Materials.CompassRose, 32 }
                }
            };
            MasterControl = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Master Control",
                Level = 108,
                Experience = 28333.3,
                Chronotes = 958,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 30 },
                    {Materials.CarbonBlack, 32 },
                    {Materials.CompassRose, 44 }
                }
            };
            XoloHardHat = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Xolo Hard Hat",
                Level = 113,
                Experience = 35000,
                Chronotes = 1066,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 54 },
                    {Materials.DragonMetal, 66 }
                }
            };
            XoloPickaxe = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Xolo Pickaxe",
                Level = 113,
                Experience = 35000,
                Chronotes = 1066,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 36 },
                    {Materials.DragonMetal, 50 },
                    {Materials.Orgone, 34 }
                }
            };
            XoloShield = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Xolo Shield",
                Level = 119,
                Experience = 45000,
                Chronotes = 1204,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 52 },
                    {Materials.Orgone, 44 },
                    {Materials.Felt, 42 }
                }
            };
            XoloSpear = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Xolo Spear",
                Level = 119,
                Experience = 45000,
                Chronotes = 1204,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.DragonMetal, 74 },
                    {Materials.Orgone, 64 }
                }
            };
            GoldDish = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "Gold Dish",
                Level = 120,
                Experience = 46666.7,
                Chronotes = 1220,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Goldrune, 86 },
                    {Materials.DragonMetal, 54 }
                }
            };
            RakshaIdol = new Artifact
            {
                Faction = Faction.Dragonkin,
                Name = "'Raksha' Idol",
                Level = 120,
                Experience = 46666.7,
                Chronotes = 1220,
                RequiredMaterials = new Dictionary<IMaterial, int>
                {
                    {Materials.Orgone, 56 },
                    {Materials.DragonMetal, 44 },
                    {Materials.Goldrune, 40 }
                }
            };
            _all = new List<IArtifact>
            {
                CenturionsDressSword,
                VenatorDagger, VenatorDagger, CenturionsDressSword,
                LegionaryGladius, LegionarySquareShield,
                PrimisElementisStandard, ZarosEffigy, ZarosianTrainingDummy,
                HookahPipe, OpulentWineGoblet,
                CrestOfDagon, DisorderPainting,
                LegatusMaximusFigurine, SolemInUmbraPainting,
                ImpMask, LesserDemonMask, GreaterDemonMask,
                OrderOfDisRobes, RitualDagger,
                FryingPan, HallowedLantern,
                BrandingIron, Manacles,
                AncientTimepiece, LegatusPendant,
                CeremonialUnicornOrnament, CeremonialUnicornSaddle,
                Tetracompass,
                EverlightHarp, EverlightTrumpet, EverlightViolin,
                FoldedArmFigurineFemale, FoldedArmFigurineMale,
                PontifexSignetRing, InciteFearSpellScroll,
                DominionDiscus, DominionJavelin, DominionPelteShield,
                TheLakeOfFirePainting, LustMetalSculpture,
                ChaosStar, SpikedDogCollar,
                BronzeDominionMedal, SilverDominionMedal, DominionTorch,
                IkovianGerege, ToyGlider, ToyWarGolem,
                DecorativeVase, PateraBowl, KantharosCup,
                CeremonialMace, ConsensusAdIdemPainting, PontifexMaximusFigurine,
                AvianSongEggPlayer, KeshikDrum, MorinKhuur,
                EkeleshuunBinderMask, NarogoshuunHobdaGobBall, RekeshuunWarTether,
                AviansieDreamcoat, CeremonialPlume, PeacockingParasol,
                OgreKyzajAxe, OrkCleaverSword,
                LarupiaTrophy, LionTrophy, SheWolfTrophy,
                PontifexCenser, PontifexCrozier, PontifexMitre,
                ThorobshuunBattleStandard, YurkolgokhStinkGrenade,
                DominarianDevice, FishingTrident,
                HawkeyeLensMultiVisionScope, Talon3RazorWing,
                NecromanticFocus, ExsanguinateSpellScroll,
                HighPriestCrozier, HighPriestMitre, HighPriestOrb,
                PandemoniumTapestry, TormentMetalSculpture,
                PrototypeGravimeter, SongbirdRecorder,
                Amphora, RodOfAsclepius,
                ZarosianEwer, ZarosianStein,
                BeastkeeperHelm, IdithuunHornRing, NosorogSculpture,
                StormguardGerege, DayguardShield,
                GaragorshuunAnchor, OurgMegahitter, OurgTowerGoblinCowerShield,
                GolemHeart, GolemInstruction,
                HellfireHaladie, HellfireKatar, HellfireZaghnal,
                DorgeshuunSpear, ForgedInWarSculpture,
                KopisDagger, XiphosShortSword,
                SmokeCloudSpellScroll, VigoremVial,
                BlackfireLance, NightguardShield,
                HuzamogaarbChoasCrown, SaragorgakStarCrown,
                PossessionMetalSculpture, Trishula, TsutsarothPiercing,
                ThePrideOfPadosanPainting, HallowedBeTheEverlightPainting, TheLordOfLightPainting,
                AncientMagicTablet, PortablePhylactery, AnimateDeadSpellSCroll,
                TheEnlightendSoulSCroll, TheEudoxianElementTablet,
                DrogokishuunHookSword, HobgoblinMansticker,
                ChaosElementalTrophy, ViriusTrophy,
                FlatCap, NightOwnFlightGoggles,
                PrototypeGodBow, PrototypeGodStaff, PrototypeGodSword,
                PraetorianHood, PraetorianRobes, PraetorianStaff,
                KaliKraChieftainCrown, KaliKraMace, KaliKraWarHorn,
                SpearOfAnnihilation,
                TsutsarothHelm, TsutsarothPauldron, TsutsarothUrumi,
                KontosLane, DoruSpear,
                ChuluuStone, QuintessenceCounter, SphericalAstrolabe,
                AncientGlobe, BattlePlans, PrimaLegioPainting,
                HorogotharCookingPot, DaBossManSculture,
                ApexCap, CurseTablet, FuneraryUrnOfShadow,
                InfulaRobes, FuneraryUrnOfSmoke, HandOfTheAncients,
                DecorativeAmphora, FuneraryUrnOfIce, LoarnabRod,
                InquisitorsSeal, InquisitorsCeremonialMask, InquisitorsCeremonialArmour,
                GladiatorSword, GladiatorHelmet, FuneraryUrnOfBlood,
                TheSerpantsFallCarving, ModelChariot, FuneraryUrnOfMiasma,
                Pasaha, RitualBell,
                Kilaya, Vazara,
                DeathMask, DragonkinCalender, DragonkinStaff,
                DragonScalpel, ProtectiveGoggles,
                DragonBurner, OrthenglassFlask,
                MeditationPipe, PersonalTotem, SingingBowl,
                LingamStone, MasterControl,
                XoloHardHat, XoloPickaxe, 
                XoloShield, XoloSpear, 
                GoldDish, RakshaIdol
            };
        }

        #endregion
    }
}