﻿using System.Collections.Generic;

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Interface that defines the properties of an Artifact.
    /// </summary>
    public interface IArtifact
    {
        /// <summary>
        /// Gets the faction for which this artifact belongs.
        /// </summary>
        Faction Faction { get; }

        /// <summary>
        /// Gets the name of the artifact.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Archaeology level required to repair this artifact.
        /// </summary>
        int Level { get; }

        /// <summary>
        /// Get the number of Chronotes the user gets for turning in this repaired artifact.
        /// </summary>
        long Chronotes { get; }

        /// <summary>
        /// Gets the number of experience points the user gets for repairing this artifact.
        /// </summary>
        double Experience { get; }

        /// <summary>
        /// Gets a read only dictionry of the required  <see cref="IMaterial"/>s needed to fix this artifact.
        /// The key is the <see cref="IMaterial"/>.
        /// The value is the required number of those materials.
        /// </summary>
        IReadOnlyDictionary<IMaterial, int> RequiredMaterials { get; }
    }
}