﻿using System.Collections.Generic;

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Interface that defines the properties of a material.
    /// </summary>
    public interface IMaterial
    {
        /// <summary>
        /// Gets the faction for which this material belongs.
        /// </summary>
        Faction Faction { get; }

        /// <summary>
        /// Gets the name of this material.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Archaeology level required to gather this material.
        /// </summary>
        int Level { get; }

        /// <summary>
        /// Gets the Id of the material. Can be used with the <see cref="IGrandExchange"/> API.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Gets a read only list of locations this material can be gathered.
        /// </summary>
        IReadOnlyList<string> Locations { get; }
    }
}