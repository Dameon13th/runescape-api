﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Runescape.Api.Model;

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Class that allows the consumer to add materials to. Once materials are added this class will let you know the cost to purchase all the materials from the Grand Exchange.
    /// </summary>
    /// <example>
    /// The following example demonstrates adding two different artifact at differnt amounts to the storage.
    /// It also shows that as artifacts are added the <see cref="MaterialStorage.TotalMaterials"/> is updated with the materials required to repair then.
    /// <code language="cs">
    ///<![CDATA[
    /// MaterialStorage storage = new MaterialStorage();
    /// storage.Add(Materials.ThirdAgeIron); // Adds a single Third age iron
    /// storage.Add(Materials.ZarosianInsignia, 12); // Add twelve Zarosian insignias 
    ///
    /// // This will be 2 (Third age iron and Zarosian insignia)
    /// int totalMaterialTypeCount = storage.TotalMaterials.Keys.Count();
    ///
    /// // This value will be 13 (One third age iron plus twelve Zarosian insignias
    /// long count = TotalMaterials.Sum(valuePair => valuePair.Value);
    /// ]]>
    /// </code>
    /// </example>
    public class MaterialStorage
    {
        #region "Private Members"

        private readonly Dictionary<IMaterial, long> _materialCount;

        #endregion

        #region "Public Members"

        /// <summary>
        /// Gets a readonly dictionary where the key is an instance of <see cref="IMaterial"/> and the value is the count in this storage.
        /// </summary>
        public IReadOnlyDictionary<IMaterial, long> TotalMaterials
        {
            get { return _materialCount; }
        }

        #endregion

        #region "Constructor"

        /// <summary>
        /// Creats a new instance of the <b>MaterialStorage</b> class.
        /// </summary>
        public MaterialStorage()
        {
            _materialCount = new Dictionary<IMaterial, long>();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Add the given <paramref name="material"/> to the storage. If the material is already in storage
        /// the count is incremented by the given <paramref name="count"/>.
        /// </summary>
        /// <param name="material">The material to add to storage.</param>
        /// <param name="count">The number of materials to add.</param>
        public void Add(IMaterial material, long count = 1)
        {
            if (_materialCount.TryGetValue(material, out long materialCount))
            {
                materialCount += count;
            }
            else
            {
                materialCount = count;
            }
            _materialCount[material] = materialCount;
        }

        /// <summary>
        /// Removes the given <paramref name="material"/> from storage.
        /// If the materoal is not currently in storrage no action is taken.
        /// If the count for the given <paramref name="material"/> is less than ot equal to zero it is removed
        /// from storage. 
        /// </summary>
        /// <param name="material">The material to remove from storage.</param>
        /// <param name="count">The number of materials to remove.</param>
        public void Remove(IMaterial material, long count = 1)
        {
            if (!_materialCount.TryGetValue(material, out long materialCount))
            {
                return;
            }

            materialCount -= count;
            if (materialCount <= 0)
            {
                _materialCount.Remove(material);
                return;
            }

            _materialCount[material] = materialCount;
        }


        /// <summary>
        /// Caclulate the how much all the materials in storage are worth in the Grand Exchange.
        /// </summary>
        /// <param name="grandExchange">The Grand Exchange API used to get the current price.</param>
        /// <returns>An awaitable task that when completed will contain the total worth of the materials in storage.</returns>
        /// <remarks>
        /// An Http REST request is made for each material type in storage.
        /// </remarks>
        public async Task<long> CaclulateWorth(IGrandExchange grandExchange)
        {
            long total = 0;

            foreach (KeyValuePair<IMaterial, long> material in _materialCount)
            {
                IItemDetail itemDetail = await grandExchange.GetItemDetailAsync(material.Key.Id);
                if (itemDetail == null)
                {
                    continue;
                }
                total += itemDetail.Current.Value * material.Value;
            }

            return total;
        }

        #endregion

        #region "Operators"

        /// <summary>
        /// Adds all the materials locateded in <paramref name="b"/> into <paramref name="a"/>.
        /// </summary>
        /// <param name="a">The storage to add to.</param>
        /// <param name="b">The storage to add from.</param>
        /// <returns>Returns <paramref name="a"/> with all the material from <paramref name="b"/> added.</returns>
        public static MaterialStorage operator +(MaterialStorage a, MaterialStorage b)
        {
            foreach (KeyValuePair<IMaterial, long> kvp in b._materialCount)
            {
                a.Add(kvp.Key, kvp.Value);
            }
            return a;
        }

        /// <summary>
        /// Removes all the materials located in <paramref name="b"/> from <paramref name="a"/>.
        /// </summary>
        /// <param name="a">The storage to remove from.</param>
        /// <param name="b">The storage to remove</param>
        /// <returns>Returns <paramref name="a"/> with all the materials from <paramref name="b"/> removed.</returns>
        public static MaterialStorage operator -(MaterialStorage a, MaterialStorage b)
        {
            foreach (KeyValuePair<IMaterial, long> kvp in b._materialCount)
            {
                a.Remove(kvp.Key, kvp.Value);
            }
            return a;
        }

        #endregion
    }
}