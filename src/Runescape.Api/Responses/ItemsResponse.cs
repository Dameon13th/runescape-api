﻿using Newtonsoft.Json;
using Runescape.Api.Model;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Runescape.Api
{
    [DataContract]
    internal class ItemsResponse : IItemsResult
    {
        static ItemsResponse()
        {
            Empty = new ItemsResponse
            {
                Items = new List<Item>(),
                Category = GrandExchangeCategory.Miscellaneous,
                FirstLetter = '?'
            };
        }

        public GrandExchangeCategory Category { get; set; }

        public char FirstLetter { get; set; }

        [JsonProperty("total")]
        public int Count { get; set; }

        public int PageNumber { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; }

        IReadOnlyList<IItem> IItemsResult.Items
        {
            get { return Items; }
        }

        public static ItemsResponse Empty { get; }
    }
}