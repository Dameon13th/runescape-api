﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for results of getting items by their <see cref="GrandExchangeCategory"/> and first letter of their name.
    /// </summary>
    public interface IItemsResult
    {
        /// <summary>
        /// Gets the Grand Exchange category used to perform this search.
        /// </summary>
        GrandExchangeCategory Category { get; }

        /// <summary>
        /// Gets the first letter used to get the items.
        /// </summary>
        char FirstLetter { get; }

        /// <summary>
        /// Gets a total of items that belong to the <see cref="Category"/>
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets the page number used to perform this search.
        /// </summary>
        int PageNumber { get; }

        /// <summary>
        /// Gets a readonly collection of <see cref="IItem"/> that fit the search criteria.
        /// </summary>
        IReadOnlyList<IItem> Items { get; }
    }
}