﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Runescape.Api
{
    [DataContract]
    internal class LastUpdatedResponse
    {
        [JsonProperty("lastConfigUpdateRuneday")]
        public int DaysSinceUpdate { get; set; }
    }
}