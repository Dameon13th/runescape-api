﻿using Newtonsoft.Json;
using Runescape.Api.Model;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Runescape.Api
{
    [DataContract]
    internal class GetCategoriesResponse
    {
        [JsonProperty("alpha")]
        public List<CountByLetter> CountByLetter { get; set; }
    }
}