﻿using Newtonsoft.Json;
using Runescape.Api.Model;
using System.Runtime.Serialization;

namespace Runescape.Api
{
    [DataContract]
    internal class ItemDetailResponse
    {
        [DataMember(Name = "item")]
        [JsonProperty("item")]
        public ItemDetail Item { get; set; }
    }
}