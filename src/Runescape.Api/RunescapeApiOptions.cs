﻿using Runescape.Api;

internal class RunescapeApiOptions
{
    public CacheOptions CacheOptions { get; set; }

    public RunescapeApiOptions()
    {
        CacheOptions = CacheOptions.None;
    }
}