﻿using System;

namespace Runescape.Api
{
    /// <summary>
    /// Class used to determine how the APIs cache their responses.
    /// </summary>
    public class CacheOptions
    {
        #region "Public Properties"

        /// <summary>
        /// An instance of <see cref="CacheOptions"/> that provides no caching.
        /// </summary>
        public static CacheOptions None { get; private set; }

        /// <summary>
        /// An instance of <see cref="CacheOptions"/> that will cause every item retrieved to be cached.
        /// </summary>
        public static CacheOptions Permanent { get; private set; }

        #endregion

        #region "Internal Properties"

        internal CacheType CacheType { get; private set; }

        internal TimeSpan CacheTime { get; set; }

        #endregion

        #region "Constructors"

        static CacheOptions()
        {
            None = new CacheOptions(CacheType.None);
            Permanent = new CacheOptions(CacheType.Permanent);
        }

        private CacheOptions(CacheType cacheType)
        {
            CacheType = cacheType;
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Create a new instance of <see cref="CacheOptions"/> that will cause the retrieved items to be cached for a given <paramref name="timeSpan"/>
        /// </summary>
        /// <param name="timeSpan">The amount of time an item should remain cached.</param>
        /// <returns>A new instance of <see cref="CacheOptions"/>.</returns>
        public static CacheOptions FromTimeSpan(TimeSpan timeSpan)
        {
            return new CacheOptions(CacheType.Timed)
            {
                CacheTime = timeSpan
            };
        }

        #endregion
    }
}