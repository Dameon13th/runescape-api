﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing values for all of the slayer categories.
    /// </summary>
    /// <remarks>
    /// This enum was generated using the results of the <see cref="IBestiary.GetSlayerCategoriesAsync"/>. As such it may not contain any new categories that were added after generation.
    /// </remarks>
    public enum SlayerCategory
    {
        /// <summary>
        /// Monkeys
        /// </summary>
        Monkeys = 1,
        /// <summary>
        /// Goblins
        /// </summary>
        Goblins = 2,
        /// <summary>
        /// Rats
        /// </summary>
        Rats = 3,
        /// <summary>
        /// Spiders
        /// </summary>
        Spiders = 4,
        /// <summary>
        /// Birds
        /// </summary>
        Birds = 5,
        /// <summary>
        /// Cows
        /// </summary>
        Cows = 6,
        /// <summary>
        /// Scorpions
        /// </summary>
        Scorpions = 7,
        /// <summary>
        /// Bats
        /// </summary>
        Bats = 8,
        /// <summary>
        /// Wolves
        /// </summary>
        Wolves = 9,
        /// <summary>
        /// Zombies
        /// </summary>
        Zombies = 10,
        /// <summary>
        /// Skeletons
        /// </summary>
        Skeletons = 11,
        /// <summary>
        /// Ghosts
        /// </summary>
        Ghosts = 12,
        /// <summary>
        /// Bears
        /// </summary>
        Bears = 13,
        /// <summary>
        /// Hill giants
        /// </summary>
        HillGiants = 14,
        /// <summary>
        /// Ice giants
        /// </summary>
        IceGiants = 15,
        /// <summary>
        /// Fire giants
        /// </summary>
        FireGiants = 16,
        /// <summary>
        /// Moss giants
        /// </summary>
        MossGiants = 17,
        /// <summary>
        /// Trolls
        /// </summary>
        Trolls = 18,
        /// <summary>
        /// Ice Warriors
        /// </summary>
        IceWarriors = 19,
        /// <summary>
        /// Ogres
        /// </summary>
        Ogres = 20,
        /// <summary>
        /// Hobgoblins
        /// </summary>
        Hobgoblins = 21,
        /// <summary>
        /// Dogs
        /// </summary>
        Dogs = 22,
        /// <summary>
        /// Ghouls
        /// </summary>
        Ghouls = 23,
        /// <summary>
        /// Green Dragons
        /// </summary>
        GreenDragons = 24,
        /// <summary>
        /// Blue Dragons
        /// </summary>
        BlueDragons = 25,
        /// <summary>
        /// Red Dragons
        /// </summary>
        RedDragons = 26,
        /// <summary>
        /// Black Dragons
        /// </summary>
        BlackDragons = 27,
        /// <summary>
        /// Lesser Demons
        /// </summary>
        LesserDemons = 28,
        /// <summary>
        /// Greater Demons
        /// </summary>
        GreaterDemons = 29,
        /// <summary>
        /// Black Demons
        /// </summary>
        BlackDemons = 30,
        /// <summary>
        /// Hellhounds
        /// </summary>
        Hellhounds = 31,
        /// <summary>
        /// Shadow Warriors
        /// </summary>
        ShadowWarriors = 32,
        /// <summary>
        /// Werewolves
        /// </summary>
        Werewolves = 33,
        /// <summary>
        /// Vampyres
        /// </summary>
        Vampyres = 34,
        /// <summary>
        /// Dagannoth
        /// </summary>
        Dagannoth = 35,
        /// <summary>
        /// Turoth
        /// </summary>
        Turoth = 36,
        /// <summary>
        /// Cave crawlers
        /// </summary>
        CaveCrawlers = 37,
        /// <summary>
        /// Banshees
        /// </summary>
        Banshees = 38,
        /// <summary>
        /// Crawling hands
        /// </summary>
        CrawlingHands = 39,
        /// <summary>
        /// Infernal mages
        /// </summary>
        InfernalMages = 40,
        /// <summary>
        /// Aberrant spectres
        /// </summary>
        AberrantSpectres = 41,
        /// <summary>
        /// Abyssal Demons
        /// </summary>
        AbyssalDemons = 42,
        /// <summary>
        /// Basilisks
        /// </summary>
        Basilisks = 43,
        /// <summary>
        /// Cockatrice
        /// </summary>
        Cockatrice = 44,
        /// <summary>
        /// Kurask
        /// </summary>
        Kurask = 45,
        /// <summary>
        /// Gargoyles
        /// </summary>
        Gargoyles = 46,
        /// <summary>
        /// Pyrefiends
        /// </summary>
        Pyrefiends = 47,
        /// <summary>
        /// Bloodveld
        /// </summary>
        Bloodveld = 48,
        /// <summary>
        /// Dust devils
        /// </summary>
        DustDevils = 49,
        /// <summary>
        /// Jellies
        /// </summary>
        Jellies = 50,
        /// <summary>
        /// Rockslugs
        /// </summary>
        Rockslugs = 51,
        /// <summary>
        /// Nechryael
        /// </summary>
        Nechryael = 52,
        /// <summary>
        /// Kalphite
        /// </summary>
        Kalphite = 53,
        /// <summary>
        /// Earth Warriors
        /// </summary>
        EarthWarriors = 54,
        /// <summary>
        /// Otherworldly beings
        /// </summary>
        OtherworldlyBeings = 55,
        /// <summary>
        /// Elves
        /// </summary>
        Elves = 56,
        /// <summary>
        /// Dwarves
        /// </summary>
        Dwarves = 57,
        /// <summary>
        /// Bronze Dragons
        /// </summary>
        BronzeDragons = 58,
        /// <summary>
        /// Iron Dragons
        /// </summary>
        IronDragons = 59,
        /// <summary>
        /// Steel Dragons
        /// </summary>
        SteelDragons = 60,
        /// <summary>
        /// Wall beasts
        /// </summary>
        WallBeasts = 61,
        /// <summary>
        /// Cave slimes
        /// </summary>
        CaveSlimes = 62,
        /// <summary>
        /// Cave bugs
        /// </summary>
        CaveBugs = 63,
        /// <summary>
        /// Shades
        /// </summary>
        Shades = 64,
        /// <summary>
        /// Crocodiles
        /// </summary>
        Crocodiles = 65,
        /// <summary>
        /// Dark beasts
        /// </summary>
        DarkBeasts = 66,
        /// <summary>
        /// Mogres
        /// </summary>
        Mogres = 67,
        /// <summary>
        /// Desert lizards
        /// </summary>
        DesertLizards = 68,
        /// <summary>
        /// Fever spiders
        /// </summary>
        FeverSpiders = 69,
        /// <summary>
        /// Harpie bug swarms
        /// </summary>
        HarpieBugSwarms = 70,
        /// <summary>
        /// Sea snakes
        /// </summary>
        SeaSnakes = 71,
        /// <summary>
        /// Skeletal wyverns
        /// </summary>
        SkeletalWyverns = 72,
        /// <summary>
        /// Killerwatts
        /// </summary>
        KillerWatts = 73,
        /// <summary>
        /// Mutated zygomites
        /// </summary>
        MutatedZygomites = 74,
        /// <summary>
        /// Icefiends
        /// </summary>
        Icefiends = 75,
        /// <summary>
        /// Minotaurs
        /// </summary>
        Minotaurs = 76,
        /// <summary>
        /// Fleshcrawlers
        /// </summary>
        FleshCrawlers = 77,
        /// <summary>
        /// Catablepon
        /// </summary>
        Catablepon = 78,
        /// <summary>
        /// Ankou
        /// </summary>
        Ankou = 79,
        /// <summary>
        /// Cave horrors
        /// </summary>
        CaveHorrors = 80,
        /// <summary>
        /// Jungle horrors
        /// </summary>
        JungleHorrors = 81,
        /// <summary>
        /// Goraks
        /// </summary>
        Goraks = 82,
        /// <summary>
        /// Suqahs
        /// </summary>
        Suqahs = 83,
        /// <summary>
        /// Brine rats
        /// </summary>
        Brinerats = 84,
        /// <summary>
        /// Scabarites
        /// </summary>
        Scabarites = 85,
        /// <summary>
        /// Terror dogs
        /// </summary>
        Terrordogs = 86,
        /// <summary>
        /// Molanisks
        /// </summary>
        Molanisks = 87,
        /// <summary>
        /// Waterfiends
        /// </summary>
        Waterfiends = 88,
        /// <summary>
        /// Spiritual Warriors
        /// </summary>
        SpiritualWarriors = 89,
        /// <summary>
        /// Spiritual rangers
        /// </summary>
        SpiritualRangers = 90,
        /// <summary>
        /// Spiritual mages
        /// </summary>
        SpiritualMages = 91,
        /// <summary>
        /// Warped tortoises
        /// </summary>
        WarpedTortoises = 92,
        /// <summary>
        /// Warped terrorbirds
        /// </summary>
        WarpedTerrorbirds = 93,
        /// <summary>
        /// Mithril Dragons
        /// </summary>
        MithrilDragons = 94,
        /// <summary>
        /// Aquanites
        /// </summary>
        Aquanites = 95,
        /// <summary>
        /// Ganodermic creatures
        /// </summary>
        GanodermicCreatures = 96,
        /// <summary>
        /// Grifolapines
        /// </summary>
        Grifolapines = 97,
        /// <summary>
        /// Grifolaroos
        /// </summary>
        Grifolaroos = 98,
        /// <summary>
        /// Fungal magi
        /// </summary>
        FungalMagi = 99,
        /// <summary>
        /// Polypore creatures
        /// </summary>
        PolyporeCreatures = 100,
        /// <summary>
        /// TzHaar
        /// </summary>
        TzHaar = 101,
        /// <summary>
        /// Volcanic creatures
        /// </summary>
        VolcanicCreatures = 102,
        /// <summary>
        /// Jungle Strykewyrms
        /// </summary>
        JungleStrykewyrms = 103,
        /// <summary>
        /// Desert Strykewyrms
        /// </summary>
        DesertStrykewyrms = 104,
        /// <summary>
        /// Ice Strykewyrms
        /// </summary>
        IceStrykewyrms = 105,
        /// <summary>
        /// Living rock creatures
        /// </summary>
        LivingRockCreatures = 106,
        /// <summary>
        /// Cyclopes
        /// </summary>
        Cyclopes = 108,
        /// <summary>
        /// Mutated jadinkos
        /// </summary>
        MutatedJadinkos = 109,
        /// <summary>
        /// Vyrewatch
        /// </summary>
        Vyrewatch = 110,
        /// <summary>
        /// Gelatinous abominations
        /// </summary>
        GelatinousAbominations = 111,
        /// <summary>
        /// Grotworms
        /// </summary>
        Grotworms = 112,
        /// <summary>
        /// Cres's creations
        /// </summary>
        CressCreations = 113,
        /// <summary>
        /// Aviansies
        /// </summary>
        Aviansies = 114,
        /// <summary>
        /// Ascension members
        /// </summary>
        AscensionMembers = 115,
        /// <summary>
        /// Pigs
        /// </summary>
        Pigs = 116,
        /// <summary>
        /// Airut
        /// </summary>
        Airut = 117,
        /// <summary>
        /// Celestial Dragons
        /// </summary>
        CelestialDragons = 118,
        /// <summary>
        /// Muspah
        /// </summary>
        Muspah = 119,
        /// <summary>
        /// Nihil
        /// </summary>
        Nihil = 120,
        /// <summary>
        /// Kal'gerion Demons
        /// </summary>
        KalgerionDemons = 121,
        /// <summary>
        /// Glacors
        /// </summary>
        Glacors = 122,
        /// <summary>
        /// Tormented Demons
        /// </summary>
        TormentedDemons = 123,
        /// <summary>
        /// Edimmu
        /// </summary>
        Edimmu = 124,
        /// <summary>
        /// Shadow creatures
        /// </summary>
        ShadowCreatures = 125,
        /// <summary>
        /// Lava Strykewyrms
        /// </summary>
        LavaStrykewyrms = 126,
        /// <summary>
        /// Adamant Dragons
        /// </summary>
        AdamantDragons = 127,
        /// <summary>
        /// Rune Dragons
        /// </summary>
        RuneDragons = 128,
        /// <summary>
        /// Crystal shapeshifters
        /// </summary>
        CrystalShapeshifters = 129,
        /// <summary>
        /// Living wyverns
        /// </summary>
        LivingWyverns = 130,
        /// <summary>
        /// Ripper Demons
        /// </summary>
        RipperDemons = 131,
        /// <summary>
        /// Camel Warriors
        /// </summary>
        CamelWarriors = 132,
        /// <summary>
        /// Acheron mammoths
        /// </summary>
        AcheronMammoths = 133,
        /// <summary>
        /// Chaos giants
        /// </summary>
        ChaosGiants = 134,
        /// <summary>
        /// Nightmare creatures
        /// </summary>
        NightmareCreatures = 135,
        /// <summary>
        /// Gemstone Dragons
        /// </summary>
        GemstoneDragons = 139,
        /// <summary>
        /// Corrupted scorpions
        /// </summary>
        CorruptedScorpions = 140,
        /// <summary>
        /// Corrupted scarabs
        /// </summary>
        CorruptedScarabs = 141,
        /// <summary>
        /// Corrupted lizards
        /// </summary>
        CorruptedLizards = 142,
        /// <summary>
        /// Corrupted dust devils
        /// </summary>
        CorruptedDustDevils = 143,
        /// <summary>
        /// Corrupted kalphites
        /// </summary>
        CorruptedKalphites = 144,
        /// <summary>
        /// Corrupted worker
        /// </summary>
        CorruptedWorker = 145,
        /// <summary>
        /// Salawa akh
        /// </summary>
        SalawaAkh = 146,
        /// <summary>
        /// Feline akh
        /// </summary>
        FelineAkh = 147,
        /// <summary>
        /// Gorilla akh
        /// </summary>
        GorillaAkh = 148,
        /// <summary>
        /// Crocodile akh
        /// </summary>
        CrocodileAkh = 149,
        /// <summary>
        /// Scarab akh
        /// </summary>
        ScarabAkh = 150,
        /// <summary>
        /// Imperial guard akh
        /// </summary>
        ImperialGuardAkh = 151,
        /// <summary>
        /// Corrupted creatures
        /// </summary>
        CorruptedCreatures = 152,
        /// <summary>
        /// Soul devourers
        /// </summary>
        SoulDevourers = 153,
        /// <summary>
        /// Creatures of the Lost Grove
        /// </summary>
        CreaturesOfTheLostGrove = 154,
        /// <summary>
        /// Stalker Creatures
        /// </summary>
        StalkerCreatures = 160,
        /// <summary>
        /// Revenants
        /// </summary>
        Revenants = 161,
        /// <summary>
        /// Frogs
        /// </summary>
        Frogs = 162,
        /// <summary>
        /// Dinosaurs
        /// </summary>
        Dinosaurs = 171,
        /// <summary>
        /// Vile blooms
        /// </summary>
        Vileblooms = 172,
        /// <summary>
        /// Dragons
        /// </summary>
        Dragons = 173,
        /// <summary>
        /// Demons
        /// </summary>
        Demons = 174,
        /// <summary>
        /// Creatures of Daemonheim
        /// </summary>
        CreaturesFfDaemonheim = 175,
        /// <summary>
        /// Zarosian creatures
        /// </summary>
        ZarosianCreatures = 176,
        /// <summary>
        /// Strykewyrms
        /// </summary>
        Strykewyrms = 177,
    }
}