﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing values for all of the skills.
    /// </summary>
    public enum Skill
    {
        /// <summary>
        /// The overall total skill.
        /// </summary>
        Overall = 0,
        /// <summary>
        /// Grants players using melee weapons higher accuracy, thus increasing the number of hits dealt to an opponent during any given time period. Since more of the player's hits will be successful, the player's opponent will incur more damage per any given period of time. Stronger weapons also require higher levels of attack in order to equip..
        /// </summary>
        Attack = 1,
        /// <summary>
        /// Allows players to wear stronger armour, increases their chances of avoiding hits and reduces damage taken.
        /// </summary>
        Defence = 2,
        /// <summary>
        /// During melee, higher strength increases a player's maximum potential damage to an opponent for each successful hit. Stronger members' weapons may also require higher levels to equip. Strength also gives access to various agility shortcuts.
        /// </summary>
        Strength = 3,
        /// <summary>
        /// Allows players to sustain more damage without dying. A players base life points are one hundred times their constitution level
        /// </summary>
        Constitution = 4,
        /// <summary>
        /// Allows players to fight with arrows and other projectiles from a distance and increases a player's chance to hit when using ranged. Ranged weapons, armour and some other items require a certain ranged level.
        /// </summary>
        Ranged = 5,
        /// <summary>
        /// Allows players to pray for assistance in combat, such as for stat boosts and partial immunity from attacks. Effects last until the player runs out of prayer points, or turns their prayers off. Can also be used to repair or bless gravestones.
        /// </summary>
        Prayer = 6,
        /// <summary>
        /// Allows players to cast spells, including teleports and enchantments, through the use of different types of runes. Increases magic-based attack accuracy and reduces the chance for magic-based attacks to hit the player.
        /// </summary>
        Magic = 7,
        /// <summary>
        /// Allows players to cook food. The food can then be consumed to heal a player's life points. The higher the cooking level, the lesser the chance is of burning food.
        /// </summary>
        Cooking = 8,
        /// <summary>
        /// Allows players to cut down trees for logs, and to carve out canoes for transportation.
        /// </summary>
        Woodcutting = 9,
        /// <summary>
        /// Allows players to create projectiles (such as arrows, bolts, and darts) and bows/crossbows which can be used for Ranged.
        /// </summary>
        Fletching = 10,
        /// <summary>
        /// Allows players to catch certain fish. The fish can then be sold or cooked and then eaten.
        /// </summary>
        Fishing = 11,
        /// <summary>
        /// Allows players to light fires, lanterns, etc. Players can cook on these fires.
        /// </summary>
        Firemaking = 12,
        /// <summary>
        /// Allows players to craft items from raw materials, such as pottery, ranged armour, and jewellery.
        /// </summary>
        Crafting = 13,
        /// <summary>
        /// Allows players to smelt ores into bars, and smith bars into armour, weapons, and other useful items.
        /// </summary>
        Smithing = 14,
        /// <summary>
        /// Allows players to obtain ores and gems from rocks found in some specific places. Ores can be used with the smithing skill. Gems can be used with the crafting skill.
        /// </summary>
        Mining = 15,
        /// <summary>
        /// Allows players to clean grimy herbs and to make potions which can be used to boost/restore skills.
        /// </summary>
        Herblore = 16,
        /// <summary>
        /// Allows players to use shortcuts and increases the rate at which energy recharges. Certain pieces of equipment such as the Crystal bow also require certain agility levels to wield. It is also used to enhance several other skills, such as allowing the player to catch multiple fish at once.
        /// </summary>
        Agility = 17,
        /// <summary>
        /// Allows players to steal from market stalls, chests and certain non-player characters (and other players in Stealing Creation) and to lockpick doors.
        /// </summary>
        Thieving = 18,
        /// <summary>
        /// Allows players to kill certain monsters using tactics not used in normal combat.
        /// </summary>
        Slayer = 19,
        /// <summary>
        /// Allows players to grow plants (such as fruits, vegetables, herbs, or trees) in certain patches across the world of RuneScape. 
        /// </summary>
        Farming = 20,
        /// <summary>
        /// Allows players to make runes in special altars using tiaras, talisman staffs, or talismans, aiding in the magic skill
        /// </summary>
        Runecrafting = 21,
        /// <summary>
        /// Allows players to track, net, deadfall, snare, and trap animals for their hides, abilities and treasures. Some of the animals caught can be used in other skills.
        /// </summary>
        Hunter = 22,
        /// <summary>
        /// Allows players to build a house and its contents, such as chairs, tables, workshops, dungeons, and more. Every player's house is located inside its own instance, separated from the rest of the game.
        /// </summary>
        Construction = 23,
        /// <summary>
        /// Allows players to summon familiars and pets to enhance game play in both combat and non-combat skills.
        /// </summary>
        Summoning = 24,
        /// <summary>
        /// Allows players to progress further down the dungeons of Daemonheim, unlocking access to exclusive weapons, treasures, monsters, and areas.
        /// </summary>
        Dungeoneering = 25,
        /// <summary>
        /// Allows players to collect divine energy around the world in order to create products such as signs and portents.
        /// </summary>
        Divination = 26,
        /// <summary>
        /// Allows players to disassemble many in game items in order to break them down into components used in creating new contraptions. 
        /// </summary>
        Invention = 27,
        /// <summary>
        /// Allows players to collection and repair broken down artifacts. Unlocks the ability to activate relics granting several passive effects.
        /// </summary>
        Archaeology = 28,
    }
}