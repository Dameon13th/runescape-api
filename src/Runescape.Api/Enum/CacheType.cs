﻿namespace Runescape.Api
{
    internal enum CacheType
    {
        None,
        Permanent,
        Timed,
    }
}