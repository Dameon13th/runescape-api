﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing a value for all activities in which a hi score exist.
    /// </summary>
    public enum Activity
    {
        /// <summary>
        /// Bounty Hunters
        /// </summary>
        BountyHunters,
        /// <summary>
        /// Bounty Hunters - Rogues.
        /// </summary>
        BountyHunterRogues,
        /// <summary>
        /// Dominion Tower.
        /// </summary>
        DominionTower,
        /// <summary>
        /// The Crucible.
        /// </summary>
        TheCrucible,
        /// <summary>
        /// Castle Wars Games.
        /// </summary>
        CastleWarsGames,
        /// <summary>
        /// Barbarian Assault - Attacker.
        /// </summary>
        BarbarianAssaultAttackers,
        /// <summary>
        /// Barbarian Assault - Defenders.
        /// </summary>
        BarbarianAssaultDefenders,
        /// <summary>
        /// Barbarian Assault - Collectors.
        /// </summary>
        BarbarianAssaultCollectors,
        /// <summary>
        /// Barbarian Assault - Healers.
        /// </summary>
        BarbarianAssaultHealers,
        /// <summary>
        /// Duel Tournament.
        /// </summary>
        DuelTournament,
        /// <summary>
        /// Mobilising Armies/
        /// </summary>
        MobilisingArmies,
        /// <summary>
        /// Conquest.
        /// </summary>
        Conquest,
        /// <summary>
        /// Fist of Guthix.
        /// </summary>
        FistOfGuthix,
        /// <summary>
        /// Gielinor Games - Resource Race.
        /// </summary>
        GielinorGamesResourceRace,
        /// <summary>
        /// Gielinor Games - Athletics.
        /// </summary>
        GielinorGamesAthletics,
        /// <summary>
        /// World Event 2 - Armadyl Lifetime Contribution.
        /// </summary>
        WorldEvent2ArmadylLifetimeContribution,
        /// <summary>
        /// World Event 2 - Bandos Lifetime Contribution
        /// </summary>
        WorldEvent2BandosLifetimeContribution,
        /// <summary>
        /// World Event 2 - Armadyl player vs player kills.
        /// </summary>
        WorldEvent2ArmadylPvpKills,
        /// <summary>
        /// World Event 2 - Bandos player vs player kills.
        /// </summary>
        WorldEvent2BandosPvpKills,
        /// <summary>
        /// Heist - Guard level.
        /// </summary>
        HeistGuardLevel,
        /// <summary>
        /// Heist - Robber level
        /// </summary>
        HeistRobberLevel,
        /// <summary>
        /// Cabbage Face Punch Bonanza game average.
        /// </summary>
        CabbageFacepunchBonanzaGameAverage,
        /// <summary>
        /// April Fools - Cow Tipping.
        /// </summary>
        AprilFoolsCowTipping,
        /// <summary>
        /// April Fools - Rats killed after miniquest.
        /// </summary>
        AprilFoolsRatsKilledAfterMiniquest,
        /// <summary>
        /// Runescore.
        /// </summary>
        Runescore,
        /// <summary>
        /// Clue Scrolls - Easy.
        /// </summary>
        ClueScrollsEasy,
        /// <summary>
        /// Clue Scrolls - Medium.
        /// </summary>
        ClueScrollsMedium,
        /// <summary>
        /// Clue Scrolls - Hard.
        /// </summary>
        ClueScrollsHard,
        /// <summary>
        /// Clue Scrolls - Elite.
        /// </summary>
        ClueScrollsElite,
        /// <summary>
        /// Clue Scrolls - Master.
        /// </summary>
        ClueScrollsMaster,
    }
}