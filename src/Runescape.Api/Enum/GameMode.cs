﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing values for the different type of game play.
    /// </summary>
    public enum GameMode
    {
        /// <summary>
        /// Normal game play mode.
        /// </summary>
        Normal,
        /// <summary>
        /// Game play mode in which the player cannot trade, participate in multi-player, or get experience handouts.
        /// </summary>
        Ironman,
        /// <summary>
        /// Same of <see cref="Ironman"/> except with the added restriction of only being able to die once.
        /// </summary>
        Hardcore
    }
}