﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing values for the different type of game play for Old School Runescape.
    /// </summary>
    public enum OldSchoolGameMode
    {
        /// <summary>
        /// Normal game play mode.
        /// </summary>
        Normal,
        /// <summary>
        /// Game play mode in which the player cannot trade, participate in multi-player, or get experience handouts.
        /// </summary>
        IronMan,
        /// <summary>
        /// Same of <see cref="IronMan"/> except with the added restriction of dying will cause the user to be downgraded into a normal Iron Man.
        /// </summary>
        HardCore,
        /// <summary>
        /// Same of <see cref="IronMan"/> except several quality of life game mechanics are disabled. Such as being able to note items at the bank.
        /// </summary>
        Ultimate,
        /// <summary>
        /// Game play mode in which player vs player combat is available outside of the Wilderness.
        /// </summary>
        Deadman
    }
}