﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing a value different categories used to catagorize the items in the Grande Exchange.
    /// </summary>
    public enum GrandExchangeCategory
    {
        /// <summary>
        /// Category to miscellaneous items.
        /// </summary>
        Miscellaneous = 0,
        /// <summary>
        /// Category for ammunition.
        /// </summary>
        Ammo = 1,
        /// <summary>
        /// Category for arrows
        /// </summary>
        Arrows = 2,
        /// <summary>
        /// Category for bolts
        /// </summary>
        Bolts = 3,
        /// <summary>
        /// Category for construction materials
        /// </summary>
        ConstructionMaterials = 4,
        /// <summary>
        /// Category for construction projects.
        /// </summary>
        ConstructionProjects = 5,
        /// <summary>
        /// Category for cooking ingredients.
        /// </summary>
        CookingIngredients = 6,
        /// <summary>
        /// Category for costumes.
        /// </summary>
        Costumes = 7,
        /// <summary>
        /// Category for crafting materials.
        /// </summary>
        CraftingMaterials = 8,
        /// <summary>
        /// Category for summoning familars.
        /// </summary>
        Familiars = 9,
        /// <summary>
        /// Category for farming produce.
        /// </summary>
        FarmingProduce = 10,
        /// <summary>
        /// Category for fletching materials.
        /// </summary>
        FletchingMaterials = 11,
        /// <summary>
        /// Category for food and drink.
        /// </summary>
        FoodDrink = 12,
        /// <summary>
        /// Category for herblore materials.
        /// </summary>
        HerbloreMaterials = 13,
        /// <summary>
        /// Category for hunting equipment.
        /// </summary>
        HuntingEquipment = 14,
        /// <summary>
        /// Category for hunting produce.
        /// </summary>
        HuntingProduce = 15,
        /// <summary>
        /// Category for jewelry.
        /// </summary>
        Jewellery = 16,
        /// <summary>
        /// Category for mage armour.
        /// </summary>
        MageArmour = 17,
        /// <summary>
        /// Category for mage weapons.
        /// </summary>
        MageWeapons = 18,
        /// <summary>
        /// Category for low level melee armour.
        /// </summary>
        MeleeArmourLowLevel = 19,
        /// <summary>
        /// Category for medium level melee armour.
        /// </summary>
        MeleeArmourMidLevel = 20,
        /// <summary>
        /// Category for high level melee armour.
        /// </summary>
        MeleeArmourHighLevel = 21,
        /// <summary>
        /// Category for low level melee weapons.
        /// </summary>
        MeleeWeaponsLowLevel = 22,
        /// <summary>
        /// Category for medium level melee weapons.
        /// </summary>
        MeleeWeaponsMidLevel = 23,
        /// <summary>
        /// Category for high level melee weapons.
        /// </summary>
        MeleeWeaponsHighLevel = 24,
        /// <summary>
        /// Category for mining and smithing.
        /// </summary>
        MiningSmithing = 25,
        /// <summary>
        /// Category for potions.
        /// </summary>
        Potions = 26,
        /// <summary>
        /// Category for prayer armour.
        /// </summary>
        PrayerArmour = 27,
        /// <summary>
        /// Category for prayer materials.
        /// </summary>
        PrayerMaterials = 28,
        /// <summary>
        /// Category for range armour.
        /// </summary>
        RangeArmour = 29,
        /// <summary>
        /// Category for range wepons.
        /// </summary>
        RangeWeapons = 30,
        /// <summary>
        /// Category for runecrafting.
        /// </summary>
        Runecrafting = 31,
        /// <summary>
        /// Category for runes.
        /// </summary>
        Runes = 32,
        /// <summary>
        /// Category for seeds.
        /// </summary>
        Seeds = 33,
        /// <summary>
        /// Category for summong scrolls.
        /// </summary>
        SummoningScrolls = 34,
        /// <summary>
        /// Category for tool containers.
        /// </summary>
        ToolsContainers = 35,
        /// <summary>
        /// Category for woodcutting product.
        /// </summary>
        WoodcuttingProduct = 36,
        /// <summary>
        /// Category for pocket items.
        /// </summary>
        PocketItems = 37,
        /// <summary>
        /// Category for stone spirits.
        /// </summary>
        StoneSpirits = 38,
        /// <summary>
        /// Category for salvage.
        /// </summary>
        Salvage = 39,
        /// <summary>
        /// Category for firemaking products.
        /// </summary>
        FireMakingProducts = 40,
        /// <summary>
        /// Category for archaeology materials.
        /// </summary>
        ArchaeologyMaterials = 41
    }
}