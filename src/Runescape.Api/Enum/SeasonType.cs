﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing value for the different time of seasons.
    /// </summary>
    public enum SeasonType
    {
        /// <summary>
        /// The seasons that are currently active.
        /// </summary>
        Current,
        /// <summary>
        /// The seasons that are no longer active and took place in the past.
        /// </summary>
        Archived
    }
}