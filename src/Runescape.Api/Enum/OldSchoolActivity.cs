﻿namespace Runescape.Api
{
    /// <summary>
    /// 
    /// </summary>
    public enum OldSchoolActivity
    {
        /// <summary>
        /// 
        /// </summary>
        LeaguePoints,
        /// <summary>
        /// 
        /// </summary>
        BountyHunterHunter,
        /// <summary>
        /// 
        /// </summary>
        BountyHunterRogue,
        /// <summary>
        /// 
        /// </summary>
        ClueScrolls,
        /// <summary>
        /// 
        /// </summary>
        ClueScrollsBeginner,
        /// <summary>
        /// 
        /// </summary>
        ClueScrollsEasy,
        /// <summary>
        /// 
        /// </summary>
        ClueScrollsMedium,
        /// <summary>
        /// 
        /// </summary>
        ClueScrollsHard,
        /// <summary>
        /// 
        /// </summary>
        ClueScrollsElite,
        /// <summary>
        /// 
        /// </summary>
        ClueScrollsMaster,
        /// <summary>
        /// 
        /// </summary>
        LMS_Rank,
        /// <summary>
        /// 
        /// </summary>
        SoulWarsZeal,
        /// <summary>
        /// 
        /// </summary>
        AbyssalSire,
        /// <summary>
        /// 
        /// </summary>
        AlchemicalHydra,
        /// <summary>
        /// 
        /// </summary>
        BarrowsChests,
        /// <summary>
        /// 
        /// </summary>
        Bryophyta,
        /// <summary>
        /// 
        /// </summary>
        Callisto,
        /// <summary>
        /// 
        /// </summary>
        Cerberus,
        /// <summary>
        /// 
        /// </summary>
        ChambersofXeric,
        /// <summary>
        /// 
        /// </summary>
        ChambersofXericChallengeMode,
        /// <summary>
        /// 
        /// </summary>
        ChaosElemental,
        /// <summary>
        /// 
        /// </summary>
        ChaosFanatic,
        /// <summary>
        /// 
        /// </summary>
        CommanderZilyana,
        /// <summary>
        /// 
        /// </summary>
        CorporealBeast,
        /// <summary>
        /// 
        /// </summary>
        CrazyArchaeologist,
        /// <summary>
        /// 
        /// </summary>
        DagannothPrime,
        /// <summary>
        /// 
        /// </summary>
        DagannothRex,
        /// <summary>
        /// 
        /// </summary>
        DagannothSupreme,
        /// <summary>
        /// 
        /// </summary>
        DerangedArchaeologist,
        /// <summary>
        /// 
        /// </summary>
        GeneralGraardor,
        /// <summary>
        /// 
        /// </summary>
        GiantMole,
        /// <summary>
        /// 
        /// </summary>
        GrotesqueGuardians,
        /// <summary>
        /// 
        /// </summary>
        Hespori,
        /// <summary>
        /// 
        /// </summary>
        KalphiteQueen,
        /// <summary>
        /// 
        /// </summary>
        KingBlackDragon,
        /// <summary>
        /// 
        /// </summary>
        Kraken,
        /// <summary>
        /// 
        /// </summary>
        KreeArra,
        /// <summary>
        /// 
        /// </summary>
        KrilTsutsaroth,
        /// <summary>
        /// 
        /// </summary>
        Mimic,
        /// <summary>
        /// 
        /// </summary>
        Nightmare,
        /// <summary>
        /// 
        /// </summary>
        Obor,
        /// <summary>
        /// 
        /// </summary>
        Sarachnis,
        /// <summary>
        /// 
        /// </summary>
        Scorpia,
        /// <summary>
        /// 
        /// </summary>
        Skotizo,
        /// <summary>
        /// 
        /// </summary>
        Tempoross,
        /// <summary>
        /// 
        /// </summary>
        TheGauntlet,
        /// <summary>
        /// 
        /// </summary>
        TheCorruptedGauntlet,
        /// <summary>
        /// 
        /// </summary>
        TheatreofBlood,
        /// <summary>
        /// 
        /// </summary>
        TheatreofBloodHardMode,
        /// <summary>
        /// 
        /// </summary>
        ThermonuclearSmokeDevil,
        /// <summary>
        /// 
        /// </summary>
        TzKalZuk,
        /// <summary>
        /// 
        /// </summary>
        TzTokJad,
        /// <summary>
        /// 
        /// </summary>
        Venenatis,
        /// <summary>
        /// 
        /// </summary>
        Vetion,
        /// <summary>
        /// 
        /// </summary>
        Vorkath,
        /// <summary>
        /// 
        /// </summary>
        Wintertodt,
        /// <summary>
        /// 
        /// </summary>
        Zalcano,
        /// <summary>
        /// 
        /// </summary>
        Zulrah
    }
}
