﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing values for possible image sizes returned from <see cref="IGrandExchange"/>
    /// </summary>
    public enum ImageSize
    {
        /// <summary>
        /// Standard in game size for bank and inventory.
        /// </summary>
        Sprite,
        /// <summary>
        /// Larger than in game size.
        /// </summary>
        Large
    }
}