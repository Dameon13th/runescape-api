﻿namespace Runescape.Api
{
    /// <summary>
    /// Enum containing values for all of the weaknesses.
    /// </summary>
    /// <remarks>
    /// This enum was generated using the results of the <see cref="IBestiary.GetWeaknessesAsync"/>. As such it may not contain any new weaknesses that were added after generation.
    /// </remarks>
    public enum Weakness
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,
        /// <summary>
        /// Air
        /// </summary>
        Air = 1,
        /// <summary>
        /// Water
        /// </summary>
        Water = 2,
        /// <summary>
        /// Earth
        /// </summary>
        Earth = 3,
        /// <summary>
        /// Fire
        /// </summary>
        Fire = 4,
        /// <summary>
        /// Stabbing
        /// </summary>
        Stabbing = 5,
        /// <summary>
        /// Slashing
        /// </summary>
        Slashing = 6,
        /// <summary>
        /// Crushing
        /// </summary>
        Crushing = 7,
        /// <summary>
        /// Arrow
        /// </summary>
        Arrow = 8,
        /// <summary>
        /// Bolt
        /// </summary>
        Bolt = 9,
        /// <summary>
        /// Thrown
        /// </summary>
        Thrown = 10,
    }
}