﻿using System;
using System.Globalization;
using Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Runescape.Api
{
    internal class PriceConverter : JsonConverter<long>
    {
        #region Overrides of JsonConverter<long>

        /// <summary>Writes the JSON representation of the object.</summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, long value, JsonSerializer serializer)
        {
           writer.WriteValue(value);
        }

        /// <summary>Reads the JSON representation of the object.</summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read. If there is no existing value then <c>null</c> will be used.</param>
        /// <param name="hasExistingValue">The existing value has a value.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override long ReadJson(JsonReader reader, Type objectType, long existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            return StringToDouble(reader.Value.ToString());
        }

        #endregion

        private static long StringToDouble(string rawValue)
        {
            string cleanedString = rawValue.Replace(" ", string.Empty);
            long multiplier = 0;

            char lastChar = cleanedString[cleanedString.Length - 1];
            switch (lastChar)
            {
                case 'k':
                    multiplier = 1000;
                    cleanedString = cleanedString.Substring(0, cleanedString.Length - 1);
                    break;
                case 'm':
                    multiplier = 1000000;
                    cleanedString = cleanedString.Substring(0, cleanedString.Length - 1);
                    break;
                case 'b':
                    multiplier = 1000000000;
                    cleanedString = cleanedString.Substring(0, cleanedString.Length - 1);
                    break;
                default:
                    multiplier = 1;
                    break;
            }
            double value = double.Parse(cleanedString, NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint);
            return Convert.ToInt64(value * multiplier);
        }
    }
}