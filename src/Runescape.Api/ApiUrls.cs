﻿using System;
using System.Text.Encodings.Web;

namespace Runescape.Api
{
    internal class ApiUrls
    {
        public static string BaseUrl = "https://secure.runescape.com/";

        public class GrandExchange
        {
            public const string LAST_UPDATE = "https://secure.runescape.com/m=itemdb_rs/api/info.json";
            public const string CATALOGUE = "https://services.runescape.com/m=itemdb_rs/api/catalogue/category.json?category={0}";
            public const string ITEMS = "https://services.runescape.com/m=itemdb_rs/api/catalogue/items.json?category={0}&alpha={1}&page={2}";
            public const string ITEM_DETAIL = "https://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json?item={0}";
            public const string BIG_IMAGE = "https://services.runescape.com/m=itemdb_rs/obj_big.gif?id={0}";
            public const string SPRITE_IMAGE = "https://services.runescape.com/m=itemdb_rs/obj_sprite.gif?id={0}";

            public static string BuildCatergoryUrl(GrandExchangeCategory category)
            {
                return string.Format(CATALOGUE, (int)category);
            }

            public static string BuildItemsUrl(GrandExchangeCategory category, char firstChar, int page)
            {
                string alphaParam = !char.IsNumber(firstChar)
                    ? new string(firstChar, 1)
                    : "%23";
                return string.Format(ITEMS, (int)category, alphaParam, page);
            }

            public static string BuildItemDetailUrl(long itemId)
            {
                return string.Format(ITEM_DETAIL, itemId);
            }

            public static string BuildItemImageUrl(long id, ImageSize size)
            {
                return size == ImageSize.Sprite ? string.Format(SPRITE_IMAGE, id) : string.Format(BIG_IMAGE, id);
            }
        }

        public class Hiscores
        {
            public const string HISCORE_RANKING = "https://secure.runescape.com/m=hiscore/ranking.json?table={0}&category={1}&size={2}";
            public const string HISCORE_NORMAL = "http://services.runescape.com/m=hiscore/index_lite.ws?player={0}";
            public const string HISCORE_IRONMAN = "http://services.runescape.com/m=hiscore_ironman/index_lite.ws?player={0}";
            public const string HISCORE_HARDCORE = "http://services.runescape.com/m=hiscore_hardcore_ironman/index_lite.ws?player={0}";

            public const string SEASONAL_HISCORE = "http://services.runescape.com/m=temp-hiscores/getRankings.json?player={0}";
            public const string SEASONAL_HISCORE_ARCHIVED = "http://services.runescape.com/m=temp-hiscores/getRankings.json?player={0}&status=archived";

            public const string SEASONS = "http://services.runescape.com/m=temp-hiscores/getHiscoreDetails.json";
            public const string SEASONS_ARCHIVED = "http://services.runescape.com/m=temp-hiscores/getHiscoreDetails.json?status=archived";

            public const string CLAN_RANKINGS = "http://services.runescape.com/m=clan-hiscores/clanRanking.json";
            public const string CLAN_MEMBERS = "http://services.runescape.com/m=clan-hiscores/members_lite.ws?clanName={0}";

            public static string BuildRankingsUrl(Skill skill, int size)
            {
                return string.Format(HISCORE_RANKING, (int)skill, 0, size);
            }

            public static string BuildRankingsUrl(Activity activity, int size)
            {
                return string.Format(HISCORE_RANKING, (int)activity, 1, size);
            }

            public static string BuildHiscoresUrl(GameMode mode, string userName)
            {
                switch (mode)
                {
                    case GameMode.Normal:
                        return string.Format(HISCORE_NORMAL, UrlEncoder.Default.Encode(userName));
                    case GameMode.Ironman:
                        return string.Format(HISCORE_IRONMAN, UrlEncoder.Default.Encode(userName));
                    case GameMode.Hardcore:
                        return string.Format(HISCORE_HARDCORE, UrlEncoder.Default.Encode(userName));
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }

            public static string BuildSeasonalHiscore(string playerName, SeasonType seasonType)
            {
                if (seasonType == SeasonType.Archived)
                {
                    return string.Format(SEASONAL_HISCORE_ARCHIVED, UrlEncoder.Default.Encode(playerName));
                }
                return string.Format(SEASONAL_HISCORE, UrlEncoder.Default.Encode(playerName));
            }

            public static string BuildSeasonUrl(SeasonType seasonType)
            {
                return seasonType == SeasonType.Archived ?
                    SEASONS_ARCHIVED :
                    SEASONS;
            }

            public static string BuildClanMembersUrl(string clanName)
            {
                return string.Format(CLAN_MEMBERS, UrlEncoder.Default.Encode(clanName));
            }
        }

        public class Bestiary
        {
            #region "Private Members"

            private const string BEAST_DATA = "http://services.runescape.com/m=itemdb_rs/bestiary/beastData.json?beastid={0}";
            private const string BEAST_SEARCH = "http://services.runescape.com/m=itemdb_rs/bestiary/beastSearch.json?term={0}";
            private const string BEAST_NAME = "http://services.runescape.com/m=itemdb_rs/bestiary/bestiaryNames.json?letter={0}";
            private const string AREA_NAMES = "http://services.runescape.com/m=itemdb_rs/bestiary/areaNames.json";
            private const string AREA_BEAST = "http://services.runescape.com/m=itemdb_rs/bestiary/areaBeasts.json?identifier={0}";
            private const string SLAYER_CATEGORY = "http://services.runescape.com/m=itemdb_rs/bestiary/slayerCatNames.json";
            private const string SLAYER_BEAST = "http://services.runescape.com/m=itemdb_rs/bestiary/slayerBeasts.json?identifier={0}";
            private const string WEAKNESSES = "http://services.runescape.com/m=itemdb_rs/bestiary/weaknessNames.json";
            private const string WEAKNESS_BEAST = "http://services.runescape.com/m=itemdb_rs/bestiary/weaknessBeasts.json?identifier={0}";
            private const string LEVEL_RANGE_BEAST = "http://services.runescape.com/m=itemdb_rs/bestiary/levelGroup.json?identifier={0}-{1}";

            #endregion

            #region "Public Methods"

            public static string BuildBeastDataUrl(long beastId)
            {
                return string.Format(BEAST_DATA, beastId);
            }

            public static string BuildBeastSearchUrl(params string[] searchTerms)
            {
                string joinedSearchTerms = string.Join("+", searchTerms);
                return string.Format(BEAST_SEARCH, joinedSearchTerms);
            }

            public static string BuildBeastNameUrl(char firstChar)
            {
                return string.Format(BEAST_NAME, char.ToUpper(firstChar));
            }

            public static string BuildAreaNamesUrl()
            {
                return AREA_NAMES;
            }

            public static string BuildAreaBeastUrl(string areaName)
            {
                return string.Format(AREA_BEAST, UrlEncoder.Default.Encode(areaName));
            }

            public static string BuildSlayerCategoriesUrl()
            {
                return SLAYER_CATEGORY;
            }

            public static string BuildSlayerBeastUrl(int slayerCategoryId)
            {
                return string.Format(SLAYER_BEAST, slayerCategoryId);
            }

            public static string BuildWeaknessUrl()
            {
                return WEAKNESSES;
            }

            public static string BuildWeaknessBeastUrl(int weaknessId)
            {
                return string.Format(WEAKNESS_BEAST, weaknessId);
            }

            public static string BuildLevelRangeUrl(int low, int high)
            {
                return string.Format(LEVEL_RANGE_BEAST, low, high);
            }

            #endregion
        }

        public class OldSchoolHiscores
        {
            private const string HISCORE_NORMAL = "https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player={0}";
            private const string HISCORE_IRONMAN = "https://secure.runescape.com/m=hiscore_oldschool_ironman/index_lite.ws?player={0}";
            private const string HISCORE_HARDCORE = "https://secure.runescape.com/m=hiscore_oldschool_hardcore_ironman/index_lite.ws?player={0}";
            private const string HISCORE_ULTIMATE = "https://secure.runescape.com/m=hiscore_oldschool_ultimate/index_lite.ws?player={0}";
            private const string HISCORE_DEADMAN = "https://secure.runescape.com/m=hiscore_oldschool_deadman/index_lite.ws?player={0}";


            public static string BuildHiscoresUrl(OldSchoolGameMode mode, string userName)
            {
                switch (mode)
                {
                    case OldSchoolGameMode.Normal:
                        return string.Format(HISCORE_NORMAL, UrlEncoder.Default.Encode(userName));
                    case OldSchoolGameMode.IronMan:
                        return string.Format(HISCORE_IRONMAN, UrlEncoder.Default.Encode(userName));
                    case OldSchoolGameMode.HardCore:
                        return string.Format(HISCORE_HARDCORE, UrlEncoder.Default.Encode(userName));
                    case OldSchoolGameMode.Ultimate:
                        return string.Format(HISCORE_ULTIMATE, UrlEncoder.Default.Encode(userName));
                    case OldSchoolGameMode.Deadman:
                        return string.Format(HISCORE_DEADMAN, UrlEncoder.Default.Encode(userName));
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }
        }

        public class SolomonStore
        {
            public static string GetUrl()
            {
                return "https://secure.runescape.com/m=mtxn_rs_shop/api/config?context%5B0%5D=0";
            }
        }

        public class WebsiteData
        {
            public static string PlayerDetails(string userName)
            {
                return $"https://secure.runescape.com/m=website-data/playerDetails.ws?names=[\"{userName}\"]&callback=jQuery000000000000000_0000000000&_=0";
            }
            
            public static string PlayerFriendDetails(string userName)
            {
                // This end point requires the user to be logged in
                return $"http://services.runescape.com/c=0/m=website-data/playerFriendsDetails.json?resultsPerPage=24&currentPage=1&callback=jQuery000000000000000_0000000000&_=0";
            }

            public static string AvatarDetails(string userName)
            {
                return $"http://services.runescape.com/m=avatar-rs/{userName}/appearance.dat";
            }

            public static string Foo(string appearanceData)
            {
                return $"http://services.runescape.com/m=adventurers-log/avatardetails.json?details={0}";
            }

            public static string AvatarPicture(string userName)
            {
                return $"http://secure.runescape.com/m=avatar-rs/{0}/chat.png";
            }
        }
    }
}