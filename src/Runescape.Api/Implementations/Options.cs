﻿using Microsoft.Extensions.Options;

namespace Runescape.Api
{
    /// <summary>
    /// Internal implementation of <see cref="IOptions{TOptions}"/> for the <see cref="RunescapeApiOptions"/>.
    /// So that if a consumer is not using .NET core dependency injection they can still get
    /// a working instance of any of the APIs that require an instance of IOptions&lt;RunescapeApiOptions&gt;.
    /// </summary>
    internal class Options : IOptions<RunescapeApiOptions>
    {
        #region "Constructor"

        public Options(CacheOptions cacheOptions)
        {
            Value = new RunescapeApiOptions
            {
                CacheOptions = cacheOptions
            };
        }

        #endregion

        #region Implementation of IOptions<out RunescapeApiOptions>

        public RunescapeApiOptions Value { get; }

        #endregion
    }
}