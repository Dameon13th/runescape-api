﻿using System.Collections.Generic;
using System.Net.Http;

namespace Runescape.Api
{
    /// <summary>
    /// Internal implementation of <see cref="IHttpClientFactory"/> so that if a consumer is not using .NET core dependency injection they can still get
    /// a working instance of any of the APIs that require an IHttpClientFactory.
    /// </summary>
    internal class HttpClientFactory : IHttpClientFactory
    {
        #region "Private Members"

        private static readonly Dictionary<string, HttpMessageHandler> _messageHandlers;
        private static readonly object _lock;

        #endregion

        #region "Constructor"

        static HttpClientFactory()
        {
            _messageHandlers = new Dictionary<string, HttpMessageHandler>();
            _lock = new object();
        }

        #endregion

        #region Implementation of IHttpClientFactory

        public HttpClient CreateClient(string name)
        {
            lock (_lock)
            {
                if (string.IsNullOrEmpty(name))
                {
                    return new HttpClient();
                }

                if (_messageHandlers.TryGetValue(name, out HttpMessageHandler messageHandler))
                {
                    return new HttpClient(messageHandler);
                }
                messageHandler = new HttpClientHandler();
                _messageHandlers[name] = messageHandler;
                return new HttpClient(messageHandler);
            }
        }

        #endregion
    }
}