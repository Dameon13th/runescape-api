﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for a video media item.
    /// </summary>
    public interface IMediaVideo : IMedia
    {
        /// <summary>
        /// Gets the video link to display the MP4 of an item  in the store.
        /// </summary>
        string VideoUrl { get; }
    }
}