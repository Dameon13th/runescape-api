﻿using System;
using System.Collections.Generic;

namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for a response from the Solomon store API endpoint.
    /// </summary>
    public interface ISolomonResponse
    {
        /// <summary>
        /// Gets the data that the Solomon store was last updated.
        /// </summary>
        DateTime LastUpdated { get; }

        /// <summary>
        /// Gets the list of links that the store uses.
        /// </summary>
        IUrls Urls { get; }

        /// <summary>
        /// Gets a collection of string similar to MediaWiki's i18n pages.
        /// </summary>
        IReadOnlyDictionary<string, string> PhraseBook { get; }

        /// <summary>
        /// Gets the list of objects that appear in the store.
        /// </summary>
        IReadOnlyDictionary<long, IMedia> Media { get; }
    }
}