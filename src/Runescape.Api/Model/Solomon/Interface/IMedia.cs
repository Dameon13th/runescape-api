﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Base level interface that defines the common properties for all Solomon store media types.
    /// </summary>
    public interface IMedia
    {
        /// <summary>
        /// Gets the string value that represents the type of media.
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets the image link to display the icon or gallery image.
        /// </summary>
        string ImageUrl { get; }
    }
}