﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for the URL collection used by the Solomon store.
    /// </summary>
    public interface IUrls
    {
        /// <summary>
        /// Gets the URL to the frequently asked questions page.
        /// </summary>
        string Faq { get; set; }

        /// <summary>
        /// Gets the URL to the terms and conditions.
        /// </summary>
        string Terms { get; set; }

        /// <summary>
        /// Gets the URL to Jagex's privacy policy.
        /// </summary>
        string Privacy { get; set; }

        /// <summary>
        /// Gets the URL to the help page.
        /// </summary>
        string Help { get; set; }

        /// <summary>
        /// Gets the URL to the loyalty program page.
        /// </summary>
        string Loyalty { get; set; }

        /// <summary>
        /// Gets the URL to the membership benefits page.
        /// </summary>
        string Benefits { get; set; }

        /// <summary>
        /// Gets the URL to the guide for the Solomon store.
        /// </summary>
        string Guide { get; set; }

        /// <summary>
        /// Gets the URL to the offer wall.
        /// </summary>
        string OfferWall { get; set; }

        /// <summary>
        /// Gets the URL to the Runescape home page.
        /// </summary>
        string HomePage { get; set; }
    }
}