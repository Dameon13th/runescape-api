﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for an Image media item
    /// </summary>
    public interface IMediaImage : IMedia
    {

    }
}