﻿using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    internal class MediaWrapper
    {
        #region "Public Properties"

        [JsonProperty("0")]
        public Media Media { get; set; }

        #endregion
    }
}