﻿namespace Runescape.Api.Model
{
    internal class MediaImage : IMediaImage
    {
        #region "Implementation of IMediaImage"

        public string Type { get; internal set; }

        public string ImageUrl { get; internal set; }

        #endregion
    }
}