﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    internal class SolomonResponseRaw
    {
        #region "Public Properties"

        [JsonProperty("_type")]
        public string Type { get; set; }

        [JsonProperty("version")]
        public long Version { get; set; }

        public DateTime LastUpdated
        {
            get
            {
                DateTime dateTime = new DateTime(1970, 1, 1);
                return dateTime.AddMilliseconds(Version);
            }
        }

        [JsonProperty("url")]
        public Urls Urls { get; set; }

        [JsonProperty("phrasebook")]
        public Dictionary<string, string> PhraseBook { get; set; }

        [JsonProperty("media")]
        public Dictionary<string, MediaWrapper> Media { get; set; }

        #endregion
    }
}
