﻿using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    internal class Urls : IUrls
    {
        #region "Public Properties"

        [JsonProperty("shop:faq")]
        public string Faq { get; set; }

        [JsonProperty("shop:terms")]
        public string Terms { get; set; }

        [JsonProperty("shop:privacy")]
        public string Privacy { get; set; }

        [JsonProperty("shop:help")]
        public string Help { get; set; }

        [JsonProperty("balance:loyaltyPage")]
        public string Loyalty { get; set; }

        [JsonProperty("balance:membersBenefits")]
        public string Benefits { get; set; }

        [JsonProperty("shop:guide")]
        public string Guide { get; set; }

        [JsonProperty("supersonic:offerwall")]
        public string OfferWall { get; set; }

        [JsonProperty("common:homepage")]
        public string HomePage { get; set; }

        #endregion
    }
}