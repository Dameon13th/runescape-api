﻿using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    internal class Media
    {
        #region "Public Properties"

        [JsonProperty("_type")]
        public string Type { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("videoUrl")]
        public string VideoUrl { get; set; }

        [JsonProperty("posterUrl")]
        public string PosterUrl { get; set; }

        #endregion
    }
}