﻿namespace Runescape.Api.Model
{
    internal class MediaVideo : MediaImage, IMediaVideo
    {
        #region "Implementation of IMediaVideo"

        public string VideoUrl { get; internal set; }

        #endregion
    }
}