﻿using System;
using System.Collections.Generic;

namespace Runescape.Api.Model
{
    internal class SolomonResponse : ISolomonResponse
    {
        #region "Constructor"

        public SolomonResponse()
        {
            Media = new SortedDictionary<long, IMedia>();
        }

        #endregion

        #region "Public Properties"

        public SortedDictionary<long, IMedia> Media { get; internal set; }

        #endregion

        #region "Implementation of ISolomonResponse"

        public string Type { get; internal set; }

        public DateTime LastUpdated { get; internal set; }

        public IUrls Urls { get; internal set; }

        public IReadOnlyDictionary<string, string> PhraseBook { get; internal set; }
        
        IReadOnlyDictionary<long, IMedia> ISolomonResponse.Media => Media;

        #endregion
    }
}