﻿using System.IO;

namespace Runescape.Api.Model
{
    internal class Image : IImage
    {
        #region Implementation of IImage

        public Stream Stream { get; set; }

        public string Extension { get; set; }

        public long Length { get; set; }

        #endregion
    }
}