﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of an item found in the Grand Exchange.
    /// </summary>
    public interface IItem
    {
        /// <summary>
        /// Gets the Id of the item.
        /// </summary>
        long Id { get; }

        /// <summary>
        /// Gets the URL of the items icon image.
        /// </summary>
        string IconUrl { get; }

        /// <summary>
        /// Gets the URL of the items large image.
        /// </summary>
        string LargeIconUrl { get; }

        /// <summary>
        /// Gets the item's type
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets the URL of the type's image.
        /// </summary>
        string TypeIconUrl { get; }

        /// <summary>
        /// Gets the name of the item.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets current price of the item.
        /// </summary>
        IPrice Current { get; }

        /// <summary>
        /// Gets the changes of the price for today.
        /// </summary>
        IPrice Today { get; }

        /// <summary>
        /// Gets a value the determines if this is a member's only item.
        /// </summary>
        bool IsMembers { get; }
    }
}