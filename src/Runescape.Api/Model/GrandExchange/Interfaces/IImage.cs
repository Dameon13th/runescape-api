﻿using System.IO;

namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of an image.
    /// </summary>
    public interface IImage
    {
        /// <summary>
        /// Gets the stream that contains the image byte data.
        /// </summary>
        Stream Stream { get; }

        /// <summary>
        /// Gets the extension of the image.
        /// </summary>
        string Extension { get; }

        /// <summary>
        /// Gets the length of bytes in the image.
        /// </summary>
        long Length { get; }
    }
}