﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of an item's Grand Exchange details.
    /// </summary>
    public interface IItemDetail : IItem
    {
        /// <summary>
        /// Gets the price change for the last 30 days.
        /// </summary>
        IPriceChange Day30 { get; }

        /// <summary>
        /// Gets the price change for the last 90 days.
        /// </summary>
        IPriceChange Day90 { get; }

        /// <summary>
        /// Gets the price change for the last 180 days.
        /// </summary>
        IPriceChange Day180 { get; }
    }
}