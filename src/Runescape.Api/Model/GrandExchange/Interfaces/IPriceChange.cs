﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a price change.
    /// </summary>
    public interface IPriceChange
    {
        /// <summary>
        /// Gets the trend of the price change. 
        /// </summary>
        string Trend { get; }

        /// <summary>
        /// Gets the actual change.
        /// </summary>
        /// <remarks>
        /// This value is a string due to the fact that based on threshholds this value is either a number or a percentage.
        /// These thresholds are currently unknown.
        /// Example: The value could either be 100 or 5%
        /// </remarks>
        string Change { get; }
    }
}