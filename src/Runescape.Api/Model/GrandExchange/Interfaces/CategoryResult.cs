﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Runescape.Api.Model
{
   internal class CategoryResult : ICategoryResult
    {
        #region "Public Properties"

        public Dictionary<string, int> CountByLetter { get; set; }

        #endregion

        #region "Constructor"
        
        public CategoryResult(GrandExchangeCategory category)
        {
            Category = category;
            CountByLetter = new Dictionary<string, int>(27);
        }

        #endregion

        #region "Implementation of ICategoryResult"

        public GrandExchangeCategory Category { get; }

        IReadOnlyDictionary<string, int> ICategoryResult.CountByLetter
        {
            get { return CountByLetter; }
        }

        #endregion
    }
}