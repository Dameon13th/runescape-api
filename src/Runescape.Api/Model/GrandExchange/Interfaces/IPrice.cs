﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a price.
    /// </summary>
    public interface IPrice
    {
        /// <summary>
        /// Get the numerical value of the price.
        /// </summary>
        long Value { get; }

        /// <summary>
        /// Gets the trend of the price.
        /// </summary>
        string Trend { get; }
    }
}