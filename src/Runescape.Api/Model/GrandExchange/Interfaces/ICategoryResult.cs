﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a response for getting the items of a category.
    /// </summary>
    public interface ICategoryResult
    {
        /// <summary>
        /// Get the category used to get the results/
        /// </summary>
        GrandExchangeCategory Category { get; }

        /// <summary>
        /// Gets a read only dictionary where the key is a letter of the alphabet.
        /// The value is the count of items whose name starts with that letter of the alphabet.
        /// </summary>
        IReadOnlyDictionary<string,int> CountByLetter { get; }
    }
}