﻿using System.Diagnostics;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DataContract]
    [DebuggerDisplay("{Letter}:{ItemCount}")]
    internal class CountByLetter
    {
        #region Implementation of ICategoryResult

        [JsonProperty("letter")]
        public string Letter { get; set; }

        [JsonProperty("items")]
        public int ItemCount { get; set; }

        #endregion
    }
}