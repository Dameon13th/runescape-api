﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Runescape.Api.Model
{
    [DataContract]
    internal class ItemDetail : Item, IItemDetail
    {
        [JsonProperty("day30")]
        public PriceChange Day30 { get; set; }

        [JsonProperty("day90")]
        public PriceChange Day90 { get; set; }

        [JsonProperty("day180")]
        public PriceChange Day180 { get; set; }

        IPriceChange IItemDetail.Day30
        {
            get { return Day30; }
        }

        IPriceChange IItemDetail.Day90
        {
            get { return Day90; }
        }

        IPriceChange IItemDetail.Day180
        {
            get { return Day180; }
        }
    }
}