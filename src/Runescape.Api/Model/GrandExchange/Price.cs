﻿using System.Diagnostics;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DataContract]
    [DebuggerDisplay("{Trend}: {Value}")]
    internal class Price : IPrice
    {
        [JsonProperty("price")]
        [JsonConverter(typeof(PriceConverter))]
        public long Value { get; set; }

        [JsonProperty("trend")]
        public string Trend { get; set; }
    }

}