﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Runescape.Api.Model
{
    [DataContract]
    [DebuggerDisplay("{Name} ({Id})")]
    internal class Item : IItem
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("icon")]
        public string IconUrl { get; set; }

        [JsonProperty("icon_large")]
        public string LargeIconUrl { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("typeIcon")]
        public string TypeIconUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("current")]
        public Price Current { get; set; }

        [JsonProperty("today")]
        public Price Today { get; set; }

        private string _rawIsMembers;
        private bool _isMembers;

        [JsonProperty("members")]
        public string RawIsMembers
        {
            get { return _rawIsMembers; }
            set
            {
                _rawIsMembers = value;
                _isMembers = bool.Parse(_rawIsMembers);
            }
        }

        IPrice IItem.Current
        {
            get { return Current; }
        }

        IPrice IItem.Today
        {
            get { return Today; }
        }

        bool IItem.IsMembers
        {
            get { return _isMembers; }
        }
    }
}