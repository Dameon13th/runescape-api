﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DataContract]
    internal class PriceChange : IPriceChange
    {
        [JsonProperty("trend")]
        public string Trend { get; set; }

        [JsonProperty("change")]
        public string Change { get; set; }
    }
}