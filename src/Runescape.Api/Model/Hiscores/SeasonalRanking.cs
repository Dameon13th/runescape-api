﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Title} ({StartDate}-{EndDate})")]
    internal class SeasonalRanking : ISeasonalRanking
    {
        #region Implementation of ISeasonalRanking

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("rank")]
        public int Rank { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("score_formatted")]
        public string FormattedScore { get; set; }

        [JsonProperty("score_raw")]
        public int RawScore { get; set; }

        [JsonProperty("hiscoreId")]
        public long HiscoreId { get; set; }

        #endregion
    }
}