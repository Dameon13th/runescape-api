﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    internal class PlayerHiscores : PlayerHiscoresBase<Skill, Activity>, IPlayerHiscores
    {
        #region "Constructor"

        public PlayerHiscores(string playerName)
        {
            PlayerName = playerName;
        }

        #endregion

        #region Implementation of IPlayerHiscores

        public GameMode Mode { get; set; }

        IReadOnlyDictionary<Skill, IHiscore> IPlayerHiscores.Skills => Skills;

        IReadOnlyDictionary<Activity, IHiscore> IPlayerHiscores.Activities => Activities;

        #endregion
    }
}