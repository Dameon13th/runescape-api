﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Name}-({Rank})-{Score}")]
    internal class Ranking : IRanking
    {
        #region Implementation of IRank

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("score")]
        [JsonConverter(typeof(PriceConverter))]
        public long Score { get; set; }

        [JsonProperty("rank")]
        [JsonConverter(typeof(PriceConverter))]
        public long Rank { get; set; }

        #endregion
    }
}