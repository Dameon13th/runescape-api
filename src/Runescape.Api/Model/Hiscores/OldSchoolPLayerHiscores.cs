﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    internal class OldSchoolPLayerHiscores : PlayerHiscoresBase<Skill, OldSchoolActivity>, IOldSchoolPlayerHiscores
    {
        #region "Constructor"

        public OldSchoolPLayerHiscores(string playerName)
        {
            PlayerName = playerName;
        }

        #endregion

        #region "Implementation of IOldSchoolPlayerHiscores"

        public OldSchoolGameMode Mode { get; set; }

        IReadOnlyDictionary<Skill, IHiscore> IOldSchoolPlayerHiscores.Skills => Skills;

        IReadOnlyDictionary<OldSchoolActivity, IHiscore> IOldSchoolPlayerHiscores.Activities => Activities;

        #endregion
    }
}