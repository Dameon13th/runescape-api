﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Title} ({StartDate}-{EndDate})")]
    internal class Season : ISeason
    {
        #region Implementation of ISeasonal

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("daysRunning")]
        public int DaysRunning { get; set; }

        [JsonProperty("monthsRunning")]
        public int MonthsRunning { get; set; }

        [JsonProperty("recurrence")]
        public int Recurrence { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        #endregion
    }
}