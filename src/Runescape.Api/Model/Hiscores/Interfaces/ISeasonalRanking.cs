﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a ranking that belongs to a season.
    /// </summary>
    public interface ISeasonalRanking
    {
        /// <summary>
        /// Gets the start date of the season.
        /// </summary>
        string StartDate { get; }

        /// <summary>
        /// Gets the end date of the season.
        /// </summary>
        string EndDate { get; }

        /// <summary>
        /// Gets the player's rank in this season.
        /// </summary>
        int Rank { get; }

        /// <summary>
        /// Gets the title of the season/
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Gets the formated scored.
        /// </summary>
        string FormattedScore { get; }

        /// <summary>
        /// Gets the player's raw score for this season.
        /// </summary>
        int RawScore { get; }

        /// <summary>
        /// Gets the ranking's Id.
        /// </summary>
        long HiscoreId { get; }
    }
}