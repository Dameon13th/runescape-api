﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the ranking of a user.
    /// </summary>
    public interface IRanking
    {
        /// <summary>
        /// Gets the player name of this ranking.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the score of this ranking.
        /// </summary>
        long Score { get; }

        /// <summary>
        /// Gets the actual ranking number.
        /// </summary>
        long Rank { get; }
    }
}