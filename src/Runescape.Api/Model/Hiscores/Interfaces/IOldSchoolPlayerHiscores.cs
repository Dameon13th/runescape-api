﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for the hi scores of a specific old school player.
    /// </summary>
    public interface IOldSchoolPlayerHiscores
    {
        /// <summary>
        /// Get the mode the hi scores represent.
        /// </summary>
        OldSchoolGameMode Mode { get; }

        /// <summary>
        /// Gets the player's name.
        /// </summary>
        string PlayerName { get; }

        /// <summary>
        /// Gets a collection of any hi scores the player has in skills.
        /// </summary>
        IReadOnlyDictionary<Skill, IHiscore> Skills { get; }

        /// <summary>
        /// Gets a collection of any hi scores the player has in activities. 
        /// </summary>
        IReadOnlyDictionary<OldSchoolActivity, IHiscore> Activities { get; }
    }
}