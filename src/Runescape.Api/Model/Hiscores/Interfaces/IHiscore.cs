﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties for a hi score.
    /// </summary>
    public interface IHiscore
    {
        /// <summary>
        /// Gets the rank of the hi score.
        /// </summary>
        int Rank { get; }

        /// <summary>
        /// Get the level of the <see cref="Skill"/> or <see cref="Activity"/> for the hi score.
        /// </summary>
        int Level { get; }

        /// <summary>
        /// Get the experience of the <see cref="Skill"/> or <see cref="Activity"/> for the hi score.
        /// </summary>
        long Experience { get; }
    }
}