﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a clan member.
    /// </summary>
    public interface IClanMember
    {
        /// <summary>
        /// Gets the clan member's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the clan member's clan rank.
        /// </summary>
        string Rank { get; }

        /// <summary>
        /// Gets the clan member's total experience.
        /// </summary>
        long TotalExperience { get; }

        /// <summary>
        /// Gets the clan member's total kills.
        /// </summary>
        int Kills { get; }
    }
}