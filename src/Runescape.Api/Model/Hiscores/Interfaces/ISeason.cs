﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a season in which hi scores were tracked.
    /// </summary>
    public interface ISeason
    {
        /// <summary>
        /// Gets the season start date.
        /// </summary>
        string StartDate { get; }

        /// <summary>
        /// Sets the season end date.
        /// </summary>
        string EndDate { get; }

        /// <summary>
        /// Gets the number of days the season is on for
        /// </summary>
        int DaysRunning { get; }

        /// <summary>
        /// Gets the number of months the season is on for.
        /// </summary>
        int MonthsRunning { get; }

        /// <summary>
        /// Gets the number of times the season has ran for.
        /// </summary>
        int Recurrence { get; }

        /// <summary>
        /// Gets the title of the season.
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Gets the literal name of the season
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the description of the season.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the current status of the season
        /// </summary>
        string Status { get; }

        /// <summary>
        /// Gets the length of the season.
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets the id of the season.
        /// </summary>
        long Id { get; }

    }
}