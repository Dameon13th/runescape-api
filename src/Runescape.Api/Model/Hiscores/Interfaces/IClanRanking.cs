﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a clan's ranking compared to another clan.
    /// </summary>
    public interface IClanRanking
    {
        /// <summary>
        /// Gets the clan's rank.
        /// </summary>
        int Rank { get; }

        /// <summary>
        /// Gets the clan's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Get the number of members in the clan.
        /// </summary>
        int ClanMates { get; }

        /// <summary>
        /// Get the total experience for the clan.
        /// </summary>
        long TotalExperience { get; }
    }
}