﻿using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    internal class Hiscore : IHiscore
    {
        #region "Public Properties"

        [JsonProperty("rank")]
        public int Rank { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("experience")]
        public long Experience { get; set; }

        #endregion

        #region "Constructor"

        public Hiscore(string line)
        {
            string[] fragments = line.Split(',');

            switch (fragments.Length)
            {
                case 2:
                    Rank = int.Parse(fragments[0]);
                    Level = int.Parse(fragments[1]);
                    return;
                case 3:
                    Rank = int.Parse(fragments[0]);
                    Level = int.Parse(fragments[1]);
                    Experience = long.Parse(fragments[2]);
                    break;
            }
        }

        #endregion

        #region Overrides of Object

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"{Rank},{Level},{Experience}";
        }

        #endregion
    }
}