﻿using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    internal class ClanRanking : IClanRanking
    {
        #region Implementation of IClanRanking

        [JsonProperty("rank")]
        public int Rank { get; set; }

        [JsonProperty("clan_name")]
        public string Name { get; set; }

        [JsonProperty("clan_mates")]
        public int ClanMates { get; set; }

        [JsonProperty("xp_total")]
        public long TotalExperience { get; set; }

        #endregion
    }
}