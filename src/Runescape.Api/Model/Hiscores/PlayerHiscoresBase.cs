﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    internal class PlayerHiscoresBase<TSkill, TActivity>
    {
        #region "Public Properties"

        public string PlayerName { get; set; }

        public Dictionary<TSkill, IHiscore> Skills { get; }

        public Dictionary<TActivity, IHiscore> Activities { get; }

        #endregion

        #region "Constructor"

        public PlayerHiscoresBase()
        {
            Skills = new Dictionary<TSkill, IHiscore>(30);
            Activities = new Dictionary<TActivity, IHiscore>(30);
        }

        #endregion
    }
}