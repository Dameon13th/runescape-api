﻿using System.Diagnostics;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Name} - {Rank}")]
    internal class ClanMember : IClanMember
    {
        #region Implementation of IClanMember

        public string Name { get; set; }

        public string Rank { get; set; }

        public long TotalExperience { get; set; }

        public int Kills { get; set; }

        #endregion

        public ClanMember(string line)
        {
            string[] fragments = line.Split(',');
            if (fragments.Length != 4)
            {
                return;
            }

            Name = fragments[0];
            Rank = fragments[1];
            TotalExperience = long.Parse(fragments[2]);
            Kills = int.Parse(fragments[3]);
        }

        #region Overrides of Object

        public override string ToString()
        {
            return $"{Name},{Rank},{TotalExperience},{Kills}";
        }

        #endregion
    }
}