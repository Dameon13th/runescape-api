﻿using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Name} ({Id})")]
    internal class BeastData : IBeastData
    {
        #region Implementation of IBeastData

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("members")]
        public bool IsMembers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("weakness")]
        public string Weakness { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("level")]
        public int Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("lifepoints")]
        public int LifePoints { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("defence")]
        public int Defense { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("attack")]
        public int Attack { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("magic")]
        public int Magic { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ranged")]
        public int Ranged { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("xp")]
        public decimal ExperiencePoints { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("slayerlevel")]
        public int SlayerLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("slayercat")]
        public string SlayerCategory { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("size")]
        public int Size { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("attackable")]
        public bool IsAttackable { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("aggressive")]
        public bool IsAggressive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("poisonous")]
        public bool IsPosonous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("areas")]
        public List<string> Area { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("animations")]
        public Dictionary<string, long> Animations { get; set; }

        IReadOnlyList<string> IBeastData.Area
        {
            get { return Area; }
        }

        IReadOnlyDictionary<string, long> IBeastData.Animations
        {
            get { return Animations; }
        }

        #endregion
    }
}