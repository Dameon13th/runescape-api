﻿using System.Diagnostics;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Name} ({Id})")]
    internal class WeaknessImpl : IWeakness
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}