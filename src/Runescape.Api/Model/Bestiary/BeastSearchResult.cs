﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Name} ({Id})")]
    internal class BeastSearchResult : IBeastSearchResult
    {
        #region Implementation of IBeastSearchResult

        [JsonProperty("value")]
        public long Id { get; set; }

        [JsonProperty("label")]
        public string Name { get; set; }

        #endregion
    }
}