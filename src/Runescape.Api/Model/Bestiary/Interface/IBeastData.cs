﻿using System.Collections.Generic;

namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a beast.
    /// </summary>
    public interface IBeastData
    {
        /// <summary>
        /// Gets the name of the beast.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Id of the beast.
        /// </summary>
        long Id { get; }

        /// <summary>
        /// Gets the value that determines if the beast requires a membership.
        /// </summary>
        bool IsMembers { get; }

        /// <summary>
        /// Gets the weakness of the beast.
        /// </summary>
        string Weakness { get; }

        /// <summary>
        /// Gets the combat level of the beast.
        /// </summary>
        int Level { get; }

        /// <summary>
        /// Gets the number of life points the beast has.
        /// </summary>
        int LifePoints { get; }

        /// <summary>
        /// Gets the defense level of the beast.
        /// </summary>
        int Defense { get; }

        /// <summary>
        /// Gets the attack level of the beast.
        /// </summary>
        int Attack { get; }

        /// <summary>
        /// Gets the magic level of the beast.
        /// </summary>
        int Magic { get; }

        /// <summary>
        /// Gets the range level of the beast.
        /// </summary>
        int Ranged { get; }

        /// <summary>
        /// Gets the experience points gained when defeating this beast.
        /// </summary>
        decimal ExperiencePoints { get; }

        /// <summary>
        /// Gets the slayer level required in order to defeat this beast.
        /// </summary>
        int SlayerLevel { get; }

        /// <summary>
        /// Gets the slayer category this beast belongs too.
        /// </summary>
        string SlayerCategory { get; }

        /// <summary>
        /// Gets the size of the beast.
        /// </summary>
        int Size { get; }

        /// <summary>
        /// Gets a value that determines if this beast can be attacked.
        /// </summary>
        bool IsAttackable { get; }

        /// <summary>
        /// Gets a value that determines if this beast is will attack once a player is in range..
        /// </summary>
        bool IsAggressive { get; }

        /// <summary>
        /// Gets a value that determines if this beast can cause a player to be poisoned.
        /// </summary>
        bool IsPosonous { get; }

        /// <summary>
        /// Gets the description of the beast.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets a read only list of the areas that this beast can be found it.
        /// </summary>
        IReadOnlyList<string> Area { get; }

        /// <summary>
        /// Gets a read only dictionary that where the key is the name of an animation and the value is the animations Id.
        /// </summary>
        IReadOnlyDictionary<string, long> Animations { get; }
    }
}