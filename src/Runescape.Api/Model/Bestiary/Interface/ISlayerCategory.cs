﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a slayer category.
    /// </summary>
    public interface ISlayerCategory
    {
        /// <summary>
        /// Gets the name of the slayer category.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Id of the slayer category.
        /// </summary>
        int Id { get; }
    }
}