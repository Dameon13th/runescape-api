﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a beast search result.
    /// </summary>
    public interface IBeastSearchResult
    {
        /// <summary>
        /// Gets the Id of the beast.
        /// </summary>
        long Id { get; }

        /// <summary>
        /// Gets the name of the beast. Commonly following by the beast's level in parenthesis. 
        /// </summary>
        string Name { get; }
    }
}