﻿namespace Runescape.Api.Model
{
    /// <summary>
    /// Interface that defines the properties of a beast weakness.
    /// </summary>
    public interface IWeakness
    {
        /// <summary>
        /// Gets the name of the weakness.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Id of the weakness.
        /// </summary>
        int Id { get; }
    }
}