﻿using System.Diagnostics;

namespace Runescape.Api.Model
{
    [DebuggerDisplay("{Name} ({Id})")]
    internal class SlayerCategoryImpl : ISlayerCategory
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}