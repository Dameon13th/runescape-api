﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Runescape.Api.Model;

namespace Runescape.Api
{
    internal class GrandExchange : ApiWrapperBase, IGrandExchange
    {
        #region "Constructor"

        public GrandExchange(IOptions<RunescapeApiOptions> options, IHttpClientFactory httpClientFactory)
            : base(options, httpClientFactory) { }

        #endregion

        #region "Private Methods"

        private async Task<IImage> GetImage(long itemId, ImageSize size)
        {
            Image image = new Image();

            string url = ApiUrls.GrandExchange.BuildItemImageUrl(itemId, size);

            MemoryStream tempStream = null;
            MemoryStream returnStream;

            try
            {
                using (HttpResponseMessage response = await GetResponse(url))
                {
                    if (response == null)
                    {
                        return null;
                    }

                    string contentType = response.Content.Headers.ContentType.MediaType;
                    image.Extension = contentType.Substring(contentType.IndexOf('/') + 1);
                    image.Length = response.Content.Headers.ContentLength.GetValueOrDefault();

                    using (Stream stream = await response.Content.ReadAsStreamAsync())
                    {
                        if (stream == null)
                        {
                            return null;
                        }
                        tempStream = new MemoryStream();
                        stream.CopyTo(tempStream);
                        tempStream.Seek(0, SeekOrigin.Begin);

                        returnStream = tempStream;
                        tempStream = null;
                    }
                }
            }
            finally
            {
                if (tempStream != null)
                {
                    tempStream.Dispose();
                }
            }

            image.Stream = returnStream;
            return image;
        }

        #endregion

        #region "Implementation of IGrandeExchange"

        public async Task<DateTime> GetLastUpdatedDateAsync()
        {
            return await TryGetFromCache("last-updated", async () =>
            {
                LastUpdatedResponse response = await GetResponseObject<LastUpdatedResponse>(ApiUrls.GrandExchange.LAST_UPDATE);

                // This is the date that the Rune dates start
                // https://runescape.wiki/w/Runedate
                DateTime returnValue = new DateTime(2002, 2, 27);

                if (response != null)
                {
                    returnValue = returnValue.AddDays(response.DaysSinceUpdate);
                }

                return returnValue;
            });
        }

        public async Task<ICategoryResult> GetCategoryAsync(GrandExchangeCategory category)
        {
            return await TryGetFromCache($"category-{category}", async () =>
            {
                string url = ApiUrls.GrandExchange.BuildCatergoryUrl(category);

                CategoryResult result = new CategoryResult(category);

                GetCategoriesResponse response = await GetResponseObject<GetCategoriesResponse>(url);
                if (response == null)
                {
                    return result;
                }

                foreach (CountByLetter countByLetter in response.CountByLetter)
                {
                    result.CountByLetter[countByLetter.Letter] = countByLetter.ItemCount;
                }

                return result;
            });
        }

        public async Task<IItemsResult> GetItemsAsync(GrandExchangeCategory category, char firstLetter, int pageNumber = 1)
        {
            if (!char.IsLetterOrDigit(firstLetter))
            {
                throw new ArgumentException($"'{nameof(firstLetter)}' must be an alphanumeric character.", nameof(firstLetter));
            }

            return await TryGetFromCache($"items-{category}-{firstLetter}-{pageNumber}", async () =>
            {
                string url = ApiUrls.GrandExchange.BuildItemsUrl(category, firstLetter, pageNumber);

                ItemsResponse response = await GetResponseObject<ItemsResponse>(url);
                if (response == null)
                {
                    return ItemsResponse.Empty;
                }
                response.Category = category;
                response.FirstLetter = firstLetter;
                response.PageNumber = pageNumber;
                return response;
            });
        }

        public async Task<IItemDetail> GetItemDetailAsync(long itemId)
        {
            return await TryGetFromCache($"item-{itemId}", async () =>
            {
                string url = ApiUrls.GrandExchange.BuildItemDetailUrl(itemId);
                ItemDetailResponse response = await GetResponseObject<ItemDetailResponse>(url);
                return response?.Item;
            });
        }

        public async Task<IImage> GetImageAsync(long itemId, ImageSize size)
        {
            return await TryGetFromCache($"image-{itemId}-{size}", async () => await GetImage(itemId, size));
        }

        #endregion
    }
}