﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Runescape.Api.Model;

namespace Runescape.Api
{
    internal class SolomonStoreApi : ApiWrapperBase, ISolomonStoreApi
    {
        #region "Constructor"

        public SolomonStoreApi(IOptions<RunescapeApiOptions> options, IHttpClientFactory httpClientFactory)
            : base(options, httpClientFactory) { }

        #endregion

        #region "Implementation of ISolomonStoreApi"

        public async Task<ISolomonResponse> GetAsync()
        {
            SolomonResponseRaw responseRaw = await GetResponseObject<SolomonResponseRaw>(ApiUrls.SolomonStore.GetUrl());

            SolomonResponse response = new SolomonResponse
            {
                Type = responseRaw.Type,
                Urls = responseRaw.Urls,
                PhraseBook = responseRaw.PhraseBook,
                LastUpdated = responseRaw.LastUpdated
            };

            foreach (KeyValuePair<string, MediaWrapper> valuePair in responseRaw.Media)
            {
                long mediaId;
                if (!long.TryParse(valuePair.Key, out mediaId))
                {
                    continue;
                }

                Media rawMedia = valuePair.Value.Media;
                IMedia media;
                if (rawMedia.Type == "::mtxn_rs_shop#Json_ImageMedia")
                {
                    media = new MediaImage
                    {
                        Type = rawMedia.Type,
                        ImageUrl = $"{ApiUrls.BaseUrl}{rawMedia.ImageUrl}"
                    };
                }
                else
                {
                    media = new MediaVideo
                    {
                        Type = rawMedia.Type,
                        VideoUrl = $"{ApiUrls.BaseUrl}{rawMedia.VideoUrl}",
                        ImageUrl = $"{ApiUrls.BaseUrl}{rawMedia.PosterUrl}"
                    };
                }

                response.Media[mediaId] = media;
            }

            return response;
        }

        #endregion
    }
}