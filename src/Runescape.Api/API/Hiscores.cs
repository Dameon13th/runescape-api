﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Runescape.Api.Model;

namespace Runescape.Api
{
    internal class Hiscores : ApiWrapperBase, IHiscores
    {
        #region "Constructor"

        public Hiscores(IOptions<RunescapeApiOptions> options, IHttpClientFactory httpClientFactory)
            : base(options, httpClientFactory) { }

        #endregion

        #region "Implementation of IHiscores"

        public async Task<IReadOnlyList<IRanking>> GetRankingAsync(Skill skill, int count)
        {
            string apiUrl = ApiUrls.Hiscores.BuildRankingsUrl(skill, count);
            List<Ranking> rankings = await GetResponseObject<List<Ranking>>(apiUrl);
            return rankings;
        }

        public async Task<IReadOnlyList<IRanking>> GetRankingAsync(Activity activity, int count)
        {
            string apiUrl = ApiUrls.Hiscores.BuildRankingsUrl(activity, count);
            List<Ranking> rankings = await GetResponseObject<List<Ranking>>(apiUrl);
            return rankings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="playerName"></param>
        /// <returns></returns>
        public async Task<IPlayerHiscores> GetHiscoresAsync(GameMode mode, string playerName)
        {
            PlayerHiscores hiscores = new PlayerHiscores(playerName)
            {
                Mode = mode
            };

            string apiUrl = ApiUrls.Hiscores.BuildHiscoresUrl(mode, playerName);
            string responseString = await GetResponseString(apiUrl);
            if (string.IsNullOrEmpty(responseString))
            {
                return hiscores;
            }

            string[] lines = responseString.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (Skill skill in Enum.GetValues(typeof(Skill)).OfType<Skill>())
            {
                string line = lines[(int)skill];
                Hiscore hiscore = new Hiscore(line);
                hiscores.Skills[skill] = hiscore;
            }

            foreach (Activity activity in Enum.GetValues(typeof(Activity)).OfType<Activity>())
            {
                string line = lines[(int)activity + 29];
                Hiscore hiscore = new Hiscore(line);
                hiscores.Activities[activity] = hiscore;
            }

            return hiscores;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<ISeasonalRanking>> GetSeasonalRankingsAsync(string playerName, SeasonType seasonType)
        {
            string apiUrl = ApiUrls.Hiscores.BuildSeasonalHiscore(playerName, seasonType);
            List<SeasonalRanking> rankings = await GetResponseObject<List<SeasonalRanking>>(apiUrl);
            return rankings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<ISeason>> GetSeasonsAsync(SeasonType seasonType)
        {
            string apiUrl = ApiUrls.Hiscores.BuildSeasonUrl(seasonType);
            List<Season> seasons = await GetResponseObject<List<Season>>(apiUrl);
            return seasons;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<IClanRanking>> GetClanRankingsAsync()
        {
            List<ClanRanking> clanRankings = await GetResponseObject<List<ClanRanking>>(ApiUrls.Hiscores.CLAN_RANKINGS);
            return clanRankings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clanName"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<IClanMember>> GetClanMembersAsync(string clanName)
        {
            List<IClanMember> members = new List<IClanMember>();

            string apiUrl = ApiUrls.Hiscores.BuildClanMembersUrl(clanName);
            string response = await GetResponseString(apiUrl);
            if (string.IsNullOrEmpty(response))
            {
                return members;
            }

            string[] lines = response.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 1; i < lines.Length; i++)
            {
                ClanMember clanMember = new ClanMember(lines[i]);
                members.Add(clanMember);
            }

            return members;
        }

        #endregion
    }
}
