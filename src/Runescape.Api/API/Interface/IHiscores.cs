﻿using Runescape.Api.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Runescape.Api
{
    /// <summary>
    /// Interface that defines the methods for calling into the Hiscores REST API.
    /// </summary>
    public interface IHiscores
    {
        /// <summary>
        /// Gets up to the top fifty rankings for a given <paramref name="skill"/>.
        /// </summary>
        /// <param name="skill">The skill to get the rankings for.</param>
        /// <param name="count">The number of rankings to retrieve. The maximum is 50.</param>
        /// <returns>
        /// An awaitable task that when completed will return a collection of <see cref="IRanking"/>.
        /// If there is an error the task will return Null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IReadOnlyList<IRanking> rankings = await hiscores.GetRankingAsync(Skill.Attack, 3);
        ///
        ///         foreach(IRanking ranking in rankings)
        ///         {
        ///             Console.WriteLine($"Name: {ranking.Name}, Rank: {ranking.Rank}, Score: {ranking.Score}");
        ///         }
        ///     }
        /// }
        ///
        /// // Name: Aclaw, Rank: 1, Score: 200000000
        /// // Name: Deja Nu Xiii, Rank: 2, Score: 200000000
        /// // Name: Doesnt Fit, Rank: 3, Score: 200000000
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IRanking>> GetRankingAsync(Skill skill, int count);

        /// <summary>
        /// Gets up to the top fifty rankings for a given <paramref name="activity"/>.
        /// </summary>
        /// <param name="activity">The activity to get the rankings for.</param>
        /// <param name="count">The number of rankings to retrieve. The maximum is 50.</param>
        /// <returns>
        /// An awaitable task that when completed will return a collection of <see cref="IRanking"/>.
        /// If there is an error the task will return Null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IReadOnlyList<IRanking> rankings = await hiscores.GetRankingAsync(Activity.BountyHunters, 3);
        ///
        ///         foreach(IRanking ranking in rankings)
        ///         {
        ///             Console.WriteLine($"Name: {ranking.Name}, Rank: {ranking.Rank}, Score: {ranking.Score}");
        ///         }
        ///     }
        /// }
        ///
        /// // Name: Viva Spunj, Rank: 1, Score: 11387
        /// // Name: Blacked Out, Rank: 2, Score: 4402
        /// // Name: neoraider, Rank: 3, Score: 4361
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IRanking>> GetRankingAsync(Activity activity, int count);

        /// <summary>
        /// Gets the hi scores for a given <paramref name="playerName"/>.
        /// </summary>
        /// <param name="mode">The game mode to retrieve the player's hi scores for.</param>
        /// <param name="playerName">The name of the player to get the hi scores for.</param>
        /// <returns>
        /// An awaitable task that when completed will return a new instance of <see cref="IPlayerHiscores"/>.
        /// If there is an error the IPlayerHiscores will be empty.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IPlayerHiscores playerHiscores = await hiscores.GetHiscoresAsync(GameMode.Normal, "Zezima");
        ///
        ///         foreach (KeyValuePair<Skill, IHiscore> keyValuePair in playerHiscores.Skills)
        ///         {
        ///             Skill skill = keyValuePair.Key;
        ///             IHiscore hiscore = keyValuePair.Value;
        ///             Console.WriteLine($"Skill: {skill}, Level: {hiscore.Level}, Rank: {hiscore.Rank}, Experience: {hiscore.Experience}");
        ///         }
        ///
        ///         foreach (KeyValuePair<Activity, IHiscore> keyValuePair in playerHiscores.Activities)
        ///         {
        ///             Activity activity = keyValuePair.Key;
        ///             IHiscore hiscore = keyValuePair.Value;
        ///             Console.WriteLine($"Activity: {activity}, Rank: {hiscore.Rank}");
        ///         }
        ///     }
        /// }
        ///
        /// // Skill: Overall, Level: 2853, Rank: 27867, Experience: -1
        /// // Skill: Attack, Level: 99, Rank: 480, Experience: 20000000
        /// // Skill: Defense, Level: 99, Rank: 904, Experience: 20000000
        /// _____________________________________________
        /// // Activity: BountyHunters, Rank: -1
        /// // Activity: BountyHuntersRogues, Rank: -1
        /// // Activity: DominionTower, Rank: 156
        /// ]]>
        /// </code>
        /// </example>
        Task<IPlayerHiscores> GetHiscoresAsync(GameMode mode, string playerName);

        /// <summary>
        /// Gets the seasonal hi scores for a give <paramref name="playerName"/>.
        /// </summary>
        /// <param name="playerName">The name of the player to get the seasonal hi scores for.</param>
        /// <param name="seasonType">The types of season to get. (Current or Archived)</param> 
        /// <returns>
        /// An awaitable task that when completed will return a collection of <see cref="ISeasonalRanking"/>.
        /// If there is an error the task will return Null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IReadOnlyList<ISeasonRanking> rankings = await hiscores.GetSeasonalRankings("Zezima", SeasonType.Archived);
        ///
        ///         foreach(ISeasonalRanking ranking in rankings)
        ///         {
        ///             Console.WriteLine($"Title: {ranking.Title}, Dates: {ranking.StartDate} - {ranking.EndDate}");
        ///         }
        ///     }
        /// }
        ///
        /// // Title: Fastest kill time: Araxxi (Solo), Dates: 23 Oct 2017 - 29 Oct 2017
        /// // Title: Fastest kill time: Araxxi (Solo), Dates: 16 Oct 2017 - 22 Oct 2017
        /// // Title: Fastest kill time: Araxxi (Solo), Dates: 9 Oct 2017 - 15 Oct 2017
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<ISeasonalRanking>> GetSeasonalRankingsAsync(string playerName, SeasonType seasonType);

        /// <summary>
        /// Gets seasons available for a given <paramref name="seasonType"/>.
        /// </summary>
        /// <param name="seasonType">The types of season to get. (Current or Archived)</param>
        /// <returns>
        /// An awaitable task that when completed will return a collection of <see cref="ISeason"/>.
        /// If there was an error the task will return Null.
        /// If there are no seasons for the given <paramref name="seasonType"/> then an empty array is returned.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IReadOnlyList<ISeason> seasons = await hiscores.GetSeasonsAsync(SeasonType.Archived);
        ///
        ///         foreach (ISeason season in seasons)
        ///         {
        ///             Console.WriteLine($"Title: {season.Title}, Status: {season.Status}, Dates: {season.StartDate} - {season.EndDate}");
        ///         }
        ///     }
        /// }
        /// // Title: Fastest kill time: Nex (Solo), Status: ARCHIVED, Dates: 23 Oct 2017 - 29 Oct 2017
        /// // Title: Fastest kill time: Nex (Solo), Status: ARCHIVED, Dates: 16 Oct 2017 - 22 Oct 2017
        /// // Title: Fastest kill time: Nex (Solo), Status: ARCHIVED, Dates: 9 Oct 2017 - 15 Oct 2017
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<ISeason>> GetSeasonsAsync(SeasonType seasonType);

        /// <summary>
        /// Gets the top three clans.
        /// </summary>
        /// <returns>
        /// An awaitable task that when completed will return a collection of <see cref="IClanRanking"/>.
        /// If there is an error the task will return null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IReadOnlyList<IClanRanking> rankings = await hiscores.GetClanRankingsAsync();
        ///
        ///         foreach (IClanRanking clanRanking in rankings)
        ///         {
        ///             Console.WriteLine($"Name: {clanRanking.Name}, Rank: {clanRanking.Rank}, Member Count: {clanRanking.ClanMates}, Total XP: {clanRanking.TotalExperience}");
        ///         }
        ///     }
        /// }
        /// // Name: Efficiency Experts, Rank: 1, Member Count: 495, Total XP: 799033363476
        /// // Name: Maxed, Rank: 2, Member Count: 497, Total XP: 679531596273
        /// // Name: Mining Golds, Rank: 3, Member Count: 498, Total XP: 432665265524
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IClanRanking>> GetClanRankingsAsync();

        /// <summary>
        /// Get the members for a given <paramref name="clanName"/>.
        /// </summary>
        /// <param name="clanName">The name of the clan to get a list of members of.</param>
        /// <returns>
        /// An awaitable task that when completed will return a collection of <see cref="IClanMember"/>.
        /// If there is an error the task will return null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IHiscores hiscores = ApiFactory.CreateHiscores();
        ///         IReadOnlyList<IClanMember> members = await hiscores.GetClanMembersAsync("Efficiency Experts");
        ///
        ///         foreach (IClanMember member in members)
        ///         {
        ///             Console.WriteLine($"Name: {member.Name}, Rank: {member.Rank}, Kill Count: {member.Kills}, Total XP: {member.TotaleExperience}");
        ///         }
        ///     }
        /// }
        /// // Name: Dragonseance, Rank: Owner, Kill Count: 210, Total XP: 4429567564
        /// // Name: Evu, Rank: Deputy Owner, Kill Count: 89, Total XP: 4553547296
        /// // Name: Determined, Rank: Deputy Owner, Kill Count: 25, Total XP: 5169029669
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IClanMember>> GetClanMembersAsync(string clanName);
    }
}