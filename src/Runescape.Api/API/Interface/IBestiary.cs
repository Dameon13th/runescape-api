﻿using Runescape.Api.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Runescape.Api
{
    /// <summary>
    /// Interface that defines the methods for calling into the Bestiary REST API.
    /// </summary>
    public interface IBestiary
    {
        /// <summary>
        /// Gets the data for a specific beast.
        /// </summary>
        /// <param name="beastId">The Id of the beast to get.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a new instance of <see cref="IBeastData"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IBeastData beastData = await bestiary.GetBeastDataAsync(18373);
        ///
        ///         Console.WriteLine($"Name: {beastData.Name}");
        ///         Console.WriteLine($"Attack: {beastData.Attack}");
        ///         Console.WriteLine($"Description: {beastData.Description}");
        ///         Console.WriteLine($"Level: {beastData.Level}");
        ///     }
        /// }
        ///
        /// // Name: Chaos Giant
        /// // Attack: 90
        /// // Description: A last product of the Red Axe's experiments
        /// // Level: 126
        /// ]]>
        /// </code>
        /// </example>
        Task<IBeastData> GetBeastDataAsync(long beastId);

        /// <summary>
        /// Performs a search for beasts whose names match the given search terms.
        /// </summary>
        /// <param name="searchTerms">The collections of terms to use for the search.</param>
        /// <returns>
        /// An awaitable task that when completed will contain an instance of <see cref="IBeastSearchResult"/> for every beast whose name matches the search terms.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.SearchBeastsAsync("Cow", "Imp");
        ///
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 47
        /// // Name: Cow (4)
        /// // Id: 81
        /// // Name: Cow (4)
        /// // Id: 397
        /// // Name: Imp (5)
        /// // Id: 708
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> SearchBeastsAsync(params string[] searchTerms);

        /// <summary>
        /// Gets a collection of beasts whose name starts with the given <paramref name="firstLetter"/>.
        /// </summary>
        /// <param name="firstLetter">The letter to be used for the search.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> for each beast that start with the given <paramref name="firstLetter"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.SearchBeastsAsync('b');
        /// 
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 263
        /// // Name: Baby Yaga
        /// // Id: 4513
        /// // Name: Baby Raccoon
        /// // Id: 7275
        /// // Name: Baby Roc
        /// // 4971
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> SearchBeastsAsync(char firstLetter);

        /// <summary>
        /// Gets a collection of the area names the beasts can reside in.
        /// </summary>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of the area names.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<string> areas = await bestiary.GetAreaNamesAsync();
        ///
        ///         Console.WriteLine($"Area Count: {areas.Count}");
        ///         foreach (string area in areas)
        ///         {
        ///             Console.WriteLine(area);
        ///         }
        ///     }
        /// }
        ///
        /// // Area Count: 293
        /// // Agility Pyramid
        /// // Agility course pit
        /// // Air Rune Temple
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<string>> GetAreaNamesAsync();

        /// <summary>
        /// Gets a collection of <see cref="IBeastSearchResult"/> that belong to a given <paramref name="area"/>.
        /// </summary>
        /// <param name="area">The name of the area to get the beasts of.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> for a given <paramref name="area"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.GetBeastsByAreaAsync("Agility Pyramid");
        /// 
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 6
        /// // Name: Simon Templeton
        /// // Id: 3123
        /// // Name: Talent scout
        /// // Id: 14363
        /// // Name: Trainee adventurer
        /// // 22327
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByAreaAsync(string area);

        /// <summary>
        /// Gets a collection of <see cref="ISlayerCategory"/> that exist in the game.
        /// </summary>
        /// <remarks>
        /// The results of this call has been used to create the <see cref="SlayerCategory"/> enum. Depending on game updates the enum could be out of date.
        /// </remarks>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="ISlayerCategory"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<ISlayerCategory> slayerCategories = await bestiary.GetSlayerCategoriesAsync();
        ///
        ///         Console.WriteLine($"Slayer Category Count: {slayerCategories.Count}");
        ///         foreach (ISlayerCategory category in slayerCategories)
        ///         {
        ///             Console.WriteLine($"Name: {category.Name}");
        ///             Console.WriteLine($"Id: {category.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Slayer Category Count: 160
        /// // Name: Aberrant spectres
        /// // Id: 41
        /// // Name: Abyssal demons
        /// // Id: 42
        /// // Name: Acheron mammoths
        /// // Id: 133
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<ISlayerCategory>> GetSlayerCategoriesAsync();

        /// <summary>
        /// Gets a collection of <see cref="IBeastSearchResult"/> that belong to a given <paramref name="category"/>.
        /// </summary>
        /// <remarks>
        /// Some of the slayer categories are no longer in use Polypore(100) Creates for instance.
        /// </remarks>
        /// <param name="category">The slayer category name to get the beasts of.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> for a given <paramref name="category"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.GetBeastsBySlayerCategory(SlayerCategory.AberrantSpectres);
        /// 
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 1
        /// // Name: Aberrant spectre (72)
        /// // Id: 1604
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> GetBeastsBySlayerCategory(SlayerCategory category);

        /// <summary>
        /// Gets a collection of <see cref="IBeastSearchResult"/> that belong to a given <paramref name="categoryId"/>.
        /// </summary>
        /// <remarks>
        /// Some of the slayer categories are no longer in use Polypore(100) Creates for instance.
        /// </remarks>
        /// <param name="categoryId">The slayer category id to get the beasts of.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> for a given <paramref name="categoryId"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.GetBeastsBySlayerCategory(41);
        /// 
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 1
        /// // Name: Aberrant spectre (72)
        /// // Id: 1604
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> GetBeastsBySlayerCategory(int categoryId);

        /// <summary>
        /// Gets a collection of <see cref="IWeakness"/> that exist in the game. 
        /// </summary>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IWeakness"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IWeakness> weaknesses = await bestiary.GetWeaknessesAsync();
        ///
        ///         Console.WriteLine($"Weaknesses Count: {weaknesses.Count}");
        ///         foreach (IWeakness weakness in weaknesses)
        ///         {
        ///             Console.WriteLine($"Name: {weakness.Name}");
        ///             Console.WriteLine($"Id: {weakness.Id}");
        ///             }
        ///         }
        ///     }
        /// }
        ///
        /// // Weaknesses Count: 11
        /// // Name: Air
        /// // Id: 1
        /// // Name: Arrow
        /// // Id: 8
        /// // Name: Bolt
        /// // Id: 9
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IWeakness>> GetWeaknessesAsync();

        /// <summary>
        /// Gets a collection of <see cref="IBeastSearchResult"/> that have the given <paramref name="weakness"/>.
        /// </summary>
        /// <param name="weakness">The weakness used to get a list of beasts.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> for a given <paramref name="weakness"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.GetBeastsByWeaknessAsync(Weakness.Air);
        ///
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 417
        /// // Name: Adamant dragon (116)
        /// // Id: 21135
        /// // Name: Afflicted (60)
        /// // Id: 1257
        /// // Name: Afflicted (61)
        /// // 1258
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByWeaknessAsync(Weakness weakness);

        /// <summary>
        /// Gets a collection of <see cref="IBeastSearchResult"/> that have the given <paramref name="weaknessId"/>.
        /// </summary>
        /// <param name="weaknessId">The weakness Id used to get a list of beasts.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> for a given <paramref name="weaknessId"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///         IReadOnlyList<IBeastSearchResult> results = await bestiary.GetBeastsByWeaknessAsync(1);
        ///
        ///         Console.WriteLine($"Search Result Count: {results.Count}");
        ///         foreach (IBeastSearchResult result in results)
        ///         {
        ///             Console.WriteLine($"Name: {result.Name}");
        ///             Console.WriteLine($"Id: {result.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Search Result Count: 417
        /// // Name: Adamant dragon (116)
        /// // Id: 21135
        /// // Name: Afflicted (60)
        /// // Id: 1257
        /// // Name: Afflicted (61)
        /// // 1258
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByWeaknessAsync(int weaknessId);

        /// <summary>
        /// Gets a collection of <see cref="IBeastSearchResult"/> whose level falls between the given <paramref name="low"/> and <paramref name="high"/>.
        /// </summary>
        /// <param name="low">The value representing the lower level of the range/</param>
        /// <param name="high">The value representing the higher level of the range.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a collection of <see cref="IBeastSearchResult"/> whose level falls between the given <paramref name="low"/> and <paramref name="high"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IBestiary bestiary = ApiFactory.CreatBestiary();
        ///     }
        /// }
        ///
        /// // Search Result Count: 208
        /// // Name: Black dragon (100)
        /// // Id: 54
        /// // Name: Steel dragon (100)
        /// // Id: 1592
        /// // Name: Rock lobster (100)
        /// // 2889
        /// ]]>
        /// </code>
        /// </example>
        Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByLevelGroupAsync(int low, int high);
    }
}