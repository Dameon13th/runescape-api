﻿using System;
using System.Threading.Tasks;
using Runescape.Api.Model;

namespace Runescape.Api
{
    /// <summary>
    /// Interface that defines the methods for calling into the Grand Exchange REST API.
    /// </summary>
    public interface IGrandExchange
    {
        /// <summary>
        /// Gets the date the grand exchange item database was last updated.
        /// </summary>
        /// <returns>
        /// An awaitable task that when completed will contain the Date the Grand Exchange database was updated.
        /// If there is an error the task will return the RuneDate (2/27/2002)
        /// </returns>
        /// <example> 
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IGrandExchange grandExchange = ApiFactory.CreateGrandExchange();
        ///         DateTime lastUpdated = await grandExchange.GetLastUpdatedDateAsync();
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>
        Task<DateTime> GetLastUpdatedDateAsync();

        /// <summary>
        /// Gets the number of items per letter of the alphabet for a given <paramref name="category"/>.
        /// </summary>
        /// <param name="category">The category to get the results for.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a new instance of <see cref="ICategoryResult"/>.
        /// If there was an error the instance returned will be empty.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IGrandExchange grandExchange = ApiFactory.CreateGrandExchange();
        ///         ICategoryResult categoryResult = await grandExchange.GetCategoryAsync(GrandExchangeCategory.Ammo);
        ///
        ///         foreach(KeyValuePair<string,int> valuePair in categoryResult.CountByLetter)
        ///         {
        ///             Console.WriteLine($"Category : {categoryResult.Category}, Letter: {valuePair.Key}, Count = {valuePair.Value}");
        ///         }
        ///     }
        /// }
        ///
        /// // Category : Ammo, Letter: #, Count = 0
        /// // Category : Ammo, Letter: a, Count = 6
        /// // Category : Ammo, Letter: b, Count = 10
        /// // Category : Ammo, Letter: c, Count = 7 
        /// ]]>
        /// </code>
        /// </example>
        Task<ICategoryResult> GetCategoryAsync(GrandExchangeCategory category);

        /// <summary>
        /// Gets a collection of items that belong to the given <paramref name="category"/> and start the given <paramref name="firstLetter"/>.
        /// Only 12 items are returned at a time. To get more increment the given <paramref name="pageNumber"/>.
        /// </summary>
        /// <param name="category">The category of items you want to get.</param>
        /// <param name="firstLetter">The first letter of the items you want to get.</param>
        /// <param name="pageNumber">The page number to retrieve. This number starts at 1 not 0.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a new instance of <see cref="IItemsResult"/>
        /// If there was an error the instance returned will be empty.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IGrandExchange grandExchange = ApiFactory.CreateGrandExchange();
        ///         IItemsResult itemsResult = await grandExchange.GetItemsAsync(GrandExchangeCategory.Ammo, 'a', 1);
        ///
        ///         Console.WriteLine($"Category: {itemsResult.Category}");
        ///         Console.WriteLine($"First Letter: {itemsResult.FirstLEtter}");
        ///         Console.WriteLine($"Count: {itemsResult.Count}"); // This is the total item count for the category, not the items for the first letter.
        ///
        ///         foreach(IItem item in itemsResult.Items)
        ///         {
        ///             Console.WriteLine($"Name: {item.Name}, Id: {item.Id}");
        ///         }
        ///     }
        /// }
        ///
        /// // Category : Ammo
        /// // First Letter: 'a'
        /// // Count : 87
        /// // Name : Adamant brutal, Id: 4798
        /// // Name : Adamant dart, Id: 810
        /// // Name : Adamant javelin, Id: 829
        /// // Name : Adamant knife, Id: 867
        /// // Name : Adamant throwing axe, Id: 804
        /// // Name : Azure skillchompa, Id: 31597
        /// ]]>
        /// </code>
        /// </example>
        Task<IItemsResult> GetItemsAsync(GrandExchangeCategory category, char firstLetter, int pageNumber = 1);

        /// <summary>
        /// Gets the Grand Exchange details for a specific item.
        /// </summary>
        /// <param name="itemId">The Id of the item to get.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a new instance of <see cref="IItemDetail"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IGrandExchange grandExchange = ApiFactory.CreateGrandExchange();
        ///         IItemDetail itemDetail = await grandExchange.GetItemDetailAsync(4798);
        ///
        ///         Console.WriteLine($"Name: {itemDetail.Name}");
        ///         Console.WriteLine($"Id: {itemDetail.Id}");
        ///         Console.WriteLine($"Icon Url: {itemDetail.IconUrl}");
        ///         Console.WriteLine($"Is Members: {itemDetail.IsMembers}");
        ///         Console.WriteLine($"Price: {itemDetail.Current.Value");
        ///         Console.WriteLine($"30 Price Change: {itemDetail.Day30.Trend} {itemDetail.Day30.Change}");
        ///     }
        /// }
        ///
        /// // Name: Adamant brutal
        /// // Id: 4798
        /// // Icon Url: https://secure.runescape.com/m=itemdb_rs/1597744869674_obj_sprite.gif?id=4798
        /// // Is Members: true
        /// // Price: 364
        /// // 30 Price Change: negative -1.0% 
        /// ]]>
        /// </code>
        /// </example>
        Task<IItemDetail> GetItemDetailAsync(long itemId);

        /// <summary>
        /// Get an image for a specific item.
        /// </summary>
        /// <param name="itemId">The Id of the item to get.</param>
        /// <param name="size">The size of the image to get.</param>
        /// <returns>
        /// An awaitable task that when completed will contain a new instance of <see cref="IImage"/>.
        /// If there was an error the instance will be null.
        /// </returns>
        /// <example>
        /// Example
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IGrandExchange grandExchange = ApiFactory.CreateGrandExchange();
        ///         IImage image = await grandExchange.GetImageAsync(4798, ImageSize.Sprite);
        ///
        ///         Console.WriteLine($"Extension: {image.Extension}");
        ///         Console.WriteLine($"Length: {image.Length}");
        ///
        ///         // Use the image.Stream to hydrate your preffered image object.
        ///     }
        /// }
        ///
        /// // Extension: gif
        /// // Length: 226
        /// ]]>
        /// </code>
        /// </example>
        Task<IImage> GetImageAsync(long itemId, ImageSize size);
    }
}