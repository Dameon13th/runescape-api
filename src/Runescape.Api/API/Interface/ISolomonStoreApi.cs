﻿using System.Threading.Tasks;
using Runescape.Api.Model;

namespace Runescape.Api
{
    /// <summary>
    /// Interface that defines the methods for calling into the Solomon store. REST API.
    /// </summary>
    public interface ISolomonStoreApi
    {
        /// <summary>
        /// Gets information about the items for sale in the Solomon store.
        /// </summary>
        /// <returns>
        /// An awaitable task that when completed will contain information for all the items for sale.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         ISolomomStoreApi solomonApi = ApiFactory.CreateSolomonStore();
        ///         ISolomonResponse response = await solomonApi.GetAsync();
        ///
        ///         Console.WriteLine($"Last Updated: {response.LastUpdated}");
        ///
        ///          foreach (KeyValuePair<long, IMedia> valuePair in response.Media)
        ///          {
        ///             Console.WriteLine($"ID: {valuePair.Key}");
        ///             Console.WriteLine($"Type: {valuePair.Value.Type}");
        ///             Console.WriteLine($"Image Url: {valuePair.Value.ImageUrl}");
        ///
        ///             IMediaVideo video = valuePair.Value as IMediaVideo;
        ///             if (video != null)
        ///             {
        ///                 Console.WriteLine($"Video Url: {video.VideoUrl}");
        ///             }
        ///             Console.Writeline();
        ///         }
        ///     }
        /// }
        ///
        /// // Last Updated: 7/19/2021 8:40:57 AM
        /// // ID: 20
        /// // Type: ::mtxn_rs_shop#Json_ImageMedia
        /// // Image Url: https://secure.runescape.com/../m=mtxn_rs_shop/cb=1/media/img/categoryItems/packs/assassin/icon_male.jpg
        /// //
        /// // ID: 23
        /// // Type: ::mtxn_rs_shop#Json_VideoMedia
        /// // Image Url: https://secure.runescape.com/../m=mtxn_rs_shop/cb=1/media/img/categoryItems/packs/assassin/gallery1_poster.jpg
        /// // Video Url: https://secure.runescape.com/../m=mtxn_rs_shop/cb=1/media/img/categoryItems/packs/assassin/gallery1.mp4
        /// ]]>
        /// </code>
        /// </example>
        Task<ISolomonResponse> GetAsync();
    }
}