﻿using System.Threading.Tasks;
using Runescape.Api.Model;

namespace Runescape.Api
{
    /// <summary>
    /// Interface that defines the method for calling into the Old school Hiscores REST API.
    /// </summary>
    public interface IOldSchoolHiscores
    {
        /// <summary>
        /// Gets the old school hi scores for a given <paramref name="playerName"/>.
        /// </summary>
        /// <param name="mode">The game mode to retrieve the player's old school hi scores for.</param>
        /// <param name="playerName">The name of the player to get the hi score for.</param>
        /// <returns>
        /// An awaitable task then when completed will return a new instance of the <see cref="IOldSchoolPlayerHiscores"/>.
        /// </returns>
        /// <example>
        /// <code language="cs">
        /// <![CDATA[
        /// public class Program()
        /// {
        ///     static async void Main(string[] args)
        ///     {
        ///         IOldSchoolHiscores hiscores = ApiFactory.CreateOldSchoolHiscores();
        ///         IOldSchoolPlayerHiscores playerHiscores = await hiscores.GetHiscoresAsync(OldSchoolGameMode.Normal, "Zezima_Os");
        ///
        ///         foreach (KeyValuePair<OldSchoolSkill, IHiscore> keyValuePair in playerHiscores.Skills)
        ///         {
        ///             OldSchoolSkill skill = keyValuePair.Key;
        ///             IHiscore hiscore = keyValuePair.Value;
        ///             Console.WriteLine($"Skill: {skill}, Level: {hiscore.Level}, Rank: {hiscore.Rank}, Experience: {hiscore.Experience}");
        ///         }
        ///
        ///         foreach (KeyValuePair<OldSchoolActivity, IHiscore> keyValuePair in playerHiscores.Activities)
        ///         {
        ///             Activity activity = keyValuePair.Key;
        ///             IHiscore hiscore = keyValuePair.Value;
        ///             Console.WriteLine($"Activity: {activity}, Rank: {hiscore.Rank}");
        ///         }         
        ///     }
        /// }
        /// 
        /// // Skill: Overall, Level: 2853, Rank: 27867, Experience: -1
        /// // Skill: Attack, Level: 99, Rank: 480, Experience: 20000000
        /// // Skill: Defense, Level: 99, Rank: 904, Experience: 20000000
        /// _____________________________________________
        /// // Activity: LeaguePoints, Rank: -1
        /// // Activity: BountyHunterHunter, Rank: -1
        /// // Activity: BountyHunterRogue, Rank: 156
        /// ]]>
        /// </code>
        /// </example>
        Task<IOldSchoolPlayerHiscores> GetHiscoresAsync(OldSchoolGameMode mode, string playerName);
    }
}
