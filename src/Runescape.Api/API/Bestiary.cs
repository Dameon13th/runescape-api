﻿using Microsoft.Extensions.Options;
using Runescape.Api.Model;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Runescape.Api
{
    internal class Bestiary : ApiWrapperBase, IBestiary
    {
        #region "Constructor"

        public Bestiary(IOptions<RunescapeApiOptions> options, IHttpClientFactory httpClientFactory)
            : base(options, httpClientFactory) { }

        #endregion

        #region Implementation of IBestiary

        public async Task<IBeastData> GetBeastDataAsync(long beastId)
        {
            return await TryGetFromCache($"Beast-{beastId}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildBeastDataUrl(beastId);
                BeastData beastData = await GetResponseObject<BeastData>(apiUrl);
                return beastData;
            });
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> SearchBeastsAsync(params string[] searchTerms)
        {
            string joinedSearchTerms = string.Join("+", searchTerms);
            return await TryGetFromCache($"Search-{joinedSearchTerms}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildBeastSearchUrl(searchTerms);
                List<BeastSearchResult> results = await GetResponseObject<List<BeastSearchResult>>(apiUrl);
                return results;
            });
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> SearchBeastsAsync(char firstLetter)
        {
            return await TryGetFromCache($"Bestiary-{firstLetter}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildBeastNameUrl(firstLetter);
                List<BeastSearchResult> results = await GetResponseObject<List<BeastSearchResult>>(apiUrl);
                return results;
            });
        }

        public async Task<IReadOnlyList<string>> GetAreaNamesAsync()
        {
            return await TryGetFromCache("Area", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildAreaNamesUrl();
                List<string> results = await GetResponseObject<List<string>>(apiUrl);
                return results;
            });
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByAreaAsync(string area)
        {
            return await TryGetFromCache($"Area-{area}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildAreaBeastUrl(area);
                List<BeastSearchResult> results = await GetResponseObject<List<BeastSearchResult>>(apiUrl);
                return results;
            });
        }

        public async Task<IReadOnlyList<ISlayerCategory>> GetSlayerCategoriesAsync()
        {
            return await TryGetFromCache("Slayer", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildSlayerCategoriesUrl();
                Dictionary<string, int> results = await GetResponseObject<Dictionary<string, int>>(apiUrl);
                List<SlayerCategoryImpl> categories = new List<SlayerCategoryImpl>();

                if (results == null)
                {
                    return categories;
                }

                foreach (string key in results.Keys)
                {
                    categories.Add(new SlayerCategoryImpl
                    {
                        Name = key,
                        Id = results[key]
                    });
                }

                return categories;
            });
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> GetBeastsBySlayerCategory(SlayerCategory category)
        {
            return await GetBeastsBySlayerCategory((int)category);
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> GetBeastsBySlayerCategory(int categoryId)
        {
            return await TryGetFromCache($"Slayer-{categoryId}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildSlayerBeastUrl(categoryId);
                List<BeastSearchResult> results = await GetResponseObject<List<BeastSearchResult>>(apiUrl);
                return results;
            });
        }

        public async Task<IReadOnlyList<IWeakness>> GetWeaknessesAsync()
        {
            return await TryGetFromCache("Weakness", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildWeaknessUrl();
                Dictionary<string, int> results = await GetResponseObject<Dictionary<string, int>>(apiUrl);
                List<WeaknessImpl> weaknesses = new List<WeaknessImpl>();

                if (results == null)
                {
                    return weaknesses;
                }

                foreach (string key in results.Keys)
                {
                    weaknesses.Add(new WeaknessImpl
                    {
                        Name = key,
                        Id = results[key]
                    });
                }

                return weaknesses;
            });
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByWeaknessAsync(Weakness weakness)
        {
            return await GetBeastsByWeaknessAsync((int)weakness);
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByWeaknessAsync(int weaknessId)
        {
            return await TryGetFromCache($"Weakness-{weaknessId}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildWeaknessBeastUrl(weaknessId);
                List<BeastSearchResult> results = await GetResponseObject<List<BeastSearchResult>>(apiUrl);
                return results;
            });
        }

        public async Task<IReadOnlyList<IBeastSearchResult>> GetBeastsByLevelGroupAsync(int low, int high)
        {
            return await TryGetFromCache($"Level-{low}-{high}", async () =>
            {
                string apiUrl = ApiUrls.Bestiary.BuildLevelRangeUrl(low, high);
                List<BeastSearchResult> results = await GetResponseObject<List<BeastSearchResult>>(apiUrl);
                return results;
            });
        }

        #endregion
    }
}
