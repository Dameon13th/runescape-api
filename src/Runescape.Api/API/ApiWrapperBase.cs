﻿using System;
using System.IO;
using System.Net.Http;
using System.Runtime.Caching;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Runescape.Api
{
    internal abstract class ApiWrapperBase
    {
        #region "Private Members"

        private readonly JsonSerializer _jsonSerializer;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly CacheOptions _cacheOptions;
        private static readonly object _lock;

        #endregion

        #region "Constructors"

        static ApiWrapperBase()
        {
            // Since we are using MemoryCache.Default which is static I figured our lock object should be static as well.
            _lock = new object();
        }

        protected ApiWrapperBase(IOptions<RunescapeApiOptions> options, IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _cacheOptions = options.Value.CacheOptions;
            _jsonSerializer = JsonSerializer.Create();
        }

        #endregion

        #region "Protected Methods"

        protected T TryGetFromCache<T>(string key, Func<T> factory) where T : class
        {
            lock (_lock)
            {
                if (_cacheOptions.CacheType == CacheType.None)
                {
                    return factory();
                }

                ObjectCache cache = MemoryCache.Default;
                if (cache[key] is T item)
                {
                    return item;
                }

                item = factory();

                switch (_cacheOptions.CacheType)
                {
                    case CacheType.Permanent:
                        cache[key] = item;
                        break;
                    case CacheType.Timed:
                        CacheItemPolicy policy = new CacheItemPolicy
                        {
                            SlidingExpiration = _cacheOptions.CacheTime
                        };
                        cache.Set(key, item, policy);
                        break;
                }

                return item;
            }
        }

        protected void WriteLine(string line, ConsoleColor color)
        {
            ConsoleColor oldColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ForegroundColor = oldColor;
        }

        protected async Task<string> GetResponseString(string url)
        {
            try
            {
                using (HttpResponseMessage response = await GetResponse(url))
                {
                    if (response == null)
                    {
                        return string.Empty;
                    }

                    using (Stream stream = await response.Content.ReadAsStreamAsync())
                    {
                        if (stream == null)
                        {
                            return string.Empty;
                        }

                        using (StreamReader reader = new StreamReader(stream))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.DarkRed);
                return string.Empty;
            }
        }

        protected async Task<T> GetResponseObject<T>(string url) where T : class
        {
            try
            {
                using (HttpResponseMessage response = await GetResponse(url))
                {
                    if (response == null)
                    {
                        return default;
                    }

                    using (Stream stream = await response.Content.ReadAsStreamAsync())
                    {
                        if (stream == null)
                        {
                            return default;
                        }

                        using (TextReader textReader = new StreamReader(stream))
                        {
                            using (JsonReader jsonReader = new JsonTextReader(textReader))
                            {
                                T responseObject = _jsonSerializer.Deserialize<T>(jsonReader);
                                return responseObject;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.DarkRed);
                return default;
            }
        }

        protected async Task<HttpResponseMessage> GetResponse(string url)
        {
            //HttpWebRequest request = WebRequest.CreateHttp(url);
            HttpResponseMessage tempResponse = null;
            HttpResponseMessage response;

            try
            {
                HttpClient client = _httpClientFactory.CreateClient();

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    tempResponse = await client.SendAsync(request);
                    response = tempResponse;
                    tempResponse = null;
                }

            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.DarkRed);
                return default;
            }
            finally
            {
                if (tempResponse != null)
                {
                    tempResponse.Dispose();
                }
            }
            return response;
        }

        #endregion
    }
}