﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Runescape.Api.Model;

namespace Runescape.Api
{
    internal class OldSchoolHiScores : ApiWrapperBase, IOldSchoolHiscores
    {
        #region "Constructor"

        public OldSchoolHiScores(IOptions<RunescapeApiOptions> options, IHttpClientFactory httpClientFactory)
            : base(options, httpClientFactory) { }

        #endregion

        #region "Implementation of IOldSchoolHiscores"

        public async Task<IOldSchoolPlayerHiscores> GetHiscoresAsync(OldSchoolGameMode mode, string playerName)
        {
            OldSchoolPLayerHiscores hiscores = new OldSchoolPLayerHiscores(playerName)
            {
                Mode = mode
            };

            string apiUrl = ApiUrls.OldSchoolHiscores.BuildHiscoresUrl(mode, playerName);
            string responseString = await GetResponseString(apiUrl);
            string[] lines = responseString.Split('\n');

            for (int i = 0; i <= 23; i++)
            {
                string line = lines[i];
                
                Hiscore hiscore = new Hiscore(line);
                hiscores.Skills[(Skill) i] = hiscore;
            }
            
            foreach (OldSchoolActivity activity in Enum.GetValues(typeof(OldSchoolActivity)).OfType<OldSchoolActivity>())
            {
                string line = lines[(int)activity + 24];
                Hiscore hiscore = new Hiscore(line);
                hiscores.Activities[activity] = hiscore;
            }

            return hiscores;
        }

        #endregion
    }
}