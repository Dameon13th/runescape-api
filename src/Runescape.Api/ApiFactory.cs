﻿using System.Net.Http;
using Runescape.Api.Model;

namespace Runescape.Api
{
    /// <summary>
    /// Class containing methods used to create new instance of the APIs.
    /// </summary>
    public static class ApiFactory
    {
        #region "Private Members"

        private static readonly IHttpClientFactory _httpClientFactory;

        #endregion

        #region "Constructor"

        static ApiFactory()
        {
            _httpClientFactory = new HttpClientFactory();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Creates and returns a new instance of the <see cref="IGrandExchange"/> API.
        /// </summary>
        /// <param name="cacheOptions">Parameter that determines what kind of caching the API will use on the items it retrieves.</param>
        /// <returns>A new instance of an implementation of the <see cref="IGrandExchange"/> interface.</returns>
        public static IGrandExchange CreateGrandExchange(CacheOptions cacheOptions = null)
        {
            if (cacheOptions == null)
            {
                cacheOptions = CacheOptions.None;
            }
            return new GrandExchange(new Options(cacheOptions), _httpClientFactory);
        }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="IHiscores"/> API.
        /// </summary>
        /// <param name="cacheOptions">Parameter that determines what kind of caching the API will use on the items it retrieves.</param>
        /// <returns>A new instance of an implementation of the <see cref="IHiscores"/> interface.</returns>
        public static IHiscores CreateHiscores(CacheOptions cacheOptions = null)
        {
            if (cacheOptions == null)
            {
                cacheOptions = CacheOptions.None;
            }
            return new Hiscores(new Options(cacheOptions), _httpClientFactory);
        }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="IBestiary"/> API.
        /// </summary>
        /// <param name="cacheOptions">Parameter that determines what kind of caching the API will use on the items it retrieves.</param>
        /// <returns>A new instance of an implementation of the <see cref="IBestiary"/> interface.</returns>
        public static IBestiary CreateBestiary(CacheOptions cacheOptions = null)
        {
            if (cacheOptions == null)
            {
                cacheOptions = CacheOptions.None;
            }
            return new Bestiary(new Options(cacheOptions), _httpClientFactory);
        }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="IOldSchoolHiscores"/> API.
        /// </summary>
        /// <param name="cacheOptions">Parameter that determines what kind of caching the API will use on the items it retrieves.</param>
        /// <returns>A new instance of an implementation of the <see cref="IOldSchoolHiscores"/> interface.</returns>
        public static IOldSchoolHiscores CreateOldSchoolHiscores(CacheOptions cacheOptions = null)
        {
            if (cacheOptions == null)
            {
                cacheOptions = CacheOptions.None;
            }
            return new OldSchoolHiScores(new Options(cacheOptions), _httpClientFactory);
        }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="IBestiary"/> API.
        /// </summary>
        /// <param name="cacheOptions">Parameter that determines what kind of caching the API will use on the items it retrieves.</param>
        /// <returns>A new instance of an implementation of the <see cref="IBestiary"/> interface.</returns>
        public static ISolomonStoreApi CreateSolomonStore(CacheOptions cacheOptions = null)
        {
            if (cacheOptions == null)
            {
                cacheOptions = CacheOptions.None;
            }
            return new SolomonStoreApi(new Options(cacheOptions), _httpClientFactory);
        }

        #endregion
    }
}