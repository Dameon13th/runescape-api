﻿using System.Runtime.CompilerServices;

namespace Runescape.Api
{
    /// <summary>
    /// Contains the <see cref="ApiFactory"/> and <see cref="CacheOptions"/> classes that are used
    /// to get instance of interfaces that represent many Runescape public REST APIs.
    /// </summary>
    /// <example>
    /// The following example demonstrates how to get an instance of the <see cref="IGrandExchange"/> and get the date the Grand Exchange was updated.</example>
    /// <code language="cs">
    /// <![CDATA[
    /// CacheOptions cacheOptions = CacheOptions.Permanent;
    ///
    /// IGrandeExchange grandeExchange = ApiFactory.CreateGrandExchange(cacheOptions);
    /// 
    /// DateTime lastUpdated = await grandeExchange.GetLastUpdatedDateAsync();
    /// ]]>
    /// </code>
    [CompilerGenerated]
    internal class NamespaceDoc
    {
    }
}

namespace Runescape.Api.Archaeology
{
    /// <summary>
    /// Contains the classes and interfaces for interacting with the items used in the Archaeology skill.
    /// Also contains two storage classes to aid in calculating material prices and the number of <see cref="IMaterial"/>
    /// required to repair a given amount of <see cref="IArtifact"/>s.
    /// </summary>
    /// <example>
    /// The following example demonstrates how to get the cost\worth of a given amount of <see cref="IMaterial"/>s.
    /// <code language="cs">
    /// <![CDATA[
    /// MaterialStorage storage = new MaterialStorage();
    /// storage.Add(Materials.ArmadyleanYellow, 30);
    /// storage.Add(Materials.CadmiumRed, 30);
    /// storage.Add(Materials.CobaltBlue, 30); 
    /// storage.Add(Materials.MalachiteGreen, 30);
    /// storage.Add(Materials.TyrianPurple, 30);
    ///
    /// IGrandeExchange grandeExchange = ApiFactory.CreateGrandExchange();
    ///
    /// long totalCost_Worth = await storage.CaclulateWorth(grandeExchange);
    /// ]]>
    /// </code>
    /// </example>
    /// <example>
    /// The following example demonstrates how to get the required number of <see cref="IMaterial"/> needed to fix a given number of <see cref="IArtifact"/>
    /// <code language="cs">
    /// <![CDATA[
    ///  // Create an artifact storage and add the artifacts that you want to know the require mateials for.
    /// ArtifactStorage artifactStorage = new ArtifactStorage();
    /// artifactStorage.Add(Artifacts.Tetracompass, 1);
    /// artifactStorage.Add(Artifacts.SpearOfAnnihilation, 1);
    /// artifactStorage.Add(Artifacts.ZarosianStein, 3);
    /// 
    /// // Create an instance of the material storage and add the materials you already own.
    /// MaterialStorage storage = new MaterialStorage();
    /// storage.Add(Materials.ArmadyleanYellow, 10);
    /// storage.Add(Materials.VulcanisedRubber, 100);
    /// 
    /// // Use the overloaded operator to subtract the materials you already own.
    /// artifactStorage -= storage;
    /// 
    /// foreach (KeyValuePair<IMaterial, long> valuePair in artifactStorage.TotalMaterials)
    /// {
    ///     IMaterial material = valuePair.Key;
    ///     long count = valuePair.Value;
    /// 
    ///     Console.Write($"You require '{count}' {material.Name}");
    /// }
    /// ]]>
    /// </code>
    /// </example>
    [CompilerGenerated]
    internal class NamespaceDoc
    {
    }
}

namespace Runescape.Api.Model
{
    /// <summary>
    /// Contains the interfaces for all the object that are returned from the Runescape APIs.
    /// </summary>
    [CompilerGenerated]
    internal class NamespaceDoc
    {

    }
}

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains the logic for adding the Runescape APIs as services to the .Net Core dependency injection container.
    /// </summary>
    /// <example>
    /// The following example demonstrates how to add the Runescape API to the container.
    /// <code language="cs">
    /// <![CDATA[
    /// public class Startup
    /// {
    ///     public void ConfigureServices(IServiceCollection services)
    ///     {
    ///         services.AddRunescapeApiServices(CacheOptions.None);
    ///     }
    /// }
    /// ]]>
    /// </code>
    /// The following example demonstrates how to have a Runescape API injected into your Web Controller
    /// <code language="cs">
    /// <![CDATA[
    /// [Route("[controller]")]
    /// public class ExampleController : ControllerBase
    /// {
    ///     private readonly IGrandExchange _grandExchange;
    /// 
    ///     public ExampleController(IGrandeExchange grandExchange)
    ///     {
    ///         _grandExchange = grandExchange;
    ///         // You are now free to use the _grandExchange field to access the Runescape Grand Exchange API.
    ///     }
    /// }
    /// ]]>
    /// </code>
    /// </example>
    [CompilerGenerated]
    internal class NamespaceDoc
    {

    }
}
